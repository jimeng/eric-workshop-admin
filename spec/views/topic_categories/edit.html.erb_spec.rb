require 'spec_helper'

describe "topic_categories/edit" do
  before(:each) do
    @topic_category = assign(:topic_category, stub_model(TopicCategory,
      :name => "MyString"
    ))
  end

  it "renders the edit topic_category form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", topic_category_path(@topic_category), "post" do
      assert_select "input#topic_category_name[name=?]", "topic_category[name]"
    end
  end
end
