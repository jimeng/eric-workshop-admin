require 'spec_helper'

describe "topic_categories/show" do
  before(:each) do
    @topic_category = assign(:topic_category, stub_model(TopicCategory,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
  end
end
