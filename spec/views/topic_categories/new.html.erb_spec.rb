require 'spec_helper'

describe "topic_categories/new" do
  before(:each) do
    assign(:topic_category, stub_model(TopicCategory,
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new topic_category form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", topic_categories_path, "post" do
      assert_select "input#topic_category_name[name=?]", "topic_category[name]"
    end
  end
end
