require 'spec_helper'
require 'date'

describe TimeRange do

  it "can create a valid TimeRange object" do
  	nine = Time.zone.now.beginning_of_day.advance(:hours => 9)
  	ten = nine.advance(:hours => 1)
  	tr = TimeRange.new(nine.to_datetime, ten.to_datetime)
  	(tr.present? && tr.valid?).should == true
  end

  it "can create tell that a TimeRange is invalid because a start_time is after end_time" do
  	nine = Time.zone.now.beginning_of_day.advance(:hours => 9)
  	ten = nine.advance(:hours => 1)
 	  tr = TimeRange.new(ten.to_datetime, nine.to_datetime)
  	(tr.present? && tr.valid?).should == false
  end

  it "can create tell that a TimeRange is invalid because start_time and end_time are not in the same day" do
  	now = Time.zone.now
  	later = now.advance(:hours => 26)
  	tr = TimeRange.new(now.to_datetime, later.to_datetime)
    tr.present?.should == true
  	tr.valid?.should == false
  end

  it "can create a TimeRange object from a 'day' and two 'times'" do
  	day = "2014-10-15"
  	time0 = "09:15 AM"
  	time1 = "10:45 AM"
  	tr = TimeRange.from_parts(day, time0, time1)
  	(tr.present? && tr.valid?).should == true
  end

  it "can output string representations of a valid TimeRange object" do
  	tr = TimeRange.from_parts("2014-10-15", "09:15 AM", "10:45 AM")
  	(tr.present? && tr.valid?).should == true
  	tr.data_start_time.should == "2014-10-15 09:15 AM"
  	tr.data_end_time.should == "2014-10-15 10:45 AM"  	
  	tr.duration_minutes.should == 90
  	tr.display_day.should == "Wednesday, October 15, 2014"
  	tr.display_short_day.should == "10/15/2014"
  	tr.display_time0.should == "9:15 AM"
  	tr.display_time1.should == "10:45 AM"
  end

  it "can avoid rounding errors when outputting a time" do
    local = Time.zone.now
    t0 = Time.new(2013, 8, 8, 11, 0, 0, local.utc_offset)
    t1 = Time.new(2013, 8, 8, 12, 30, 0, local.utc_offset)
    tr = TimeRange.new t0, t1
    tr.data_start_time.should == "2013-08-08 11:00 AM"
    tr.data_end_time.should == "2013-08-08 12:30 PM"   
    tr.duration_minutes.should == 90
    tr.display_day.should == "Thursday, August 8, 2013"
    tr.display_short_day.should == "08/08/2013"
    tr.display_time0.should == "11:00 AM"
    tr.display_time1.should == "12:30 PM"
    tr.start_time.should == t0
    tr.end_time.should == t1
    tr2 = TimeRange.new t0.to_datetime, t1.to_datetime
    tr2.start_time.should == t0
    tr2.end_time.should == t1
  end

  it "can handle null inputs with pizzazz" do
    tr = TimeRange.new nil, nil
    tr.present?.should == true
    tr.valid?.should == false
    tr.start_time.nil?.should == true
    tr.end_time.nil?.should == true
    tr.day.should == nil
    tr.duration_minutes.should == 0
    
    tr.duration_minutes = 90
    tr.valid?.should == true
    tr.duration_minutes.should == 90
  end

end
