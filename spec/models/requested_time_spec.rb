require 'spec_helper'

describe RequestedTime do
  pending "add some examples to (or delete) #{__FILE__}"

  it "can create a valid RequestedTime object given a valid TimeRange object" do
    nine = Time.zone.now.beginning_of_day.advance(:hours => 9)
    ten = nine.advance(:hours => 1)
    tr = TimeRange.new(nine.to_datetime, ten.to_datetime)
    tr.present?.should == true
    tr.valid?.should == true
    requested_time = RequestedTime.new :time_range => tr
    requested_time.present?.should == true
    requested_time.time_range.present?.should == true
    requested_time.time_range.valid?.should == true
  end

end
