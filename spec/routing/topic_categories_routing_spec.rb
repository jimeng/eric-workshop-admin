require "spec_helper"

describe TopicCategoriesController do
  describe "routing" do

    it "routes to #index" do
      get("/topic_categories").should route_to("topic_categories#index")
    end

    it "routes to #new" do
      get("/topic_categories/new").should route_to("topic_categories#new")
    end

    it "routes to #show" do
      get("/topic_categories/1").should route_to("topic_categories#show", :id => "1")
    end

    it "routes to #edit" do
      get("/topic_categories/1/edit").should route_to("topic_categories#edit", :id => "1")
    end

    it "routes to #create" do
      post("/topic_categories").should route_to("topic_categories#create")
    end

    it "routes to #update" do
      put("/topic_categories/1").should route_to("topic_categories#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/topic_categories/1").should route_to("topic_categories#destroy", :id => "1")
    end

  end
end
