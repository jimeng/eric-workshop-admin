require 'faker'
FactoryGirl.define do
  factory :user do |f|
    f.firstname { Faker::Name.first_name }
    f.lastname { Faker::Name.last_name }
    f.calendar_url {Faker::Internet.email}
    f.uniqname {Faker::Internet.user_name}
    f.password {Faker::Name.name}
  end
end
