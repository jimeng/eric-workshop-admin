require 'spec_helper'

describe User do
    describe "login failure" do
      it "gives an error message when login fails" do
        lambda do
          visit login_path
          usr = FactoryGirl.build(:user)
          fill_in "name",          :with => usr.uniqname
          fill_in "password",      :with => usr.password
          click_button('Log in')
          page.should have_content("Login failed")
        end.should_not change(User, :count)
      end
    end

    describe "login sucess" do
      it "gives content from workshop dashboard if success" do
        lambda do
          visit login_path
          contact = FactoryGirl.build(:user, uniqname: "ericeche", password: "foo")
          fill_in "name",          :with => contact.uniqname
          fill_in "password",      :with => contact.password
          click_button('Log in')
          page.should have_content("Workshop Dashboard")
        end.should_not change(User, :count)
      end
    end
end
