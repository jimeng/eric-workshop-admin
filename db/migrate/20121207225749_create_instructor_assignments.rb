class CreateInstructorAssignments < ActiveRecord::Migration
  def change
    create_table :instructor_assignments do |t|
      t.references :workshop
      t.references :user
      t.integer :order

      t.timestamps
    end
    add_index :instructor_assignments, :workshop_id
    add_index :instructor_assignments, :user_id
    add_index :instructor_assignments, [:workshop_id, :user_id], unique: true
  end
end
