class CreateRegistrations < ActiveRecord::Migration
  def change
    create_table :registrations do |t|
      t.string  :name
      t.string  :setup_url
      t.boolean :active

      t.timestamps
    end
    add_index :registrations, :name
  end
end
