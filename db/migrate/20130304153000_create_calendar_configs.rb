class CreateCalendarConfigs < ActiveRecord::Migration
  def change
    create_table :calendar_configs do |t|
      t.string :name
      t.string :value

      t.timestamps
    end
    add_index :calendar_configs, :name, :unique => true
  end
end
