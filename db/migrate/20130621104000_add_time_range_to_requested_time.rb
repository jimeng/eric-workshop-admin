class AddTimeRangeToRequestedTime < ActiveRecord::Migration
  def change
    change_table :requested_times do |t|
      t.datetime :time_range_start_time
      t.datetime :time_range_end_time
    end
    RequestedTime.all.each do |rt|
      rt.update_attribute :time_range, TimeRange.new(rt.start_time, rt.end_time)
    end
  end
end
