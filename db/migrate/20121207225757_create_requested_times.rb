class CreateRequestedTimes < ActiveRecord::Migration
  def change
    create_table :requested_times do |t|
      t.references :workshop
      t.datetime :start_time
      t.integer :duration_minutes

      t.timestamps
    end
    add_index :requested_times, :workshop_id
  end
end
