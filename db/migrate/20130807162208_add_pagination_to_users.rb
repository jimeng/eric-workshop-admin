class AddPaginationToUsers < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.string :pagination
    end
  end
end
