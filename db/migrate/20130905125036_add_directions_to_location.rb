class AddDirectionsToLocation < ActiveRecord::Migration
  def change
        add_column :locations, :directions, :text, limit: 5000
  end
end
