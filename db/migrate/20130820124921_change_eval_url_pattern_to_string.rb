class ChangeEvalUrlPatternToString < ActiveRecord::Migration
  def up
    change_column :evaluations, :eval_url_pattern, :text, limit: 65535
  end

  def down
    change_column :evaluations, :eval_url_pattern, :string
  end
end
