class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :firstname
      t.string :lastname
      t.string :uniqname
      t.string :role, {default: 'requester'}
      t.string :calendar_url
      t.boolean :active, {default: true}
      t.boolean :notify_new_request, {default: false}

      t.timestamps
    end
    add_index :users, :uniqname, unique: true
  end
end
