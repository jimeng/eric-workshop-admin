class CreateNotificationPreferences < ActiveRecord::Migration
  def change
    create_table :notification_preferences do |t|
      t.references :user, :null => false
      t.string :notifier, :null => false, :limit => 16, :default => 'ping'
      t.string :event_type, :limit => 32
      t.string :scope, :limit => 16
      t.references :workshop
      t.boolean :active, :default => true

      t.timestamps
    end
    add_index :notification_preferences, :user_id
    add_index :notification_preferences, :workshop_id
    add_index :notification_preferences, :event_type
  end
end
