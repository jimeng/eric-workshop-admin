class CreateAttachments < ActiveRecord::Migration
  def change
    create_table :attachments do |t|
      t.references :workshop

      t.timestamps
    end
    add_index :attachments, :workshop_id
  end
end
