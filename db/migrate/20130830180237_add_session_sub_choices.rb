class AddSessionSubChoices < ActiveRecord::Migration
  def change
    create_table :session_sub_choices do |t|
      t.references :session_type
      t.string     :name
      t.boolean    :active, :default => true

      t.timestamps
    end
    add_index :session_sub_choices, :session_type_id
  end
end
