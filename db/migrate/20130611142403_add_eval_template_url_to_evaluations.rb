class AddEvalTemplateUrlToEvaluations < ActiveRecord::Migration
  def change
    change_table :evaluations do |t|
      t.string :eval_template_url
    end
  end
end
