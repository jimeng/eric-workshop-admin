class AddAccessTokenAndRefreshTokenToUsers < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.string  :access_token
      t.string  :refresh_token
    end
  end
end
