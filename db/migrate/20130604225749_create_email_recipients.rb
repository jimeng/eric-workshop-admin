class CreateEmailRecipients < ActiveRecord::Migration
  def change
    create_table :email_recipients do |t|
      t.references :comment
      t.references :user

      t.timestamps
    end
    add_index :email_recipients, :comment_id
    add_index :email_recipients, :user_id
    add_index :email_recipients, [:comment_id, :user_id], unique: true
  end
end
