class AddDefaultToPhysicalItems < ActiveRecord::Migration
  def change
		change_column_default :statistics, :physical_items, false
  end
end
