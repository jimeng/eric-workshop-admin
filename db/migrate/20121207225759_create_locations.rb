class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :name
      t.string :building
      t.integer :seats
      t.integer :computers
      t.string :computer_type
      t.string :support_phone
      t.string :support_email
      t.string :calendar_url
      t.boolean :active, {default: true}

      t.timestamps
    end
  end
end
