class AddMultipleToEmailTemplates < ActiveRecord::Migration
  def change
    add_column :email_templates, :multiple, :boolean, default: false
  end
end
