class CreateTopicsWorkshops < ActiveRecord::Migration
  def change
    create_table :topics_workshops do |t|
      t.references :topic
      t.references :workshop
    end
    add_index :topics_workshops, :workshop_id
    add_index :topics_workshops, :topic_id
  end
end
