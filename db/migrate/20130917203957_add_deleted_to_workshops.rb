class AddDeletedToWorkshops < ActiveRecord::Migration
  def change
    add_column :workshops, :deleted, :boolean, default: false 
  end
end
