class CreateWorkshopEvents < ActiveRecord::Migration
  def change
    create_table :workshop_events do |t|
      t.references :workshop
      t.string :name
      t.timestamp :timestamp
      t.references :user
      t.text :details

      t.timestamps
    end
    add_index :workshop_events, :workshop_id
    add_index :workshop_events, :user_id
  end
end
