class AddSystemStatsToEmailTemplates < ActiveRecord::Migration
  def change
    add_column :email_templates, :system_stats, :boolean, default: false
  end
end
