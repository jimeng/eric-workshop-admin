class AddSubChoicePicks < ActiveRecord::Migration
  def change
    create_table :sub_choice_picks do |t|
      t.references :workshop
      t.references :session_sub_choice

      t.timestamps
    end
    add_index :sub_choice_picks, :workshop_id
    add_index :sub_choice_picks, :session_sub_choice_id
    add_index :sub_choice_picks, [:workshop_id, :session_sub_choice_id], unique: true
  end
end
