class CreateStatistics < ActiveRecord::Migration
  def change
    create_table :statistics do |t|
      t.references :workshop, null: false
      t.integer :participants
      t.time :prep_time
      t.string :unit
      t.string :unit2
      t.boolean :physical_items
      t.boolean :completed, {default: false}
      t.text :notes

      t.timestamps
    end
    add_index :statistics, :workshop_id, unique: true
  end
end
