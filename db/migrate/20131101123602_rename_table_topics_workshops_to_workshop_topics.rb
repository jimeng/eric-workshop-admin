class RenameTableTopicsWorkshopsToWorkshopTopics < ActiveRecord::Migration
  def change
    rename_table :topics_workshops, :workshop_topics
    change_table :workshop_topics do |t|
      t.timestamps
    end
  end
end
