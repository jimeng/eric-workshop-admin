class AddInfoUrlToEvaluation < ActiveRecord::Migration
  def change
        add_column :evaluations, :eval_info_url, :string
  end
end
