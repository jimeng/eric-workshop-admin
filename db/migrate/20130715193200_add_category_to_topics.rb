class AddCategoryToTopics < ActiveRecord::Migration
  def change
    change_table :topics do |t|
      t.references :category
    end

  end
end
