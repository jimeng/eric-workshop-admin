class CreateTopics < ActiveRecord::Migration
  def change
    create_table :topics do |t|
      t.string :title
      t.text :description
      t.boolean :active, {default: true}

      t.timestamps
    end
  end
end
