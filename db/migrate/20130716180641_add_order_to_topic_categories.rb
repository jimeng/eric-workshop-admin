class AddOrderToTopicCategories < ActiveRecord::Migration
  def change
    change_table :topic_categories do |t|
      t.integer :order
    end
  end
end
