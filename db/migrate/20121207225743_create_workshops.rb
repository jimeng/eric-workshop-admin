class CreateWorkshops < ActiveRecord::Migration
  def change
    create_table :workshops do |t|
      t.string :title
      t.datetime :start_time
      t.integer :duration_minutes
      t.references :contact_user
      t.references :location
      t.boolean :accepted, {default: false}
      t.boolean :archived, {default: false}
      t.boolean :class_related, {default: true}
      t.boolean :library_location_required, {default: true}
      t.string  :location_other
      t.integer :expected_attendance
      t.references :managed_by
      t.references :requested_by
      t.boolean :evaluation_setup, {default: false}
      t.boolean :registration_setup, {default: false}
      t.references :evaluation
      t.references :registration
      t.string :course
      t.string :subject
      t.string :section
      t.references :session_type
      t.text :notes
      t.text :requester_questions
      t.text :scheduler_notes
      t.string :instruction_event_id
      t.string :location_event_id

      t.timestamps
    end
    add_index :workshops, :contact_user_id
    add_index :workshops, :location_id
    add_index :workshops, :managed_by_id
    add_index :workshops, :requested_by_id
    add_index :workshops, :session_type_id
  end
end
