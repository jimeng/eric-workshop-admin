class AddEvalResultsUrlToWorkshops < ActiveRecord::Migration
  def change
    change_table :workshops do |t|
      t.string :eval_results_url
    end
  end
end
