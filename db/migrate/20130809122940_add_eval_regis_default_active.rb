class AddEvalRegisDefaultActive < ActiveRecord::Migration
  def change
	change_column_default :evaluations, :active, true
	change_column_default :registrations, :active, true
	change_column_default :email_templates, :active, true
  end
end
