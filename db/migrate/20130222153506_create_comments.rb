class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :subject
      t.text :content
      t.references :workshop
      t.references :user
      t.boolean :email_to_requester
      t.boolean :email_to_contact

      t.timestamps
    end
    add_index :comments, :workshop_id
    add_index :comments, :user_id
  end
end
