class RenameUserPaginationToRecordsPerPage < ActiveRecord::Migration
 def change
   rename_column :users, :pagination, :records_per_page
 end
end
