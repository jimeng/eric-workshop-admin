class CreateUpdates < ActiveRecord::Migration
  def change
    create_table :updates do |t|
      t.references :user
      t.references :workshop
      t.references :perpetrator
      t.string :event_type, :limit => 32
      t.boolean :seen, :default => false

      t.timestamps
    end
    add_index :updates, :created_at
    add_index :updates, :user_id
    add_index :updates, :workshop_id
  end
end
