class CreateEvaluations < ActiveRecord::Migration
  def change
    create_table :evaluations do |t|
      t.string  :name
      t.string  :setup_url
      t.boolean :active

      t.timestamps
    end
    add_index :evaluations, :name
  end
end
