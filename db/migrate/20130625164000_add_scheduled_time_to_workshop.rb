class AddScheduledTimeToWorkshop < ActiveRecord::Migration
  def change
    change_table :workshops do |t|
      t.datetime :scheduled_time_start_time
      t.datetime :scheduled_time_end_time
    end
    Workshop.all.each do |w|
      w.update_attribute :scheduled_time, TimeRange.new(w.start_time, w.end_time)
    end
  end
end
