class AddEvalUrlPatternToEvaluation < ActiveRecord::Migration
  def change
    add_column :evaluations, :eval_url_pattern, :string
  end
end
