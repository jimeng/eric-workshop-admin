# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20131101123602) do

  create_table "attachments", :force => true do |t|
    t.integer  "workshop_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.string   "attached_file_file_name"
    t.string   "attached_file_content_type"
    t.integer  "attached_file_file_size"
    t.datetime "attached_file_updated_at"
  end

  add_index "attachments", ["workshop_id"], :name => "index_attachments_on_workshop_id"

  create_table "calendar_configs", :force => true do |t|
    t.string   "name"
    t.string   "value"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "calendar_configs", ["name"], :name => "index_calendar_configs_on_name", :unique => true

  create_table "comments", :force => true do |t|
    t.string   "subject"
    t.text     "content"
    t.integer  "workshop_id"
    t.integer  "user_id"
    t.boolean  "email_to_requester"
    t.boolean  "email_to_contact"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "comments", ["user_id"], :name => "index_comments_on_user_id"
  add_index "comments", ["workshop_id"], :name => "index_comments_on_workshop_id"

  create_table "email_recipients", :force => true do |t|
    t.integer  "comment_id"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "email_recipients", ["comment_id", "user_id"], :name => "index_email_recipients_on_comment_id_and_user_id", :unique => true
  add_index "email_recipients", ["comment_id"], :name => "index_email_recipients_on_comment_id"
  add_index "email_recipients", ["user_id"], :name => "index_email_recipients_on_user_id"

  create_table "email_templates", :force => true do |t|
    t.string   "name"
    t.string   "subject"
    t.text     "body"
    t.boolean  "active",       :default => true
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.boolean  "multiple",     :default => false
    t.boolean  "system_stats", :default => false
  end

  create_table "evaluations", :force => true do |t|
    t.string   "name"
    t.string   "setup_url"
    t.boolean  "active",            :default => true
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.string   "eval_template_url"
    t.text     "eval_url_pattern"
    t.string   "eval_info_url"
  end

  add_index "evaluations", ["name"], :name => "index_evaluations_on_name"

  create_table "instructor_assignments", :force => true do |t|
    t.integer  "workshop_id"
    t.integer  "user_id"
    t.integer  "order"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "instructor_assignments", ["user_id"], :name => "index_instructor_assignments_on_user_id"
  add_index "instructor_assignments", ["workshop_id", "user_id"], :name => "index_instructor_assignments_on_workshop_id_and_user_id", :unique => true
  add_index "instructor_assignments", ["workshop_id"], :name => "index_instructor_assignments_on_workshop_id"

  create_table "locations", :force => true do |t|
    t.string   "name"
    t.string   "building"
    t.integer  "seats"
    t.integer  "computers"
    t.string   "computer_type"
    t.string   "support_phone"
    t.string   "support_email"
    t.string   "calendar_url"
    t.boolean  "active",        :default => true
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.text     "directions"
  end

  create_table "notification_preferences", :force => true do |t|
    t.integer  "user_id",                                       :null => false
    t.string   "notifier",    :limit => 16, :default => "ping", :null => false
    t.string   "event_type",  :limit => 32
    t.string   "scope",       :limit => 16
    t.integer  "workshop_id"
    t.boolean  "active",                    :default => true
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
  end

  add_index "notification_preferences", ["event_type"], :name => "index_notification_preferences_on_event_type"
  add_index "notification_preferences", ["user_id"], :name => "index_notification_preferences_on_user_id"
  add_index "notification_preferences", ["workshop_id"], :name => "index_notification_preferences_on_workshop_id"

  create_table "registrations", :force => true do |t|
    t.string   "name"
    t.string   "setup_url"
    t.boolean  "active",     :default => true
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  add_index "registrations", ["name"], :name => "index_registrations_on_name"

  create_table "requested_times", :force => true do |t|
    t.integer  "workshop_id"
    t.datetime "start_time"
    t.integer  "duration_minutes"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.datetime "time_range_start_time"
    t.datetime "time_range_end_time"
  end

  add_index "requested_times", ["workshop_id"], :name => "index_requested_times_on_workshop_id"

  create_table "session_sub_choices", :force => true do |t|
    t.integer  "session_type_id"
    t.string   "name"
    t.boolean  "active",          :default => true
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
  end

  add_index "session_sub_choices", ["session_type_id"], :name => "index_session_sub_choices_on_session_type_id"

  create_table "session_types", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "active",      :default => true
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "statistics", :force => true do |t|
    t.integer  "workshop_id"
    t.integer  "participants"
    t.time     "prep_time"
    t.string   "unit"
    t.string   "unit2"
    t.boolean  "physical_items", :default => false
    t.boolean  "completed",      :default => false
    t.text     "notes"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
  end

  add_index "statistics", ["workshop_id"], :name => "index_statistics_on_workshop_id"

  create_table "sub_choice_picks", :force => true do |t|
    t.integer  "workshop_id"
    t.integer  "session_sub_choice_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  add_index "sub_choice_picks", ["session_sub_choice_id"], :name => "index_sub_choice_picks_on_session_sub_choice_id"
  add_index "sub_choice_picks", ["workshop_id", "session_sub_choice_id"], :name => "index_sub_choice_picks_on_workshop_id_and_session_sub_choice_id", :unique => true
  add_index "sub_choice_picks", ["workshop_id"], :name => "index_sub_choice_picks_on_workshop_id"

  create_table "topic_categories", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "order"
  end

  create_table "topics", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.boolean  "active",      :default => true
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.integer  "category_id"
  end

  create_table "units", :force => true do |t|
    t.string   "name"
    t.boolean  "active",     :default => true
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "updates", :force => true do |t|
    t.integer  "user_id"
    t.integer  "workshop_id"
    t.integer  "perpetrator_id"
    t.string   "event_type",     :limit => 32
    t.boolean  "seen",                         :default => false
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
  end

  add_index "updates", ["created_at"], :name => "index_updates_on_created_at"
  add_index "updates", ["user_id"], :name => "index_updates_on_user_id"
  add_index "updates", ["workshop_id"], :name => "index_updates_on_workshop_id"

  create_table "users", :force => true do |t|
    t.string   "firstname"
    t.string   "lastname"
    t.string   "uniqname"
    t.string   "role",               :default => "requester"
    t.string   "calendar_url"
    t.boolean  "active",             :default => true
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
    t.string   "crypted_password"
    t.string   "salt"
    t.boolean  "notify_new_request", :default => false
    t.string   "access_token"
    t.string   "refresh_token"
    t.string   "records_per_page"
    t.string   "unit"
  end

  add_index "users", ["uniqname"], :name => "index_users_on_uniqname", :unique => true

  create_table "workshop_events", :force => true do |t|
    t.integer  "workshop_id"
    t.string   "name"
    t.datetime "timestamp"
    t.integer  "user_id"
    t.text     "details"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "workshop_events", ["user_id"], :name => "index_workshop_events_on_user_id"
  add_index "workshop_events", ["workshop_id"], :name => "index_workshop_events_on_workshop_id"

  create_table "workshop_topics", :force => true do |t|
    t.integer  "topic_id"
    t.integer  "workshop_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "workshop_topics", ["topic_id"], :name => "index_topics_workshops_on_topic_id"
  add_index "workshop_topics", ["workshop_id"], :name => "index_topics_workshops_on_workshop_id"

  create_table "workshops", :force => true do |t|
    t.string   "title"
    t.datetime "start_time"
    t.integer  "duration_minutes"
    t.integer  "contact_user_id"
    t.integer  "location_id"
    t.boolean  "accepted",                  :default => false
    t.boolean  "archived",                  :default => false
    t.boolean  "class_related",             :default => true
    t.boolean  "library_location_required", :default => true
    t.string   "location_other"
    t.integer  "expected_attendance"
    t.integer  "managed_by_id"
    t.integer  "requested_by_id"
    t.boolean  "evaluation_setup",          :default => false
    t.boolean  "registration_setup",        :default => false
    t.integer  "evaluation_id"
    t.integer  "registration_id"
    t.string   "course"
    t.string   "subject"
    t.string   "section"
    t.integer  "session_type_id"
    t.text     "notes"
    t.text     "requester_questions"
    t.text     "scheduler_notes"
    t.string   "instruction_event_id"
    t.string   "location_event_id"
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
    t.string   "eval_results_url"
    t.datetime "scheduled_time_start_time"
    t.datetime "scheduled_time_end_time"
    t.integer  "ttcid"
    t.boolean  "deleted",                   :default => false
  end

  add_index "workshops", ["contact_user_id"], :name => "index_workshops_on_contact_user_id"
  add_index "workshops", ["location_id"], :name => "index_workshops_on_location_id"
  add_index "workshops", ["managed_by_id"], :name => "index_workshops_on_managed_by_id"
  add_index "workshops", ["requested_by_id"], :name => "index_workshops_on_requested_by_id"
  add_index "workshops", ["session_type_id"], :name => "index_workshops_on_session_type_id"

end
