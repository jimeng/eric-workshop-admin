local_only = ENV['LOCAL_GEMS_ONLY'] || false
unless local_only
  ltig_gems = `hostname`.chomp == 'chai.umdl.umich.edu' ?
    'http://ltig:309@chai.umdl.umich.edu:3003' :
    'http://ltig:309@ruby-3.ltig.lib.umich.edu'
  source ltig_gems
  source 'https://rubygems.org'
end
gem 'rails', '3.2.16'
gem 'rake', '~> 10.1.0'
gem 'json', '~> 1.8.0'

gem 'rails_config', '~> 0.3.3'

gem 'sorcery', '~> 0.8.1'
gem 'request_store', '~> 1.0.5'
gem 'cancan', '~> 1.6.9'
gem 'squeel', '~> 1.0.18'

group :assets do
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'
  gem 'uglifier', '>= 1.0.3'
  gem 'jquery-datatables-rails', '1.11.2.7'
  gem 'jquery-ui-rails'
  gem 'jquery-rails'
  gem 'bootstrap-sass', '~> 2.3.1.0'
  gem 'handlebars_assets', '~> 0.14.1'
end

gem "momentjs-rails", "~> 1.7.2"
# gem "bootstrap-datepicker-rails", "~> 1.0.0"
gem "bootstrap-x-editable-rails", "~> 1.4.1"

gem 'umiac', '~> 0.0.1'

gem "google-api-client", "~> 0.6.3"
gem "mailman", "~> 0.6.0"
gem "liquid", "~> 2.5.0"

gem "rufus-scheduler", "~> 3.0.0"
gem "eventmachine", "~> 1.0.3"

# To use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# To use Jbuilder templates for JSON
# gem 'jbuilder'

# Deploy with Capistrano
# gem 'capistrano'

gem 'will_paginate'
gem 'paperclip', '~> 3.4.0'

# We have released version 0.5.0 of net-ldap to chai because the project maintainers are no
# longer publishing to rubygems.org and there are bugs in the latest released version that
# were affecting us.
gem "net-ldap", "~> 0.5.0"

gem "fullcalendar-rails", "~> 1.5.4.0"
gem "jquery-timepicker-rails", "~> 1.1.0.0"
gem "bootstrap-datetimepicker-rails", "~> 0.0.11.1"
gem "select2-rails", "~> 3.4.7"

gem "pnotify-rails", "~> 1.2.0"
      
platform :ruby do
  gem 'unicorn', '4.6.0'
  gem 'sqlite3'

  gem 'debugger', group: [:development, :test]
  gem 'mysql2'

  group :assets do
    gem 'therubyracer'
  end
end

platform :jruby do
  gem 'activerecord-jdbcsqlite3-adapter', '~> 1.3.3'
  gem 'activerecord-jdbcmysql-adapter', '~> 1.3.3'
  gem 'ruby-debug', group: [:development, :test]
  #gem 'torquebox-cache', '~> 3.0.0'
  #gem "jruby-openssl", "~> 0.9.4", require: false

  group :assets do
    gem 'therubyrhino'
  end
end

group :development do
  gem "rails-dev-tweaks"
  gem 'hirb'
end

gem 'rspec-rails', '~> 2.14.0', :group => [:test, :development]

group :test do
  gem 'capybara', '~> 2.1.0'
  gem 'factory_girl_rails', '~> 4.2.1', :require => false
  gem 'faker', '~> 1.1.2'
end

