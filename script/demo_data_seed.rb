
load "#{Rails.root}/script/common_data_seed.rb"

users = User.create([
  {firstname: 'Noah', lastname: 'Botimer', uniqname: 'botimer', role: 'admin', active: true, calendar_url: 'botimer@umich.edu'},
  {firstname: 'Eric', lastname: 'Echeverri', uniqname: 'ericeche', role: 'admin', active: true, calendar_url: 'ericeche@umich.edu'},
  {firstname: 'Jim', lastname: 'Eng', uniqname: 'jimeng', role: 'admin', active: true, calendar_url: 'jimeng@umich.edu', notify_new_request: true},
  {firstname: 'John', lastname: 'Leasia', uniqname: 'jleasia', role: 'admin', active: true, calendar_url: 'jleasia@umich.edu'},
  {firstname: 'Doreen', lastname: 'Bradley', uniqname: 'dbradley', role: 'scheduler', active: true, calendar_url: 'dbradley@umich.edu'},
  {firstname: 'Donna', lastname: 'McCauley', uniqname: 'dmmcaul', role: 'scheduler', active: true, calendar_url: 'dmmcaul@umich.edu'},
  {firstname: 'Laurie', lastname: 'Sutch', uniqname: 'lasutch', role: 'instructor', active: true, calendar_url: 'lasutch@umich.edu'},
  {firstname: 'Diana', lastname: 'Perpich', uniqname: 'dperpich', role: 'instructor', active: true, calendar_url: 'dperpich@umich.edu'},
  {firstname: 'Susan', lastname: 'Hollar', uniqname: 'shollar', role: 'requester', active: true},
  {firstname: 'Charles', lastname: 'Severance', uniqname: 'csev', role: 'requester', active: true},
  {firstname: 'Gonzalo', lastname: 'Silverio', uniqname: 'gsilver', role: 'requester', active: true}
])

User.where(role: ['scheduler', 'admin']).each do |u|
  n = NotificationPreference.new(:event_type => 'workshop.create', :notifier => 'email', :scope => 'all')
  n.user = u 
  n.save
  n = NotificationPreference.new(:event_type => 'workshop.create', :notifier => 'ping', :scope => 'all')
  n.user = u 
  n.save
  n = NotificationPreference.new(:event_type => 'workshop.update.instructor', :notifier => 'ping', :scope => 'all')
  n.user = u 
  n.save
  n = NotificationPreference.new(:event_type => 'email.received', :notifier => 'ping', :scope => 'all')
  n.user = u 
  n.save
  n = NotificationPreference.new(:event_type => 'email.sent', :notifier => 'ping', :scope => 'all')
  n.user = u 
  n.save
end

locations = Location.create([
  {name: 'Gallery Lab', building: 'Hatcher 100', seats: 30, computers: 1, computer_type: 'PC', support_phone: '111-1111',
    support_email: 'support@umich.edu', calendar_url: 'jnckvrkjriq12vufq3p9af9810@group.calendar.google.com', active: true},
  {name: '4041 Shapiro', building: 'Shapiro', seats: 75, computers: 0,  support_phone: '111-1111', support_email: 'support@umich.edu',
   active: true, calendar_url:  'locvrhf3p2e69r77udsvcihjis@group.calendar.google.com'},
  {name: '4041 Shapiro (ULIC)', building: 'Shapiro', seats: 20, computers: 0,  support_phone: '111-1111', support_email: 'support@umich.edu',
   active: true, calendar_url: 'umich.edu_55ec7knevvmrq5l11gcbuhria0@group.calendar.google.com'},
  {name: '4004 Shapiro (TAFR)', building: 'Shapiro', seats: 25, computers: 1, computer_type: 'Mix', support_phone: '111-1111',
    support_email: 'support@umich.edu', active: true, calendar_url: 'hth66igsmu766phgh4ng6l7e64@group.calendar.google.com'},
  {name: 'Training Rm A', building: 'Duderstadt', seats: 30, computers: 30, computer_type: 'Mac', support_phone: '222-2222',
    support_email: 'support2@umich.edu', active: true, calendar_url: 'umich.edu_q9nnqic91h0i03mgv2n6i0mfcg@group.calendar.google.com'},
  {name: 'Dean Conf Rm', building: 'Hatcher 8th Flr', seats: 20, computers: 1, computer_type: 'PC', support_phone: '222-2222',
    support_email: 'support2@umich.edu', active: false, calendar_url: ' umich.edu_f8gjtpvjraj058cjh3mra7u9g0@group.calendar.google.com'},
 {name: 'Doorless Rm', building: 'Basement -7', seats: 0, computers: 2000, computer_type: 'Other', support_phone: '000-0000',
  support_email: 'lockedin@umich.edu', active: true}
])


comments = Comment.create([
  {subject: 'Note',  user_id: 4, content: 'Instructor would like 10 min at end for questions', workshop_id: 1},
  {subject: 'Note2', user_id: 6, content: 'Please bring copy of slides for handouts', workshop_id: 1},
  {subject: 'Note3', user_id: 4, content: 'Please bring copy of slides for handouts', workshop_id: 2},
  {subject: 'Note4', user_id: 6, content: 'Suppose end get boy warrant general natural. Delightful met sufficient projection ask.
    Decisively everything principles if preference do impression of. Preserved oh so difficult repulsive on in household.
    In what do miss time be. Valley as be appear cannot so by. Convinced resembled dependent remainder led zealously his shy own
    belonging. Always length letter adieus add number moment she. Promise few compass six several old offices removal parties fat.
    Concluded rapturous it intention perfectly daughters is as.', workshop_id: 3},
  {subject: 'Note4', user_id: 4, content: 'His followed carriage proposal entrance directly had elegance. Greater for cottage gay parties
    natural. Remaining he furniture on he discourse suspected perpetual. Power dried her taken place day ought the. Four and our ham west
    miss. Education shameless who middleton agreement how. We in found world chief is at means weeks smile.', workshop_id: 3},
  {subject: 'Note4', user_id: 4, content: 'His followed carriage proposal entrance directly had elegance. Greater for cottage gay parties
    natural. Remaining he furniture on he discourse suspected perpetual. Power dried her taken place day ought the. Four and our ham west
    miss. Education shameless who middleton agreement how. We in found world chief is at means weeks smile.', workshop_id: 5},
  {subject: 'Note4', user_id: 6, content: 'Suppose end get boy warrant general natural. Delightful met sufficient projection ask.
    Decisively everything principles if preference do impression of. Preserved oh so difficult repulsive on in household. In what
    do miss time be. Valley as be appear cannot so by. Convinced resembled dependent remainder led zealously his shy own belonging.
    Always length letter adieus add number moment she. Promise few compass six several old offices removal parties fat. Concluded
    rapturous it intention perfectly daughters is as.', workshop_id: 4}
])

requested_times = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 8, 8, 12, 30, 0, '-4'), DateTime.new(2013, 8, 8, 13, 30, 0, '-4') ) },
  { time_range: TimeRange.new( DateTime.new(2013, 8, 9, 12, 30, 0, '-4'), DateTime.new(2013, 8, 9, 13, 30, 0, '-4') ) },
  { time_range: TimeRange.new( DateTime.new(2013, 8, 10, 12, 30, 0, '-4'), DateTime.new(2013, 8, 10, 13, 30, 0, '-4') ) },
  { time_range: TimeRange.new( DateTime.new(2013, 8, 12, 12, 30, 0, '-4'), DateTime.new(2013, 8, 12, 13, 30, 0, '-4') ) },
  { time_range: TimeRange.new( DateTime.new(2013, 8, 13, 12, 30, 0, '-4'), DateTime.new(2013, 8, 13, 13, 30, 0, '-4') ) },
  { time_range: TimeRange.new( DateTime.new(2013, 8, 14, 12, 30, 0, '-4'), DateTime.new(2013, 8, 14, 13, 30, 0, '-4') ) },
  { time_range: TimeRange.new( DateTime.new(2013, 8, 15, 12, 30, 0, '-4'), DateTime.new(2013, 8, 15, 13, 30, 0, '-4') ) },
  { time_range: TimeRange.new( DateTime.new(2013, 8, 16, 12, 30, 0, '-4'), DateTime.new(2013, 8, 16, 13, 30, 0, '-4') ) },
  { time_range: TimeRange.new( DateTime.new(2013, 8, 17, 12, 30, 0, '-4'), DateTime.new(2013, 8, 17, 13, 30, 0, '-4') ) },
  { time_range: TimeRange.new( DateTime.new(2013, 8, 22, 12, 30, 0, '-4'), DateTime.new(2013, 8, 22, 13, 30, 0, '-4') ) },
  { time_range: TimeRange.new( DateTime.new(2013, 8, 8, 12, 30, 0, '-4'), DateTime.new(2013, 8, 8, 13, 30, 0, '-4') ) },
  { time_range: TimeRange.new( DateTime.new(2013, 8, 9, 12, 30, 0, '-4'), DateTime.new(2013, 8, 9, 13, 30, 0, '-4') ) },
  { time_range: TimeRange.new( DateTime.new(2013, 8, 10, 12, 30, 0, '-4'), DateTime.new(2013, 8, 10, 13, 30, 0, '-4') ) },
  { time_range: TimeRange.new( DateTime.new(2013, 8, 12, 12, 30, 0, '-4'), DateTime.new(2013, 8, 12, 13, 30, 0, '-4') ) },
  { time_range: TimeRange.new( DateTime.new(2013, 8, 13, 12, 30, 0, '-4'), DateTime.new(2013, 8, 13, 13, 30, 0, '-4') ) },
  { time_range: TimeRange.new( DateTime.new(2013, 8, 14, 12, 30, 0, '-4'), DateTime.new(2013, 8, 14, 13, 30, 0, '-4') ) },
  { time_range: TimeRange.new( DateTime.new(2013, 8, 15, 12, 30, 0, '-4'), DateTime.new(2013, 8, 15, 13, 30, 0, '-4') ) },
  { time_range: TimeRange.new( DateTime.new(2013, 8, 16, 12, 30, 0, '-4'), DateTime.new(2013, 8, 16, 13, 30, 0, '-4') ) },
  { time_range: TimeRange.new( DateTime.new(2013, 8, 17, 12, 30, 0, '-4'), DateTime.new(2013, 8, 17, 13, 30, 0, '-4') ) },
  { time_range: TimeRange.new( DateTime.new(2013, 8, 12, 12, 30, 0, '-4'), DateTime.new(2013, 8, 12, 13, 30, 0, '-4') ) },
  { time_range: TimeRange.new( DateTime.new(2013, 8, 22, 12, 30, 0, '-4'), DateTime.new(2013, 8, 22, 13, 30, 0, '-4') ) }
])


statistics_attrs = [
  {participants: 12, prep_time: 30, unit: 'LSA', physical_items: false, completed: true, notes: "That was not a fun class"},
  {participants: 20, prep_time: 30, unit: 'DLPS', physical_items: true,completed: true, notes: "That was a fun class"},
  {participants: 12, completed: false, notes: "That was a boring class"},
  {participants: 12, completed: false, notes: "That was an exciting class"},
  {participants: 12, completed: false, notes: "That was a stressful class"},
  {participants: 12, completed: false, notes: "That was a redundant class"}
]



workshops = Workshop.create([
  {title: 'Searching Databases', accepted: true, archived: false, class_related: false},
  {title: 'English 232 001', accepted: false, subject: "English", course: "232", section: "001", class_related: true},
  {title: 'Advanced Search', accepted: true, class_related: false},
  {title: 'Psych 100 001', accepted: true, subject: "Psych", course: "100", section: "001", archived: true, class_related: true},
  {title: 'History 120 001', accepted: true, subject: "History", course: "120", section: "001", archived: false, class_related: true},
  {title: 'Amcult 320 003', accepted: false, subject: "Amcult", course: "320", section: "003", archived: false, class_related: true},
  {title: 'English 300 011', accepted: false, subject: "English", course: "300", section: "001", class_related: true},
  {title: 'Citations', accepted: true, class_related: false},
  {title: 'Astro 100 001', accepted: true, subject: "Astro", course: "100", section: "001", archived: false, class_related: true},
  {title: 'Math 120 001', accepted: true, subject: "Math", course: "120", section: "001", archived: false, class_related: true},
  {title: 'Anthro 320 003', accepted: false, subject: "Anthro", course: "320", section: "003", archived: false, class_related: true},
  {title: 'Turning Pages', accepted: true,  archived: false, class_related: false},
  {title: 'Spanish 232 001', accepted: false, subject: "Spanish", course: "232", section: "001", class_related: true}
])


w = workshops[0]
w.scheduled_time = TimeRange.from_parts("2013-06-11", "10:00 AM", "10:30 AM")
w.contact_user = users[8]
w.requested_by = users[8]
w.session_type = SessionType.first
w.managed_by = users[4]
w.instructors += [users[4], users[5]]
w.topics += Topic.find([1,2,4])
w.requested_times += [requested_times[0], requested_times[1]]
w.statistics.update_attributes statistics_attrs[2]
w.save

w = workshops[1]
w.scheduled_time = TimeRange.from_parts("2013-07-11", "11:00 AM", "12:30 PM")
w.contact_user = users[7]
w.requested_by = users[4]
w.session_type = SessionType.first
w.instructors << users[5]
w.topics += Topic.find([2,3])
w.requested_times = [requested_times[2]]
w.statistics.update_attributes statistics_attrs[3]
w.save

w = workshops[2]
w.scheduled_time = TimeRange.from_parts("2013-08-11", "10:00 AM", "10:30 AM")
w.contact_user = users[7]
w.requested_by = users[2]
w.session_type = SessionType.first
w.location = locations[0]
w.topics += [Topic.first]
w.requested_times = [requested_times[3]]
w.statistics.update_attributes statistics_attrs[4]
w.save

w = workshops[3]
w.scheduled_time = TimeRange.from_parts("2013-06-30", "09:00 AM", "10:30 AM")
w.contact_user = users[9]
w.requested_by = users[3]
w.topics += Topic.find([2,3])
w.statistics.update_attributes statistics_attrs[1]
w.requested_times += [requested_times[4], requested_times[5]]
w.save

w = workshops[4]
w.scheduled_time = TimeRange.from_parts("2013-07-07", "02:30 PM", "03:30 PM")
w.contact_user = users[2]
w.requested_by = users[4]
w.statistics.update_attributes statistics_attrs[0]
w.requested_times += [requested_times[6], requested_times[7]]
w.save

w = workshops[5]
w.contact_user = users[4]
w.requested_by = users[7]
w.topics += Topic.find([5,6])
w.requested_times += [requested_times[8], requested_times[9]]
w.statistics.update_attributes statistics_attrs[5]
w.save

w = workshops[6]
w.contact_user = users[7]
w.requested_by = users[4]
w.session_type = SessionType.first
w.instructors << users[5]
w.topics += [Topic.first]
w.requested_times = [requested_times[10]]
w.statistics.update_attributes statistics_attrs[3]
w.save

w = workshops[7]
w.contact_user = users[7]
w.requested_by = users[5]
w.session_type = SessionType.first
w.location = locations[0]
w.topics += [Topic.first]
w.requested_times = [requested_times[11]]
w.statistics.update_attributes statistics_attrs[4]
w.save

w = workshops[8]
w.contact_user = users[9]
w.requested_by = users[3]
w.topics += Topic.find([1,5])
w.statistics.update_attributes statistics_attrs[1]
w.requested_times += [requested_times[12], requested_times[13]]
w.save

w = workshops[9]
w.contact_user = users[2]
w.requested_by = users[9]
w.statistics.update_attributes statistics_attrs[0]
w.requested_times += [requested_times[14], requested_times[15]]
w.save

w = workshops[10]
w.contact_user = users[4]
w.requested_by = users[1]
w.requested_times += [requested_times[16], requested_times[17]]
w.statistics.update_attributes statistics_attrs[5]
w.save

w = workshops[11]
w.contact_user = users[2]
w.requested_by = users[2]
w.session_type = SessionType.first
w.instructors += [users[2], users[4]]
w.topics += [Topic.first]
w.requested_times += [requested_times[18], requested_times[19]]
w.statistics.update_attributes statistics_attrs[5]
w.save

w = workshops[12]
w.contact_user = users[4]
w.requested_by = users[4]
w.session_type = SessionType.first
w.instructors << users[6]
w.topics += [Topic.first]
w.requested_times = [requested_times[20]]
w.statistics.update_attributes statistics_attrs[4]
w.save

cc1 = CalendarConfig.new :name => 'client_id', :value => '34376286559-abvofin2v6sl4ggseisfp037jjje1sq0.apps.googleusercontent.com'
cc1.save
cc2 = CalendarConfig.new :name => 'client_secret', :value => '8g99IS-pTpv5LhnN0nqUritO'
cc2.save
cc3 = CalendarConfig.new :name => 'api_key', :value => 'AIzaSyDEBVpB45NKGTEAOGxHItVW8p4blMGUQTo'
cc3.save
cc4 = CalendarConfig.new :name => 'instruction_calendar_id', :value => 'gn5lbpohn745hjrckb4qi70kdc@group.calendar.google.com'
cc4.save
cc5 = CalendarConfig.new :name => 'disclaimer', :value => 'This calendar item is bogus. It is from a fake calendar used for testing of the Workshop Scheduling application. Please disregard.  If you find these bogus calendar events annoying, you can contact ltig-staff <ltig-staff@umich.edu> and we will do our best to make them stop.'
cc5.save
