require 'set'
require 'tsort'



Rails.application.eager_load!

class Hash
  include TSort
  alias tsort_each_node each_key
  def tsort_each_child(node, &block)
    fetch(node).each(&block)
  end
end

def process_string(str)
  if str.nil?
    str = ""
  end
  str.strip.dump
end

def process_datetime(dt)
  dt.strftime("DateTime.new(%Y, %-m, %-d, %-H, %-M, 0, '-4')")
end

def process_number(num)
  if num.nil?
    num = 0
  end
  num
end

def process_boolean(b)
  if b.nil?
    b = false
  end
  b
end

def process_timerange(tr)
  "TimeRange.new( #{process_datetime(tr.start_time)}, #{process_datetime(tr.end_time)} )"
end

def expand_list list, list_name, map
  result = Set.new
  items = Set.new list
  list.each do |item|
    unless map[item][list_name].blank?
      items = items + expand_list(map[item][list_name], list_name, map)
    end
  end
  items.to_a
end

update_not_create = ['Statistics']

mass_assignables = {
  'InstructorAssignment.user_id' => "", 
  'InstructorAssignment.workshop_id' => "", 
  'EmailRecipient.comment_id' => "", 
  'EmailRecipient.user_id' => "",
  'WorkshopTopic.topic_id' => "", 
  'WorkshopTopic.workshop_id' => "",
  'SubChoicePick.session_sub_choice_id' => "", 
  'SubChoicePick..workshop_id' => "",
  'Statistics.workshop_id' => ""
}
skip_classes = [
  'WorkshopEvent', 'InstructorAssignment', 'Attachment', 'EmailRecipient' 
]
skip_methods = [
  'id', 'created_at', 'updated_at', 'crypted_password', 'salt', 
  'access_token', 'refresh_token', 'records_per_page', 'duration_minutes',
  'start_time', 'scheduled_time_start_time', 'scheduled_time_end_time',
  'prep_time', 'time_range_start_time', 'time_range_end_time'
]

index_names = {
  'category' => 'TopicCategory',
  'contact_user' => 'User',
  'requested_by' => 'User',
  # 'location' => 'Location',
  # 'session_type' => 'SessionType',
  # 'evaluation' => 'Evaluation',
  # 'registration' => 'Registration',
  # 'workshop' => 'Workshop',
  # 'user' => 'User',
  # 'comment' => 'Comment',
  'managed_by' => 'User',
  'statistics' => 'Statistics'
}

list = []
map = {}
compositions = {}
classes = {}
instances = {}
methods = {}

puts "building list of classes to export"
ActiveRecord::Base.descendants.each do |k|
  unless skip_classes.include? k.name
    classes[k.name] = k.new

    if map[k.name].nil?
      map[k.name] = Set.new
    end

    if methods[k.name].nil?
      methods[k.name] = {}
    end

    if compositions[k.name].nil?
      compositions[k.name] = {}
    end


    # if a belongs_to b --> b is before a, a is after b
    # if a has_one b or a has_many b --> a is before b, b is after a

    k.reflect_on_all_associations.each do |assn|
      begin
        puts "  #{k.name}.#{assn.name}  ==> #{assn.macro} #{assn.class_name}"
        if "#{assn.macro}" == "belongs_to"
          map[k.name] << "#{assn.class_name}"
          methods[k.name]["#{assn.name}_id"] = "#{assn.class_name}"
        else
          # puts "  #{k.name}.#{assn.name}  ==> #{assn.macro} #{assn.class_name}"
        end
        # item['depends_on'] << "#{assn.class_name}"
      rescue => e
        puts "  #{k.name} ==> #{assn} --> ERROR #{e}"
      end
    end

    k.reflect_on_all_aggregations.each do |assn|
      begin
        if "#{assn.macro}" == 'composed_of'
          compositions[k.name][assn.name] = assn.class_name
        else
          #item['before'] << "#{assn.class_name}"
          puts "  #{k.name}.#{assn.name}  --> #{assn.macro} #{assn.class_name}"
        end
      rescue => e
        puts "  #{k.name} --> #{assn} --> ERROR "
      end
    end
  end
end
puts "done building list of classes to export"


# puts "\n\n#{map}\n\n"
sorted_list = map.tsort
# puts "#{sorted_list}\n\n"

now = Time.new
dump = File.new(now.strftime('load_data-%Y-%m-%d-%H-%M.rb'), 'w')

sorted_list.each do |kname|
  # puts kname
  if kname.include? '.'
    puts "....... dotted class-name: kname"
  else
    unless instances.has_key? kname
      instances[kname] = {}
    end
    object = classes[kname]
    k = object.class
    # methods
    object.attribute_names.each do |m|
      # puts "    #{m} #{k.columns_hash[m].type}"
      unless skip_methods.include? m
        unless methods[kname][m].present?
          if k.columns_hash[m].blank?
            puts "   -----------------------------> no entry in columns_hash for #{m}"
          else
            methods[kname][m] = "#{k.columns_hash[m].type}"
          end
        end
      end
      # js_file.write("  this.#{m} = jsonObj.#{m};\n")
    end

    count = k.all.count
    list_name = "#{kname.underscore.pluralize}"
    puts "exporting #{count} #{list_name}"
    if count > 0
      dump << "puts \"importing #{count} #{list_name}\"\n"
      processing_stats = false
      if kname == "Statistics"
        processing_stats = true
      else
        dump << "#{list_name} = #{kname}.create([\n"
      end
      ref_string = ""
      k.all.each_with_index do |item, i|
        stats_str = ""
        unless processing_stats
          if i > 0
            dump << "\n  },{\n"
          else 
            dump << "  {\n"
          end  
        end
        instances[kname][item.id] = i
        m_index = 0
        need_to_save = false
        methods[kname].each do |m_name, m_type|
          begin
            # puts "    ==> #{m_index} #{m_name} #{m_type}"
            value = item.__send__(m_name.to_sym)
            unless value.nil?
              if processing_stats
                unless m_name.end_with? '_id'
                  case m_type.to_sym
                  when :integer
                    stats_str << "obj.update_attribute :#{m_name.to_sym}, #{process_number(value)}\n"
                  when :string
                    stats_str << "obj.update_attribute :#{m_name.to_sym}, #{process_string(value)}\n"
                  when :text
                    stats_str << "obj.update_attribute :#{m_name.to_sym}, #{process_string(value)}\n"
                  when :datetime
                    stats_str << "obj.update_attribute :#{m_name.to_sym}, #{process_datetime(value)}\n"
                  when :boolean
                    stats_str << "obj.update_attribute :#{m_name.to_sym}, #{process_boolean(value)}\n"
                  else
                    stats_str << "obj.update_attribute :#{m_name.to_sym}, #{value}\n"
                  end
                end
              elsif m_name.end_with? '_event_id'
                if m_index > 0
                  dump << ",\n"
                end
                m_index = m_index + 1
                dump << "    #{m_name}: #{process_string(value)}"
              elsif m_name.end_with? '_id'
                p_name = m_name.slice(0, m_name.length - 3)
                if index_names.has_key? p_name
                  index_name = index_names[p_name]
                else
                  index_name = p_name.camelize.singularize
                end
                if mass_assignables.has_key? "#{kname}.#{m_name}"
                  if m_index > 0
                    dump << ",\n"
                  end
                  m_index = m_index + 1
                  dump << "    #{p_name}: #{index_name.underscore.pluralize}[#{instances[index_name][value]}]"
                else
                  ref_string << "#{list_name}[#{i}].#{p_name} = #{index_name.underscore.pluralize}[#{instances[index_name][value]}]\n"
                  need_to_save = true
                end
              else
                if m_index > 0
                  dump << ",\n"
                end
                m_index = m_index + 1
                case m_type.to_sym
                when :integer
                  dump << "    #{m_name}: #{process_number(value)}"
                when :string
                  dump << "    #{m_name}: #{process_string(value)}"
                when :text
                  dump << "    #{m_name}: #{process_string(value)}"
                when :datetime
                  dump << "    #{m_name}: #{process_datetime(value)}"
                when :boolean
                  dump << "    #{m_name}: #{process_boolean(value)}"
                else
                  dump << "    #{m_name}: #{value}"
                end
              end
            end
          rescue => e
            puts "ERROR in methods loop -- #{kname}.#{m_name} -- #{m_type} #{value} -- #{e}"
          end
        end
        unless compositions[kname].blank?
          compositions[kname].each do |m_name, class_name|
            value = item.__send__(m_name.to_sym)
           case class_name
            when 'TimeRange'
              if value.valid?
                if m_index > 0
                  dump << ",\n"
                end
                dump << "    #{m_name}: #{process_timerange(value)}"
              end
            else
            end
            m_index = m_index + 1
          end
        end
        if processing_stats
          unless stats_str.blank?
            dump << "obj = Statistics.find_by_workshop_id #{item.workshop.id}\n#{stats_str}\n"
          end
        elsif need_to_save
          ref_string << "#{list_name}[#{i}].save\n"
        end 
      end
      unless processing_stats
        dump << "\n  }\n"
        if ref_string.blank?
          dump << "])\n\n"
        else
          dump << "])\n\n# --------- references\n#{ref_string}\n\n"
        end
      end
      dump << "puts \"imported #{instances[kname].length} #{list_name}\"\n\n"
      puts "exported #{instances[kname].length} #{list_name}"
    end

  end

end

dump.close
