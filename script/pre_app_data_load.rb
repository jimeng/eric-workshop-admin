# This script was run in pieces in production to add sessions which were taught before the app was
# available, and get them into the system for reporting purposes. Data from a spreadsheet sent by Doreen.
# First instructors and requesters who were not already in production are added because they are referenced
# in the sessions. After sessions are added, the created_at date is updated to be the scheduled date
# so they will show up in appropriate order on the instruction report.

# This should not be run again - it is here just for reference.

# Once this has been run, then the start and end workshop id was found and the following was run to update
# the created_at date
#   ActiveRecord::Base.connection.execute("UPDATE workshops SET created_at = workshops.scheduled_time_start_time
#   where (id >  '500')")

# add users (requesters) who aren't already in the system
User.create([
{ firstname: 'Anne', lastname: 'Gwozdek', uniqname: 'agwozdek', role: 'requester' },
{ firstname: 'Anna', lastname: 'Wieck', uniqname: 'amwieck', role: 'requester' },
{ firstname: 'Chris', lastname: 'Black', uniqname: 'ckblack', role: 'requester' },
{ firstname: 'Charlene', lastname: 'Ruloff', uniqname: 'cruloff', role: 'requester' },
{ firstname: 'Emily', lastname: 'Goedde', uniqname: 'egoedde', role: 'requester' },
{ firstname: 'Fred', lastname: 'Becchetti', uniqname: 'fdb', role: 'requester' },
{ firstname: 'Jane', lastname: 'Fulcher', uniqname: 'fulcherj', role: 'requester' },
{ firstname: 'Kersta', lastname: 'Gustafson', uniqname: 'gkersta', role: 'requester' },
{ firstname: 'Jeremy', lastname: 'Lapham', uniqname: 'jerelaph', role: 'requester' },
{ firstname: 'Jason', lastname: 'Mitchell ', uniqname: 'jwmitche', role: 'requester' },
{ firstname: 'Kelly', lastname: 'Everett', uniqname: 'kneve', role: 'requester' },
{ firstname: 'Lawrence', lastname: 'La Fountain-Stokes', uniqname: 'lawrlafo', role: 'requester' },
{ firstname: 'Lionel', lastname: 'Beasley', uniqname: 'lionbeas', role: 'requester' },
{ firstname: 'Andrew', lastname: 'Maynard', uniqname: 'maynarda', role: 'requester' },
{ firstname: 'Alecia', lastname: 'McCall', uniqname: 'mccallam', role: 'requester' },
{ firstname: 'Melody', lastname: 'Pugh', uniqname: 'melodypu', role: 'requester' },
{ firstname: 'Molly', lastname: 'Kleinman', uniqname: 'mollyak', role: 'requester' },
{ firstname: 'Chris', lastname: 'Poulsen', uniqname: 'poulsen', role: 'requester' },
{ firstname: 'Rachel', lastname: 'Chapman', uniqname: 'rachellc', role: 'requester' },
{ firstname: 'Rockefeller', lastname: 'Oteng', uniqname: 'roteng', role: 'requester' },
{ firstname: 'Ruth', lastname: 'Gonzalez', uniqname: 'rumgone', role: 'requester' }
])

# add instructors who aren't in the system
User.create([
{ firstname: 'Marisa', lastname: 'Conte', uniqname: 'meese', role: 'instructor' },
{ firstname: 'Barbara', lastname: 'Shipman', uniqname: 'bshipman', role: 'instructor' },
{ firstname: 'Gurpreet', lastname: 'Rana', uniqname: 'preet', role: 'instructor' },
{ firstname: 'Mark', lastname: 'MacEachern', uniqname: 'markmac', role: 'instructor' },
{ firstname: 'Kate', lastname: 'Saylor', uniqname: 'kmacdoug', role: 'instructor' },
{ firstname: 'Jamie', lastname: 'Vander Broek', uniqname: 'jlausch', role: 'instructor' },
{ firstname: 'Nandita', lastname: 'Mani', uniqname: 'nanditam', role: 'instructor' },
{ firstname: 'Merle', lastname: 'Rosenzweig', uniqname: 'oriley', role: 'instructor' },
{ firstname: 'Sara', lastname: 'Samuel', uniqname: 'henrysm', role: 'instructor' }
])

# add session requests
workshop = Workshop.create([
  {title: 'Introduction to Advanced Enquiry in the Humanities', subject: "Rackham", course: "525", section: "001", class_related: true,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.scheduled_time = TimeRange.from_parts("2013-07-01", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 1, 6, 00, 0, '-4'), DateTime.new(2013, 7, 1, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.contact_user = User.where(:uniqname => 'lawrlafo').first
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'jherrada')
w.instructors += User.where(:uniqname => 'pdaub')

stats_attrs = [ { participants: 18,  unit: 'Special Collections Library', physical_items: true, completed: true } ]
w.statistics.update_attributes stats_attrs[0]

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

w.save
# -------------------
workshop = Workshop.create([
  {title: 'Introduction to Advanced Enquiry in the Humanities', subject: "Rackham", course: "525", section: "000", class_related: true,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.scheduled_time = TimeRange.from_parts("2013-07-01", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 1, 6, 00, 0, '-4'), DateTime.new(2013, 7, 1, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.contact_user = User.where(:uniqname => 'lawrlafo').first
w.session_type = SessionType.where(:name => 'Campus Outreach Session').first
w.instructors += User.where(:uniqname => 'jherrada')
w.instructors += User.where(:uniqname => 'pdaub')

stats_attrs = [ { participants: 15,  unit: 'Special Collections Library', physical_items: true, completed: true } ]
w.statistics.update_attributes stats_attrs[0]

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

w.save

# -------------------
workshop = Workshop.create([
  {title: 'UM Medical School New Faculty Orientation',  class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.scheduled_time = TimeRange.from_parts("2013-07-01", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 1, 6, 00, 0, '-4'), DateTime.new(2013, 7, 1, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Campus Outreach Session').first
w.instructors += User.where(:uniqname => 'dlauseng')
w.instructors += User.where(:uniqname => 'markmac')
w.instructors += User.where(:uniqname => 'preet')
w.instructors += User.where(:uniqname => 'judsmith')

stats_attrs = [ { participants: 70,  unit: 'Taubman Health Sciences Library', physical_items: false, completed: true,
  notes: 'Information Table available during orientation sessions from 7:30 a.m. to 1 p.m., available to all attendees.  Direct conversations with 22 people.'} ]

w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Orientation for pediatrics fellows',  class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.contact_user = User.where(:uniqname => 'rachellc').first
w.scheduled_time = TimeRange.from_parts("2013-07-02", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 2, 6, 00, 0, '-4'), DateTime.new(2013, 7, 2, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'meese')

stats_attrs = [ { participants: 20,  unit: 'Taubman Health Sciences Library', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Orientation for New Obstetrics & Gynecology Fellows',  class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-07-02", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 2, 6, 00, 0, '-4'), DateTime.new(2013, 7, 2, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'bshipman')

stats_attrs = [ { participants: 6,  unit: 'Taubman Health Sciences Library', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Resources for Ghanaian (KNUST) Emergency Nursing Students',  class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.contact_user = User.where(:uniqname => 'jerelaph').first
w.scheduled_time = TimeRange.from_parts("2013-07-03", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 3, 6, 00, 0, '-4'), DateTime.new(2013, 7, 3, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'preet')

stats_attrs = [ { participants: 3,  unit: 'Taubman Health Sciences Library', physical_items: false, completed: true,
  notes: 'KNUST/KATH nursing students - part of the Ghana Emergency Medicine Collaborative (as funded by MEPI)'} ]
w.statistics.update_attributes stats_attrs[0]

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Towards a Free Revolutionary Art', subject: "HA", course: "394", section: "001",  class_related: true,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.contact_user = User.where(:uniqname => 'amwieck').first
w.scheduled_time = TimeRange.from_parts("2013-07-03", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 3, 6, 00, 0, '-4'), DateTime.new(2013, 7, 3, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'deirdres')

stats_attrs = [ { participants: 10,  unit: 'Fine Arts Library', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

w.save

# -------------------
workshop = Workshop.create([
  {title: 'MMSS:  Michigan Math & Science Summer program',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.contact_user = User.where(:uniqname => 'fdb').first
w.scheduled_time = TimeRange.from_parts("2013-07-09", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 9, 6, 00, 0, '-4'), DateTime.new(2013, 7, 9, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Campus Outreach Session').first
w.instructors += User.where(:uniqname => 'pdaub')

stats_attrs = [ { participants: 19,  unit: 'Special Collections Library', physical_items: true, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Ghanaian Emerg Med resident orientation to Global Health Information Sources',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.contact_user = User.where(:uniqname => 'roteng').first
w.scheduled_time = TimeRange.from_parts("2013-07-09", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 9, 6, 00, 0, '-4'), DateTime.new(2013, 7, 9, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'preet')

stats_attrs = [ { participants: 2,  unit: 'Taubman Health Sciences Library', physical_items: false, completed: true,
  notes: '3rd year residents from KNUST, Kumasi, Ghana here under funding of the Ghana Emergency Medicine Collaborative, a partnership between UM Dept of Emergency Medicine and KNUST, Kumasi.'} ]
w.statistics.update_attributes stats_attrs[0]

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

w.save

# -------------------
workshop = Workshop.create([
  {title: 'EPID 757', subject: "EPID", course: "757", section: "000",  class_related: true,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-07-09", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 9, 6, 00, 0, '-4'), DateTime.new(2013, 7, 9, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'markmac')

stats_attrs = [ { participants: 15,  unit: 'Taubman Health Sciences Library', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Orientation for Pharmacy residents',  class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-07-10", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 10, 6, 00, 0, '-4'), DateTime.new(2013, 7, 10, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'cshannon')

stats_attrs = [ { participants: 11,  unit: 'Taubman Health Sciences Library', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Citation Management for Education',  class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.contact_user = User.where(:uniqname => 'mollyak').first
w.scheduled_time = TimeRange.from_parts("2013-07-10", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 10, 6, 00, 0, '-4'), DateTime.new(2013, 7, 10, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'mgomis')

stats_attrs = [ { participants: 10,  unit: 'Academic Technologies Group', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

w.save

# -------------------
workshop = Workshop.create([
  {title: 'New Student Orientation',  class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.scheduled_time = TimeRange.from_parts("2013-07-11", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 11, 6, 00, 0, '-4'), DateTime.new(2013, 7, 11, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'yangjw')

stats_attrs = [ { participants: 150,  unit: 'Clark Library', physical_items: false, completed: true, notes: '( 3 session, approximately 50 per session)'} ]
w.statistics.update_attributes stats_attrs[0]

scpick1 = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])
scpick2 = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'Online').pluck(:id).first} ])
scpick3 = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'Video').pluck(:id).first} ])

w.save

# -------------------
workshop = Workshop.create([
  {title: 'School of Nursing Graduate Student Orientation',  class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.contact_user = User.where(:uniqname => 'rumgone').first
w.scheduled_time = TimeRange.from_parts("2013-07-12", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 12, 6, 00, 0, '-4'), DateTime.new(2013, 7, 12, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'kmacdoug')
w.instructors += User.where(:uniqname => 'dlauseng')

stats_attrs = [ { participants: 90,  unit: 'Taubman Health Sciences Library', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

w.save

# -------------------
workshop = Workshop.create([
  {title: 'COMPLIT 122 201', subject: "COMPLIT", course: "122", section: "201",  class_related: true,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.contact_user = User.where(:uniqname => 'egoedde').first
w.scheduled_time = TimeRange.from_parts("2013-07-16", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 16, 6, 00, 0, '-4'), DateTime.new(2013, 7, 16, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'jooehrli')

stats_attrs = [ { participants: 90,  unit: 'Undergraduate Library', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Working with Images: Photoshop Skills for Practical Use',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-07-17", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 17, 6, 00, 0, '-4'), DateTime.new(2013, 7, 17, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Open Workshop').first
w.instructors += User.where(:uniqname => 'hammbr')

stats_attrs = [ { participants: 8,  unit: 'Academic Technologies Group', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]


w.save

# -------------------
workshop = Workshop.create([
  {title: 'Gone Google! Tips and Tricks for Email, Part 2',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-07-17", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 17, 6, 00, 0, '-4'), DateTime.new(2013, 7, 17, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Open Workshop').first
w.instructors += User.where(:uniqname => 'lasutch')
w.instructors += User.where(:uniqname => 'juliew')

stats_attrs = [ { participants: 8,  unit: 'Academic Technologies Group| Desktop Support Services', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]


w.save

# -------------------
workshop = Workshop.create([
  {title: 'Creating Professional Looking Conference Posters',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-07-18", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 18, 6, 00, 0, '-4'), DateTime.new(2013, 7, 18, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Open Workshop').first
w.instructors += User.where(:uniqname => 'rpettigr')


stats_attrs = [ { participants: 14,  unit: 'Academic Technologies Group', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: '-- no title provided --',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.contact_user = User.where(:uniqname => 'ckblack').first
w.scheduled_time = TimeRange.from_parts("2013-07-22", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 22, 6, 00, 0, '-4'), DateTime.new(2013, 7, 22, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Campus Outreach Session').first
w.instructors += User.where(:uniqname => 'kdown')


stats_attrs = [ { participants: 5,  unit: 'Graduate Library Reference', physical_items: false, completed: true,
  notes: 'This was a grants session for the grants support staff at the Medical School.'} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'EndNote Basics',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-07-22", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 22, 6, 00, 0, '-4'), DateTime.new(2013, 7, 22, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Open Workshop').first
w.instructors += User.where(:uniqname => 'htuckett')


stats_attrs = [ { participants: 18,  unit: 'Graduate Library Reference', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'RefWorks UROP Summer',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-07-23", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 23, 6, 00, 0, '-4'), DateTime.new(2013, 7, 23, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'mgomis')

stats_attrs = [ { participants: 26,  unit: 'Academic Technologies Group', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Literature Review in the Social Sciences',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-07-23", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 23, 6, 00, 0, '-4'), DateTime.new(2013, 7, 23, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Open Workshop').first
w.instructors += User.where(:uniqname => 'mfreelan')
w.instructors += User.where(:uniqname => 'htuckett')


stats_attrs = [ { participants: 16,  unit: 'Graduate Library Reference', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Ghana Emerg Med nursing - open access resource training',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.contact_user = User.where(:uniqname => 'jerelaph').first
w.scheduled_time = TimeRange.from_parts("2013-07-24", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 24, 6, 00, 0, '-4'), DateTime.new(2013, 7, 24, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'preet')

stats_attrs = [ { participants: 2,  unit: 'Taubman Health Sciences Library', physical_items: false, completed: true,
  notes: '2nd session with this group through involvement with the Ghana Emergency Medicine Collaborative.  Am also training visiting Ghana Emerg Med residents.  Future sessions planned will be more integrated.  Possibility of faculty training as well.'} ]
w.statistics.update_attributes stats_attrs[0]

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

w.save

# -------------------
workshop = Workshop.create([
  {title: 'New Student Orientation',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-07-25", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 25, 6, 00, 0, '-4'), DateTime.new(2013, 7, 25, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'yangjw')
w.instructors += User.where(:uniqname => 'jooehrli')

stats_attrs = [ { participants: 115,  unit: 'Taubman Health Sciences Library', physical_items: false, completed: true,
  notes: '(3 seessions, 1st:41, 2nd:44, 3rd:30)'} ]
w.statistics.update_attributes stats_attrs[0]

scpick1 = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])
scpick2 = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'Online').pluck(:id).first} ])
scpick3 = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'Video').pluck(:id).first} ])

w.save

# -------------------
workshop = Workshop.create([
  {title: 'ENGLISH 125 214', subject: "ENGLISH", course: "125", section: "214",  class_related: true,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.contact_user = User.where(:uniqname => 'lionbeas').first
w.scheduled_time = TimeRange.from_parts("2013-07-25", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 25, 6, 00, 0, '-4'), DateTime.new(2013, 7, 25, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'ehamstra')
w.instructors += User.where(:uniqname => 'jlausch')

stats_attrs = [ { participants: 16,  unit: 'Undergraduate Library', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

w.save

# -------------------
workshop = Workshop.create([
  {title: 'WordPress for Future Public Health Leaders Students',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.contact_user = User.where(:uniqname => 'gkersta').first
w.scheduled_time = TimeRange.from_parts("2013-07-26", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 26, 6, 00, 0, '-4'), DateTime.new(2013, 7, 26, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'mgomis')

stats_attrs = [ { participants: 48,  unit: 'Academic Technologies Group', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

w.save

# -------------------
workshop = Workshop.create([
  {title: 'More About Dreamweaver and Cascading Style Sheets',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-07-29", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 29, 6, 00, 0, '-4'), DateTime.new(2013, 7, 29, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Open Workshop').first
w.instructors += User.where(:uniqname => 'lasutch')

stats_attrs = [ { participants: 78,  unit: 'Academic Technologies Group', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Tour of Hatcher and Shapiro', subject: "ELI", course: "994", section: "001",  class_related: true,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.contact_user = User.where(:uniqname => 'melodyu').first
w.scheduled_time = TimeRange.from_parts("2013-07-30", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 30, 6, 00, 0, '-4'), DateTime.new(2013, 7, 30, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'gduque')

stats_attrs = [ { participants: 12,  unit: 'Undergraduate Library', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'GENESIS - Best Program',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.contact_user = User.where(:uniqname => 'mccallam').first
w.scheduled_time = TimeRange.from_parts("2013-07-31", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 7, 31, 6, 00, 0, '-4'), DateTime.new(2013, 7, 31, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Campus Outreach Session').first
w.instructors += User.where(:uniqname => 'kmacdoug')
w.instructors += User.where(:uniqname => 'dlauseng')

stats_attrs = [ { participants: 14,  unit: 'Taubman Health Sciences Library', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Using Word 2010 Effectively for Your Dissertation for PC Users',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-08-06", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 8, 6, 6, 00, 0, '-4'), DateTime.new(2013, 8, 6, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Open Workshop').first
w.instructors += User.where(:uniqname => 'mgomis')


stats_attrs = [ { participants: 14,  unit: 'Academic Technologies Group', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Tour for visiting Librarians',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-08-06", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 8, 6, 6, 00, 0, '-4'), DateTime.new(2013, 8, 6, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Community Session').first
w.instructors += User.where(:uniqname => 'jlausch')


stats_attrs = [ { participants: 4,  unit: 'Undergraduate Library', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Anesthesiology Research Rotation',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-08-07", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 8, 7, 6, 00, 0, '-4'), DateTime.new(2013, 8, 7, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'markmac')


stats_attrs = [ { participants: 4,  unit: 'Taubman Health Sciences Library', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Kickstart Your Portfolio Facilitation Series: ePortfolio Tools Overview',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-08-07", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 8, 7, 6, 00, 0, '-4'), DateTime.new(2013, 8, 7, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Open Workshop').first
w.instructors += User.where(:uniqname => 'mgomis')
w.instructors += User.where(:uniqname => 'clluke')

stats_attrs = [ { participants: 8,  unit: 'Academic Technologies Group| Technology Integration Group', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'M1 Orientation',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-08-08", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 8, 8, 6, 00, 0, '-4'), DateTime.new(2013, 8, 8, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'markmac')
w.instructors += User.where(:uniqname => 'nanditam')


stats_attrs = [ { participants: 40,  unit: 'Taubman Health Sciences Library', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Photoshop Tools and Effects for Enhancing Images',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-08-08", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 8, 8, 6, 00, 0, '-4'), DateTime.new(2013, 8, 8, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Open Workshop').first
w.instructors += User.where(:uniqname => 'hammbr')


stats_attrs = [ { participants: 9,  unit: 'Academic Technologies Group', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: "Dental Hygiene Orientation:  Degree completion & Master's e-learning programs",   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.contact_user = User.where(:uniqname => 'agwozdek').first
w.scheduled_time = TimeRange.from_parts("2013-08-14", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 8, 14, 6, 00, 0, '-4'), DateTime.new(2013, 8, 14, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'cshannon')


stats_attrs = [ { participants: 10,  unit: 'Taubman Health Sciences Library', physical_items: false, completed: true,
  notes: "we used journals, but not those from our collection, as it's too difficult to bring them now."} ]
w.statistics.update_attributes stats_attrs[0]

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

w.save

# -------------------
workshop = Workshop.create([
  {title: "UM Library Basics (for international students and visiting scholars)",   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-08-16", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 8, 16, 6, 00, 0, '-4'), DateTime.new(2013, 8, 16, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'yangjw')
w.instructors += User.where(:uniqname => 'gduque')

stats_attrs = [ { participants: 30,  unit: 'Clark Library', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

scpick1 = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])
scpick2 = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'Online').pluck(:id).first} ])
w.save

# -------------------
workshop = Workshop.create([
  {title: 'Creating Web Pages with Dreamweaver CS6',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-08-16", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 8, 16, 6, 00, 0, '-4'), DateTime.new(2013, 8, 16, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Open Workshop').first
w.instructors += User.where(:uniqname => 'lasutch')


stats_attrs = [ { participants: 19,  unit: 'Academic Technologies Group', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'CTools for Course Websites',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-08-19", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 8, 19, 6, 00, 0, '-4'), DateTime.new(2013, 8, 19, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Open Workshop').first
w.instructors += User.where(:uniqname => 'dperpich')


stats_attrs = [ { participants: 10,  unit: 'Academic Technologies Group| Technology Integration Group', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'CTools Gradebook and Assignments',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-08-19", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 8, 19, 6, 00, 0, '-4'), DateTime.new(2013, 8, 19, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Open Workshop').first
w.instructors += User.where(:uniqname => 'dperpich')


stats_attrs = [ { participants: 11,  unit: 'Academic Technologies Group| Technology Integration Group', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Kickstart Your Portfolio Facilitation Workshop: WordPress',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-08-20", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 8, 20, 6, 00, 0, '-4'), DateTime.new(2013, 8, 20, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Open Workshop').first
w.instructors += User.where(:uniqname => 'mgomis')
w.instructors += User.where(:uniqname => 'clluke')

stats_attrs = [ { participants: 5,  unit: 'Academic Technologies Group| Technology Integration Group', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Google Sites 101',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-08-20", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 8, 20, 6, 00, 0, '-4'), DateTime.new(2013, 8, 20, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Open Workshop').first
w.instructors += User.where(:uniqname => 'clluke')

stats_attrs = [ { participants: 5,  unit: 'Academic Technologies Group| Technology Integration Group', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Working with Images: Photoshop Skills for Practical Use',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-08-21", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 8, 21, 6, 00, 0, '-4'), DateTime.new(2013, 8, 21, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Open Workshop').first
w.instructors += User.where(:uniqname => 'hammbr')

stats_attrs = [ { participants: 7,  unit: 'Academic Technologies Group| Technology Integration Group', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Duplicate Entry-Researchpalooza',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-08-21", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 8, 21, 6, 00, 0, '-4'), DateTime.new(2013, 8, 21, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Campus Outreach Session').first
w.instructors += User.where(:uniqname => 'oriley')

stats_attrs = [ { participants: 0,  unit: 'Taubman Health Sciences Library', physical_items: false, completed: true,
  notes: 'Not to be counted, duplicate entry for Researchpalooza.'} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Researchpalooza',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-08-21", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 8, 21, 6, 00, 0, '-4'), DateTime.new(2013, 8, 21, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Campus Outreach Session').first
w.instructors += User.where(:uniqname => 'oriley')

stats_attrs = [ { participants: 60,  unit: 'Taubman Health Sciences Library', physical_items: false, completed: true,
  notes: "THL hosted a table at the Medical School Office of Research's Researchpalooza"} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Library Research Tips & Tricks (Engineering)',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-08-21", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 8, 21, 6, 00, 0, '-4'), DateTime.new(2013, 8, 21, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Open Workshop').first
w.instructors += User.where(:uniqname => 'henrysm')

stats_attrs = [ { participants: 6,  unit: 'Art, Architecture & Engineering Library', physical_items: false, completed: true,
  notes: "Basic introduction to using the library followed by a tour of the Duderstadt Center."} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Library Basics (North Campus)',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-08-23", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 8, 23, 6, 00, 0, '-4'), DateTime.new(2013, 8, 23, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Campus Outreach Session').first
w.instructors += User.where(:uniqname => 'henrysm')

stats_attrs = [ { participants: 24,  unit: 'Art, Architecture & Engineering Library', physical_items: false, completed: true,
  notes: "International graduate student library orientation."} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Summer Graduate Nursing Student Orientation',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.contact_user = User.where(:uniqname => 'kneve').first
w.scheduled_time = TimeRange.from_parts("2013-08-27", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 8, 27, 6, 00, 0, '-4'), DateTime.new(2013, 8, 27, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'dlauseng')

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

stats_attrs = [ { participants: 14,  unit: 'Taubman Health Sciences Library', physical_items: false, completed: true,
  notes: "final catch-all session"} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'SON Undergrad "Catch-all" orientation',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.contact_user = User.where(:uniqname => 'kneve').first
w.scheduled_time = TimeRange.from_parts("2013-08-28", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 8, 28, 6, 00, 0, '-4'), DateTime.new(2013, 8, 28, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'kmacdoug')

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

stats_attrs = [ { participants: 17,  unit: 'Taubman Health Sciences Library', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Orientation to Library Services for Medical Student LCs',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-08-28", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 8, 28, 6, 00, 0, '-4'), DateTime.new(2013, 8, 28, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'cshannon')

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

stats_attrs = [ { participants: 170,  unit: 'Taubman Health Sciences Library', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Library resources in Environmental Health Sciences Dept orientation',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.contact_user = User.where(:uniqname => 'maynarda').first
w.scheduled_time = TimeRange.from_parts("2013-08-29", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 8, 29, 6, 00, 0, '-4'), DateTime.new(2013, 8, 29, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'cshannon')

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

stats_attrs = [ { participants: 50,  unit: 'Taubman Health Sciences Library', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Kinesiology Graduate Student Orientation',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.contact_user = User.where(:uniqname => 'crulof').first
w.scheduled_time = TimeRange.from_parts("2013-08-29", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 8, 29, 6, 00, 0, '-4'), DateTime.new(2013, 8, 29, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Campus Outreach Session').first
w.instructors += User.where(:uniqname => 'ehamstra')


stats_attrs = [ { participants: 20,  unit: 'Undergraduate Library', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Orientation for Earth and Environmental Sciences Graduate Students',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.contact_user = User.where(:uniqname => 'poulsen').first
w.scheduled_time = TimeRange.from_parts("2013-08-30", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 8, 30, 6, 00, 0, '-4'), DateTime.new(2013, 8, 30, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Campus Outreach Session').first
w.instructors += User.where(:uniqname => 'ltz')


stats_attrs = [ { participants: 13,  unit: 'Science Library', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'SPH Student Resource Fair',   class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-09-3", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 9, 3, 6, 00, 0, '-4'), DateTime.new(2013, 9, 3, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Campus Outreach Session').first
w.instructors += User.where(:uniqname => 'cshannon')
w.instructors += User.where(:uniqname => 'kmacdoug')
w.instructors += User.where(:uniqname => 'judsmith')

stats_attrs = [ { participants: 87,  unit: 'Taubman Health Sciences Library', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'MUSICOL 501', subject: "MUSICOL", course: "501", section: "000",   class_related: true,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.contact_user = User.where(:uniqname => 'fulcherj').first
w.scheduled_time = TimeRange.from_parts("2013-09-5", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 9, 5, 6, 00, 0, '-4'), DateTime.new(2013, 9, 5, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'imbesij')


stats_attrs = [ { participants: 6,  unit: 'Music Library', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

w.save

# -------------------
workshop = Workshop.create([
  {title: 'NURS 802 001', subject: "NURS", course: "802",  section: '001', class_related: true,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.contact_user = User.where(:uniqname => 'jwmitche').first
w.scheduled_time = TimeRange.from_parts("2013-09-06", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 9, 6, 6, 00, 0, '-4'), DateTime.new(2013, 9, 6, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'dlauseng')


stats_attrs = [ { participants: 8,  unit: 'Taubman Health Sciences Library', physical_items: false, completed: true,
  notes: 'Introduction to Epidemiology for 1st year DNP students - overview of literature searching in light of looking for epidemiological based literature'} ]
w.statistics.update_attributes stats_attrs[0]

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Cytoscape:  Going from Raw Data to a Publishable Image',  class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-09-09", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 9, 9, 6, 00, 0, '-4'), DateTime.new(2013, 9, 9, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Open Workshop').first
w.instructors += User.where(:uniqname => 'oriley')


stats_attrs = [ { participants: 3,  unit: 'Taubman Health Sciences Library', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'UMHS Nursing Poster Day',  class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]

w.scheduled_time = TimeRange.from_parts("2013-09-09", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 9, 9, 6, 00, 0, '-4'), DateTime.new(2013, 9, 9, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Campus Outreach Session').first
w.instructors += User.where(:uniqname => 'dlauseng')
w.instructors += User.where(:uniqname => 'kmacdoug')
w.instructors += User.where(:uniqname => 'bshipman')

stats_attrs = [ { participants: 23,  unit: 'Taubman Health Sciences Library', physical_items: false, completed: true,
  notes: 'Event runs over two days (9/9-9/10)'} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'Earth and Environmental Sciences New Grad Orientation Tour',  class_related: false,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.contact_user = User.where(:uniqname => 'poulsen').first
w.scheduled_time = TimeRange.from_parts("2013-09-10", "6:00 AM", "6:45 AM")
req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 9, 10, 6, 00, 0, '-4'), DateTime.new(2013, 9, 10, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'ltz')

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

stats_attrs = [ { participants: 3,  unit: 'Science Library', physical_items: true, completed: true,
  notes: 'Event runs over two days (9/9-9/10)'} ]
w.statistics.update_attributes stats_attrs[0]

w.save

# -------------------
workshop = Workshop.create([
  {title: 'NURS 862 001', subject: "NURS", course: "862",  section: '001', class_related: true,
    registration_setup: true, evaluation_setup: true, library_location_required: false, location_other: 'unknown'}
])

w = workshop[0]
w.contact_user = User.where(:uniqname => 'jwmitche').first
w.scheduled_time = TimeRange.from_parts("2013-09-12", "6:00 AM", "6:45 AM")

req_time = RequestedTime.create([
  { time_range: TimeRange.new( DateTime.new(2013, 9, 12, 6, 00, 0, '-4'), DateTime.new(2013, 9, 12, 6, 45, 0, '-4') ) }
])
w.requested_times += [req_time[0]]
w.session_type = SessionType.where(:name => 'Curriculum-Related').first
w.instructors += User.where(:uniqname => 'dlauseng')


stats_attrs = [ { participants: 4,  unit: 'Taubman Health Sciences Library', physical_items: false, completed: true} ]
w.statistics.update_attributes stats_attrs[0]

scpick = SubChoicePick.create([ { workshop_id: w.id, session_sub_choice_id: SessionSubChoice.where(:name => 'In Person').pluck(:id).first} ])

w.save

