# deprecated in favor of script/export_all.rb

topic_category_index = {}
topic_index = {}
session_type_index = {}
evaluation_index = {}
registration_index = {}
unit_index = {}
email_template_index = {}
user_index = {}
npref_index = {}
location_index = {}
requested_times_index = {}
workshop_index = {}

default_user = nil

def process_string(str)
  if str.nil?
    str = ""
  end
  str.strip.dump
end

def process_datetime(dt)
  dt.strftime("DateTime.new(%Y, %-m, %-d, %-H, %-M, 0, '-4')")
end

def process_number(num)
  if num.nil?
    num = 0
  end
  num
end

def process_boolean(b)
  if b.nil?
    b = false
  end
  b
end

now = Time.new
dump = File.new(now.strftime('load_data-%Y-%m-%d-%H-%M.rb'), 'w')
dump << "categories = TopicCategory.create([\n"
TopicCategory.all.each_with_index do |cat, i|
  dump << "  { name: #{process_string(cat.name)}, order: #{cat.order} }"
  if i == TopicCategory.all.count - 1
    dump << "\n"
  else
    dump << ",\n"
  end
  topic_category_index[cat.id] = i
end
dump << "])\n\ntopics = Topic.create([\n"
puts "TopicCategories exported. #{topic_category_index.length}"
tc_str = ""
Topic.all.each_with_index do |t, i|
  dump << "  { title: #{process_string(t.title)}, description: #{process_string(t.description)}, active: #{t.active} }"
  if i == Topic.all.count - 1
    dump << "\n"
  else
    dump << ",\n"
  end
  topic_index[t.id] = i
  tc_str << "topics[#{i}].category = categories[#{topic_category_index[t.category.id]}]\n"
end
dump << "])\n\n#{tc_str}\ntopics.each do |t|\n  t.save\nend\n\nsession_types = SessionType.create([\n"
puts "Topics exported. #{topic_index.length}"
SessionType.all.each_with_index do |st, i|
  dump << "  { name: #{process_string(st.name)}, description: #{process_string(st.description)}, active: #{st.active}}"
  if i == SessionType.all.count - 1
    dump << "\n"
  else
    dump << ",\n"
  end
  session_type_index[st.id] = i
end
dump << "])\n\nevaluations = Evaluation.create([\n"
puts "SessionTypes exported. #{session_type_index.length}"
Evaluation.all.each_with_index do |e, i|
  dump << "{ name: #{process_string(e.name)}, setup_url: #{process_string(e.setup_url)}, active: #{e.active}, eval_template_url: #{process_string(e.eval_template_url)}, eval_url_pattern: #{process_string(e.eval_url_pattern)}, eval_info_url: #{process_string(e.eval_info_url)} }"
  if i == Evaluation.all.count - 1
    dump << "\n"
  else
    dump << ",\n"
  end
  evaluation_index[e.id] = i
end
dump << "])\n\nregistrations = Registration.create([\n"
puts "Evaluations exported. #{evaluation_index.length}"
Registration.all.each_with_index do |r, i|
  dump << "  { name: #{process_string(r.name)}, setup_url: #{process_string(r.setup_url)}, active: #{r.active} }"
  if i == Registration.all.count - 1
    dump << "\n"
  else
    dump << ",\n"
  end
  registration_index[r.id] = i
end
dump << "])\n\nunits = Unit.create([\n"
puts "Registrations exported. #{registration_index.length}"
Unit.all.each_with_index do |u, i|
  dump << "  { name: #{process_string(u.name)} }"
  if i == Unit.all.count - 1
    dump << "\n"
  else
    dump << ",\n"
  end
  unit_index[u.id] = i
end
dump << "])\n\ntemplates = EmailTemplate.create([\n"
puts "Units exported. #{unit_index.length}"
EmailTemplate.all.each_with_index do |t, i|
  dump << "  { name: #{process_string(t.name)}, subject: #{process_string(t.subject)}, active: #{process_boolean(t.active)}, multiple: #{process_boolean(t.multiple)}, system_stats: #{process_boolean(t.system_stats)},\n"
  dump << "    body: #{process_string(t.body)} }"
  if i == EmailTemplate.all.count - 1
    dump << "\n"
  else
    dump << ",\n"
  end
  email_template_index[t.id] = i
end
dump << "])\n\nusers = User.create([\n"
puts "EmailTemplates exported. #{email_template_index.length}"
User.all.each_with_index do |u, i|
  dump << "  { firstname: #{process_string(u.firstname)}, lastname: #{process_string(u.lastname)}, uniqname: #{process_string(u.uniqname)}, role: #{process_string(u.role)}, active: #{process_boolean(u.active)}, calendar_url: #{process_string(u.calendar_url)}, unit: #{process_string(u.unit)} }"
  if i == User.all.count - 1
    dump << "\n"
  else
    dump << ",\n"
  end
  user_index[u.id] = i
  if 'jleasia' == u.uniqname
    default_user = i
  end
end
dump << "])\n\nlocations = Location.create([\n"
puts "Users exported. #{user_index.length}"
Location.all.each_with_index do |l, i|
  dump << "  { name: #{process_string(l.name)}, building: #{process_string(l.building)}, seats: #{process_number(l.seats)}, computers: #{process_number(l.computers)}, computer_type: #{process_string(l.computer_type)},\n"
  unless l.directions.blank?
    dump << "    directions: #{process_string(l.directions)}, "
  end
  dump << "    support_phone: #{process_string(l.support_phone)}, active: #{process_boolean(l.active)}, calendar_url: #{process_string(l.calendar_url)} }"
  if i == Location.all.count - 1
    dump << "\n"
  else
    dump << ",\n"
  end
  location_index[l.id] = i
end
dump << "])\n\nrequested_times = RequestedTime.create([\n"
puts "Locations exported. #{location_index.length}"
RequestedTime.all.each_with_index do |rt, i|
  dump << "  { time_range: TimeRange.new( #{process_datetime(rt.time_range.start_time)}, #{process_datetime(rt.time_range.end_time)} ) }"
  if i == RequestedTime.all.count - 1
    dump << "\n"
  else
    dump << ",\n"
  end
  requested_times_index[rt.id] = i
end
dump << "])\n\nworkshops = Workshop.create([\n"
puts "RequestedTimes exported. #{requested_times_index.length}"
w_str = ""
Workshop.all.each_with_index do |w,i|
  dump << "  { title: #{process_string(w.title)}, accepted: #{process_boolean(w.accepted)}, archived: #{process_boolean(w.archived)}, class_related: #{process_boolean(w.class_related)} }"
  if i == Workshop.all.count - 1
    dump << "\n"
  else
    dump << ",\n"
  end
  workshop_index[w.id] = i
  w_str << "w = workshops[#{i}]\n"
  if w.contact_user.blank? && w.requested_by.blank?
    puts "Request #{w.id} has no contact and no requester.  Using jleasia."
    w_str << "w.contact_user = users[#{default_user}]\n"
    w_str << "w.requested_by = users[#{default_user}]\n"
  elsif w.contact_user.blank?
    puts "Request #{w.id} has no contact.  Using requester."
    w_str << "w.contact_user = users[#{user_index[w.requested_by.id]}]\n"
    w_str << "w.requested_by = users[#{user_index[w.requested_by.id]}]\n"
  elsif w.requested_by.blank?
    puts "Request #{w.id} has no requester.  Using contact."
    w_str << "w.requested_by = users[#{user_index[w.contact_user.id]}]\n"
    w_str << "w.contact_user = users[#{user_index[w.contact_user.id]}]\n"
  else
    w_str << "w.requested_by = users[#{user_index[w.requested_by.id]}]\n"
    w_str << "w.contact_user = users[#{user_index[w.contact_user.id]}]\n"
  end

  unless w.session_type.blank?
    w_str << "w.session_type = session_types[#{session_type_index[w.session_type.id]}]\n"
  end
  w.requested_times.each do |rt|
    w_str << "w.requested_times << requested_times[#{requested_times_index[rt.id]}]\n"
  end
  # w.instructors.each do |instr|
  #   w_str << "w.instructors << User.find_by_uniqname(users[#{user_index[instr.id]}].uniqname)\n"
  # end
  w.topics.each do |t|
    w_str << "w.topics << topics[#{topic_index[t.id]}]\n"
  end
  if w.scheduled_time.present? && w.scheduled_time.valid?
    w_str << "w.scheduled_time = TimeRange.new( #{process_datetime(w.scheduled_time.start_time)}, #{process_datetime(w.scheduled_time.end_time)} )\n"
  end
  w_str << "w.library_location_required = #{w.library_location_required}\n"
  unless w.location.blank?
    w_str << "w.location = locations[#{location_index[w.location.id]}]\n"
  end
  unless w.location_other.blank?
    w_str << "w.location_other = #{process_string(w.location_other)}\n"
  end
  w_str << "w.subject = #{process_string(w.subject)}\n"
  w_str << "w.course = #{process_string(w.course)}\n"
  w_str << "w.section = #{process_string(w.section)}\n"
  w_str << "w.expected_attendance = #{process_number(w.expected_attendance)}\n"
  w_str << "w.scheduler_notes = #{process_string(w.scheduler_notes)}\n"
  w_str << "w.notes = #{process_string(w.notes)}\n"
  w_str << "w.requester_questions = #{process_string(w.requester_questions)}\n"
  w_str << "w.evaluation_setup = #{process_boolean(w.evaluation_setup)}\n"
  w_str << "w.registration_setup = #{process_boolean(w.registration_setup)}\n"
  w_str << "w.eval_results_url = #{process_string(w.eval_results_url)}\n"
  w_str << "w.deleted = #{process_boolean(w.deleted)}\n"
  unless w.ttcid.blank?
    w_str << "w.ttcid = #{w.ttcid}\n"
  end
  # w_str << "w.deleted = #{process_boolean(w.deleted)}\n"
  unless w.managed_by.blank?
    w_str << "w.managed_by = users[#{user_index[w.managed_by.id]}]\n"
  end

  # statistics
  s = w.statistics
  w_str << "w.statistics = Statistics.new( :completed => #{process_boolean(s.completed)}, :participants => #{process_number(s.participants)}, :physical_items => #{process_boolean(s.physical_items)},\n"
  w_str << "    :unit => #{process_string(s.unit)},\n    :notes => #{process_string(s.notes)} )\n"

  # attachments

  w_str << "w.save\n\n"


end
dump << "])\n\n#{w_str}\n\n"
puts "workshops exported. #{workshop_index.length}"
n = 0
np_str = ""
NotificationPreference.all.each do |np|
  if np.user.nil?
    # do nothing -- skip this invalid rule
  else
    dump << "rule = NotificationPreference.new( :notifier => #{process_string(np.notifier)}, :event_type => #{process_string(np.event_type)}, :scope => #{process_string(np.scope)}, :active => #{process_boolean(np.active)} )\n"
    dump << "rule.user = users[#{user_index[np.user_id]}]\n"
    unless np.workshop_id.nil?
      dump << "rule.workshop = workshops[#{workshop_index[np.workshop_id]}]\n"
    end
    dump << "rule.save\n"
    npref_index[np.id] = n
    n = n + 1
  end
end
dump << "\n\ncomments = Comment.create([\n"
puts "NotificationPreference rules exported. #{npref_index.length}"
comment_count = Comment.all.count
Comment.all.each_with_index do |c,i|
  dump << "  { subject: #{process_string(c.subject)},  user_id: users[#{user_index[c.user.id]}].id, content: #{process_string(c.content)}, workshop_id: workshops[#{workshop_index[c.workshop_id]}].id }"
  if i == comment_count - 1
    dump << "\n])\n\n"
  else
    dump << ",\n"
  end
end
puts "Comments exported. #{comment_count}"

ia_count = 0
InstructorAssignment.all.each do |ia|
  dump << "ia = InstructorAssignment.new( :user => users[#{user_index[ia.user.id]}], :workshop => workshops[#{workshop_index[ia.workshop_id]}], :order => #{process_number(ia.order)} )\nia.save\n"
  ia_count = ia_count + 1
end 
puts "InstructorAssignments exported. #{ia_count}"


#statistics_attrs = [
#  {participants: 12, prep_time: 30, unit: 'LSA', physical_items: false, completed: true, notes: "That was not a fun class"},



dump.close
