require 'rubygems'
require 'rufus/scheduler'
require 'eventmachine'

# This script uses EventMachine and Rufus::Scheduler to periodically remove old 
# Update objects.  This can be started with reails runner as follows:
#
#      rails r script/remove_old_updates.rb &

EM.run {

  # No more than X updates per person
  max_updates_per_person = 200
  # No updates more than N weeks old
  max_age_of_updates_in_weeks = 4
  # when to remove old updates (e.g. every day at 2 a.m. would be '0 2 * * *')
  cron_expression = '0 2 * * *'

  Rails.logger.info "\n---------------------------------------\nStarting Remove-Old-Updates Scheduler\n---------------------------------------\n"

  begin
    scheduler = Rufus::Scheduler.new(:thread_name => 'remove_old_updates')

    scheduler.cron cron_expression do

      t = Time.zone.now - max_age_of_updates_in_weeks.weeks

      # (more than #{ActionView::Helpers::DateHelper.time_ago_in_words  t} old)
      Rails.logger.info "\n---------------------------------------\nRemoving updates before #{t} (more than #{max_age_of_updates_in_weeks} weeks old) \n---------------------------------------\n"

      removed = 0
      Update.where("created_at < ?", t).each do |u|
        # Rails.logger.info "Removing: #{u.as_json}"
        u.destroy
        removed = removed + 1
      end

      Rails.logger.info "Removed #{removed} updates\n---------------------------------------\nEnforce limit of #{max_updates_per_person} updates per person\n---------------------------------------\n"

      removed = 0
      User.joins(:updates).having('count(updates.id) > ?', max_updates_per_person).group('updates.user_id').each do |user|
        Rails.logger.info "Removing excess updates for user: #{u.display_name}"
        Update.where(:user_id => user).order('created_at desc').offset(max_updates_per_person).each do |u|
          # Rails.logger.info "     ----> Delete: #{u.created_at}"
          u.destroy
          removed = removed + 1
        end
      end
      Rails.logger.info "Removed #{removed} updates\n---------------------------------------\n"
    end

  rescue => e1
    Rails.logger.warn "#{e1}"
  end

}
