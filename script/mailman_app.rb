# mailman_app.rb
begin
  ms = MailmanService.new
  ms.start
  ms.join
rescue => e
  ms.stop
  Rails.logger.info "mailman_app has died"
else
  ms.stop
  Rails.logger.info "Someone killed mailman_app"
ensure
  if ms.alive?
    ms.stop
    Rails.logger.info "I killed mailman_app with my own hands"
  else
    Rails.logger.info "I happened along and found mailman_app dead"
  end
end
