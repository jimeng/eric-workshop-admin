User.where(role: ['scheduler', 'admin']).each do |u|
	n = NotificationPreference.new(:event_type => 'workshop.create', :notifier => 'email', :scope => 'all')
	n.user = u 
	n.save
	n = NotificationPreference.new(:event_type => 'workshop.create', :notifier => 'ping', :scope => 'all')
	n.user = u 
	n.save
	n = NotificationPreference.new(:event_type => 'workshop.instructor.add', :notifier => 'ping', :scope => 'mine')
	n.user = u 
	n.save
	n = NotificationPreference.new(:event_type => 'workshop.instructor.remove', :notifier => 'ping', :scope => 'mine')
	n.user = u 
	n.save
	n = NotificationPreference.new(:event_type => 'workshop.room.add', :notifier => 'ping', :scope => 'mine')
	n.user = u 
	n.save
	n = NotificationPreference.new(:event_type => 'workshop.room.remove', :notifier => 'ping', :scope => 'mine')
	n.user = u 
	n.save
	n = NotificationPreference.new(:event_type => 'workshop.update', :notifier => 'ping', :scope => 'mine')
	n.user = u 
	n.save
	n = NotificationPreference.new(:event_type => 'email.received', :notifier => 'ping', :scope => 'all')
	n.user = u 
	n.save
	n = NotificationPreference.new(:event_type => 'email.sent', :notifier => 'ping', :scope => 'all')
	n.user = u 
	n.save
end
