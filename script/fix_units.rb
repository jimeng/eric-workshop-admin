count = 0
Statistics.all.each do |stats|
  unless stats.unit.blank?
    new_unit = stats.unit.gsub("Research - Science Engineering Clark & Data", "Research - Science, Engineering, Clark & Data").gsub("Learning & Teaching", "L&T")
    unless stats.unit == new_unit
      stats.update_attribute(:unit, new_unit)
      count = count + 1
    end
  end
end
puts "#{count} changes made"
