categories = TopicCategory.create([
  {name: 'Finding & Using Sources', order: 1},
  {name: 'Critical Evaluation of Sources', order: 2},
  {name: 'Citing Sources and Academic Integrity', order: 3},
  {name: 'Technology', order: 4}
])

topics = Topic.create([
  {title: 'Academic Integrity and Avoiding Plagiarism (50 min)', description: 'Explores the issue of academic integrity by looking at both intentional and unintentional plagiarism with some practice exercises to identify and correct patchwriting.', active: true},
  {title: 'Basic Image Manipulation using Photoshop (50 min)', description: 'Students learn the basics of Photoshop to manipulate images including lighting effects and combining multiple images into one.', active: true},
  {title: 'Citation Searching and Cited Reference Searching (20 min)', description: 'A detailed look at citation searching within Web of Science with an explanation of how citation searching fits into the research process. Pros and cons of citation searching will be discussed.', active: true},
  {title: 'Citation Management Software for 10 Citations or More (50 min)', description: 'Learn to use RefWorks, EndNote, or Mendeley to manage citations for your research projects. Includes hands-on experience exporting citations from databases.', active: true},
  {title: 'Cleaning Models for 3D Printing (90 min)', description: 'Learn how to prepare your materials for output on 3D printers.', active: true},
  {title: 'Creating Web Pages (50 min)', description: 'Students learn the basics of Google Sites (or other software product) to create a basic website for a class assignment.', active: true},
  {title: 'Data Visualization (90 min)', description: 'Learn to communicate information effectively using design strategies to visualize your data or explore software such as VisIt, Paraview or MATLAB.', active: true},
  {title: 'Designing Better Presentations (50 min)', description: 'Students learn key design principles to make better presentations (PowerPoint or Prezi) as well as tools they may not know such as using the Master Slide, adding SmartArt, working with video/audio, and using advanced animation to make a point.', active: true},
  {title: 'Designing Effective Posters (50 min)', description: 'Students learn the basics of a tool such as Illustrator or InDesign to make their poster and receive tips for creating effective and eye-catching layouts', active: true},
  {title: 'Digital Media Commons Resources - Daily Orientation (30 min)', description: 'Learn about the unique audio, video, and podcasting resources available at the Digital Media Commons in the Duderstadt Center on North campus.', active: true},
  {title: 'Discovering Digital Media Creation Services (20-60 min)', description: 'Introduction to the services offered by the Digital Media Commons (DMC) in digital media creation, 3d printing, motion capture, visualization, and more.', active: true},
  {title: 'Effective Video-based Assignments (50 min)', description: 'Students are introduced to tools that can be used to create video, learn the basics of storytelling using video, and are exposed to the wide variety of resources available to help them in this creation process.', active: true},
  {title: 'Evaluating Sources, interactive (10 min)', description: 'A discussion about information sources on the web, who can be authors on the Internet, and Wikipedia. Includes information about the publishing and peer-review process for articles and books and compares that to the publishing process online. Includes tips for identifying a source\'s author, their credibility, and the timeliness of a sources.', active: true},
  {title: 'Exploring the Map Collection (50 min)', description: 'Explore items in our map collection that support coursework and research assignments.', active: true},
  {title: 'Finding Articles and Searching Databases (20 min)', description: 'A brief introduction to a database of your choice. It will begin with an explanation of what a database is, how best to navigate to it, and a general description of the database. The class will do some simple keyword searches and there will be an explanation of how to find the full text of the entries in the results.', active: true},
  {title: 'Finding Books & Media in the Library Catalog (10 min)', description: 'A brief introduction to the Mirlyn library catalog that will explain the difference between a catalog and a database, will demonstrate how to look up a known item, how to check if the item is available, how to put a hold on an item, and how to look for a topic by keyword and how to focus your research results.', active: true},
  {title: 'Finding Government Information and Census Data (50 min)', description: 'Explore resources and tools to locate government and census data efficiently.', active: true},
  {title: 'GIS: Geographic Information Systems (50 min)', description: 'Learn to use geographic information systems to explore data and create new information.', active: true},
  {title: 'Introduction to Avoiding Plagiarism (5 min)', description: 'Brief mention of the issue of plagiarism and a quick look at the University Library page on academic integrity.', active: true},
  {title: 'Introduction to Blogging Tools (50 min)', description: 'Students are exposed to various tools used for blogging.', active: true},
  {title: 'Library Research for Graduate Students (30-90 min)', description: 'An introduction to library resources and services for graduate students. Includes an overview of research resources available in specific disciplines, resources for reference management, and sources for technology help with dissertations.', active: true},
  {title: 'Library Tour (please specify a library) (30 min)', description: '  Generally includes stops at the Circulation Desks, the Reference areas, technology centers, Serials/Microforms and the stacks. This can be tailored depending on the needs of the course.', active: true},
  {title: 'Scholarly vs. non-scholarly sources, interactive (15 min)', description: 'This activity provides hands-on experience looking at scholarly and non-scholarly journals and differentiating between the two. A discussion on why using scholarly literature to conduct research is important, and a guided exploration into Ulrich\'s Periodicals Directory (or other resource as appropriate) to determine the scholarly status of a title.', active: true},
  {title: 'Understanding and Using a Citation Style (15 min)', description: 'A hands-on exercise with a style (APA, MLA, Chicago, etc}, including how to read bibliographies and locate the text of the cited reference, how to cite a reference in-text and in bibliographies, and the importance of citing references correctly.', active: true},
  {title: 'Using Microsoft Word More Effectively (50 min)', description: 'Students learn to use Word more effectively and efficiently for longer papers, using headings, format painter and more.', active: true},
  {title: 'Using Special Collections and Archives (50 min)', description: 'We will tailor an instruction session for your course ranging from a general introduction to the resources that are available in the Special Collections Library to an in-depth presentation on specific materials and their historical contexts.', active: true}
])

topics[0].category = categories[2]
topics[1].category = categories[3]
topics[2].category = categories[0]
topics[3].category = categories[2]
topics[4].category = categories[3]
topics[5].category = categories[3]
topics[6].category = categories[3]
topics[7].category = categories[3]
topics[8].category = categories[3]
topics[9].category = categories[3]
topics[10].category = categories[3]
topics[11].category = categories[3]
topics[12].category = categories[1]
topics[13].category = categories[0]
topics[14].category = categories[0]
topics[15].category = categories[0]
topics[16].category = categories[0]
topics[17].category = categories[3]
topics[18].category = categories[2]
topics[19].category = categories[3]
topics[20].category = categories[0]
topics[21].category = categories[0]
topics[22].category = categories[1]
topics[23].category = categories[2]
topics[24].category = categories[3]
topics[25].category = categories[0]
topics.each do |t| 
  t.save
end

session_types = SessionType.create([
  {name: 'Curriculum-Related', description: 'Classes, tours, orientations, course-specific presentations, or any sessions offered to UM
    faculty and students that support the curriculum. E.g., English 125, UROP, incoming students in a department/program', active: true},
  {name: 'Campus Outreach Session', description: 'For a UM lab group, staff in a dept., faculty meeting, sorority, etc.', active: true},
  {name: 'Open Workshops', description: 'Sessions for UM folks but are not tied to a specific curriculum. Examples are learning to use RefWorks,
    PowerPoint, and other sessions that any UM folks can self-select to attend.', active: true},
  {name: 'Community Session', description: 'Sessions given to non-UM groups or individuals, such as donor presentations, tours, development events,
    or guest lecturing at some venue. These preseentations can occur on or off campus.', active: true},
  {name: 'Credit-bearing University Course', description: 'For-credit courses offered through a university school or college,
    such as UC 174, MENAS 250.', active: true}
])

session_sub_choices = SessionSubChoice.create([
  {name: 'In Person', active: true},
  {name: 'Online', active: true},
  {name: 'Video', active: true},
  {name: 'Distance Education', active:  true}
])

s = session_sub_choices[0]
s.session_type =  SessionType.first
s.save
s = session_sub_choices[1]
s.session_type = SessionType.first
s.save
s = session_sub_choices[2]
s.session_type = SessionType.first
s.save
s = session_sub_choices[3]
s.session_type = SessionType.first
s.save

evaluations = Evaluation.create([
  {name: 'Library Evals', setup_url: 'https://webapps.lsa.umich.edu/es_conf/app/', active: false},

  {name: 'um.Lessons', setup_url: 'https://lessons.ummu.umich.edu/2k/index.html', active: false},
  {name: 'A mailto link', setup_url: 'mailto:jimeng@umich.edu', active: false},
  {name: 'Other (describe in comments)', active: false},
  {name: 'Qualtrics Intro Session Form', setup_url: 'https://umichlib.qualtrics.com/ControlPanel/', eval_template_url: 'http://umichlib.qualtrics.com/SE/?SID=SV_5byGWBNCO1zVjRH', active: true,
    eval_url_pattern: "{{ workshop.evaluation.eval_template_url }}&sid=L{{ workshop.id }}&stitle={{ workshop.title }}&spresenters={% for instructor in workshop.instructors_list %}{{ instructor.uniqname }}{% if forloop.rindex0 > 0 %},{% endif %}{% endfor %}&starttime={{ workshop.scheduled_time.start_time | date: '%Y-%m-%d %-I:%M %P' }}"
    },
  {name: 'Qualtrics Tech Session Form', setup_url: 'https://umichlib.qualtrics.com/ControlPanel/', eval_template_url: 'http://umichlib.qualtrics.com/SE/?SID=SV_cDchMWANUqkxmGp', active: true,
    eval_url_pattern: "{{ workshop.evaluation.eval_template_url }}&sid=L{{ workshop.id }}&stitle={{ workshop.title }}&spresenters={% for instructor in workshop.instructors_list %}{{ instructor.uniqname }}{% if forloop.rindex0 > 0 %},{% endif %}{% endfor %}&starttime={{ workshop.scheduled_time.start_time | date: '%Y-%m-%d %-I:%M %P' }}"
  },
  {name: 'Qualtrics Advanced Session Form', setup_url: 'https://umichlib.qualtrics.com/ControlPanel/', eval_template_url: 'http://umichlib.qualtrics.com/SE/?SID=SV_6XaFPyS2qQJrewB', active: true,
    eval_url_pattern: "{{ workshop.evaluation.eval_template_url }}&sid=L{{ workshop.id }}&stitle={{ workshop.title }}&spresenters={% for instructor in workshop.instructors_list %}{{ instructor.uniqname }}{% if forloop.rindex0 > 0 %},{% endif %}{% endfor %}&starttime={{ workshop.scheduled_time.start_time | date: '%Y-%m-%d %-I:%M %P' }}"
  }
])

registrations = Registration.create([
  {name: 'TTC general', setup_url: 'http://ttc.iss.lsa.umich.edu/ttc/sessions/upcoming/', active: true},
  {name: 'TTC undergrad', setup_url: 'http://ttc.iss.lsa.umich.edu/undergrad/sessions/upcoming/', active: true},
  {name: 'TTC (email ISS)', setup_url: 'mailto:issinstruction@umich.edu', active: false}
])

units = Unit.create([
{name: 'Collections - Papyrology'},
{name: 'Collections - Preservation/Conservation'},
{name: 'Collections - Special Collections'},
{name: 'Collections - Technical Services'},
{name: 'Area Programs / Special Collections'},
{name: 'Library Information Technology Units'},
{name: 'Learning & Teaching - Academic Technology Group'},
{name: 'Learning & Teaching - Digital Media Commons'},
{name: 'Learning & Teaching - Learning Programs & Initiatives'},
{name: 'Learning & Teaching - Technology Integration Group'},
{name: 'Learning & Teaching - User Information & Discovery Services'},
{name: 'Michigan Publishing Units'},
{name: 'Operations Units'},
{name: 'Research - Arts & Humanities'},
{name: 'Research - Asia Library'},
{name: 'Research - International Studies'},
{name: 'Research - Science, Engineering & Data'},
{name: 'Research - Social Sciences'},
{name: 'Hatcher Reference'},
{name: 'Taubman Health Sciences Library'}
])

templates = EmailTemplate.create([
  { name: "eval_url", subject: "Evaluation URLs for your Library Workshop ({{ workshop.title }})", active: true, multiple: false, system_stats: false,
    body: "MLibrary Instructors will be presenting a workshop for you at this time:\r\n\r\n        {{ workshop.scheduled_time.display_time_range | replace: '&nbsp;&nbsp; ',' -- ' }}\r\n\r\nThe workshop will cover these topics:\r\n\r\n{% for topic in workshop.topics %}        {{ forloop.index }}: {{ topic.title }}\r\n{% endfor %}\r\nThe following URL allows participants to evaluate the workshop:\r\n\r\n        {{ workshop.short_eval_url }}\r\n\r\nPlease provide that URL to participants in the workshop and remind them to complete the evaluation there when the workshop has been completed.\r\n\r\nShould you need additional information about the workshop, you will find it here: {{ workshop.instructor_url }}" },
  { name: "new_request", subject: "{% capture count %}{{ workshops | size }}{% endcapture %}{{ count }} {% if count == '1' %}workshop{% else %}workshops{% endif %} added", active: true, multiple: true, system_stats: true,
    body: "{% capture count %}{{ workshops | size }}{% endcapture %}{{ count }} {% if count == '1' %}workshop{% else %}workshops{% endif %} added\r\n\r\n{% for workshop in workshops %}\r\n-------------------\r\nTitle: {{ workshop.title }}\r\nURL: {{ workshop.scheduler_url }}\r\nContact: {{ workshop.contact_user.display_name }} ({{ workshop.contact_user.uniqname }})\r\nRequested by: {{ workshop.requested_by.display_name }} ({{ workshop.requested_by.uniqname }})\r\n\r\nTopics:\r\n{% for topic in workshop.topics %}        {{ topic.title }}\r\n{% endfor %}\r\n{% if workshop.instructors_count > 0 %}Instructors:\r\n{% endif %}{% for instructor in workshop.instructors_list %}        {{ instructor.display_name }} ({{ instructor.uniqname }})\r\n{% endfor %}\r\n{% if workshop.requested_times_count > 0 %}Requested times:\r\n{% endif %}{% for rt in workshop.requested_times_list %}        {{ rt.time_range.display_time_range | replace: '&nbsp;&nbsp; ', ' -- ' }}\r\n{% endfor %}\r\n{% if workshop.notes and workshop.notes != '' %}Special requests:\r\n{{ workshop.notes }}{% endif %}\r\n{% endfor %}-------------------\r\n\r\nStatus update:\r\n        New requests:     {{ new_request_count }}\r\n        No time:          {{ no_time_count }}\r\n        No room:          {{ no_room_count }}\r\n        No instructor:    {{ no_instructor_count }}\r\n        Need stats:       {{ need_stats_count }}" },
  { name: "contact_user", subject: "Your Library Workshop Request ({{ workshop.title }})", active: true, multiple: false, system_stats: false,
    body: "Title: {{ workshop.title }}\r\nURL: {{ workshop.instructor_url }}\r\nRequested by: {{ workshop.requested_by.display_name }}\r\nContact: {{ workshop.contact_user.display_name }}\r\n\r\nTopics:\r\n{% for topic in workshop.topics %}        {{ forloop.index }}: {{ topic.title }}\r\n{% endfor %}\r\n{% if workshop.scheduled_time and workshop.scheduled_time.valid? %}Scheduled time: \r\n        {{ workshop.scheduled_time.display_time_range | replace:'&nbsp;&nbsp; ', ' -- ' }}\r\n{% elsif workshop.requested_times_count > 0 %}Requested times:\r\n{% for rt in workshop.requested_times_list %}        {{ rt.time_range.display_time_range | replace:'&nbsp;&nbsp; ', ' -- ' }}\r\n{% endfor %}{% endif %}\r\n{% if workshop.instructors_count > 0 %}Instructors:\r\n{% for instructor in workshop.instructors_list %}        {{ instructor.display_name }}\r\n{% endfor %}{% endif %}" },
  { name: "instructor", subject: "Library Workshop ({{ workshop.title }})", active: true, multiple: false, system_stats: false,
    body: "Title: {{ workshop.title }}\r\nURL: {{ workshop.instructor_url }}\r\nRequested by: {{ workshop.requested_by.display_name }}\r\nContact: {{ workshop.contact_user.display_name }}\r\n\r\nTopics:\r\n{% for topic in workshop.topics %}        {{ forloop.index }}: {{ topic.title }}\r\n{% endfor %}\r\n{% if workshop.scheduled_time and workshop.scheduled_time.valid? %}Scheduled time: \r\n        {{ workshop.scheduled_time.display_time_range | replace:'&nbsp;&nbsp; ', ' -- ' }}\r\n{% elsif workshop.requested_times_count > 0 %}Requested times:\r\n{% for rt in workshop.requested_times_list %}        {{ rt.time_range.display_time_range | replace:'&nbsp;&nbsp; ', ' -- ' }}\r\n{% endfor %}{% endif %}\r\n{% if workshop.instructors_count > 0 %}Instructors:\r\n{% for instructor in workshop.instructors_list %}        {{ instructor.display_name }}\r\n{% endfor %}{% endif %}" },
  { name: "eval_error_page", subject: "eval_error_page", active: true, multiple: false, system_stats: false,
    body: "<div class=\"row-fluid \">\r\n    <h2 class=\"text-center\">Evaluation Request</h2>\r\n    <div>\r\n      <p>\r\n        Your request to complete an evaluation for a Library Workshop could not be handled at this time. Here is some additional information:\r\n      </p>\r\n      <div class=\"well well-tzippy\">\r\n        {{ workshop.full_eval_url }}\r\n      </div>\r\n      <p>\r\n        If you see a URL there, please copy and paste it into your browser.  If you still are not able to complete the evaluation, please report the error.\r\n      </p>\r\n      <p>\r\n        If you do not see a URL above, please report the error.\r\n      </p>\r\n    </div>\r\n  </div>" },
  { name: "room_support", subject: "Support request for {{ workshop.location.name }} for {{ workshop.scheduled_time.display_time_range | replace:'&nbsp;&nbsp; ', ' -- ' }}", active: true, multiple: false, system_stats: false,
    body: "An instructional session is scheduled in {{ workshop.location.name }} at this time:\r\n   {{ workshop.scheduled_time.display_time_range | replace:'&nbsp;&nbsp; ', ' -- ' }}\r\nSupport will be needed for this session.  \r\n\r\nThe session title is \"{{ workshop.title }}.\"   \r\n{% if workshop.instructors_count > 1 %}The instructors of this session are {% for instructor in workshop.instructors_list %}{{ instructor.display_name }}{% if forloop.last %} and {% else %},{% endif %}{% endfor %}.{% elsif workshop.instructors_count > 0 %}The instructor of this session is {{ workshop.instructors_list.first.display_name }}.{% endif %}\r\n{% if workshop.requested_by == workshop.contact_user %}Our contact is {{ workshop.contact_user.display_name }}.{% else %}This session was requested by {{ workshop.requested_by.display_name }}, and our contact is {{ workshop.contact_user.display_name }}.{% endif %}" },
  { name: "instructor_eval_url", subject: "URLs for your Library Workshop ({{ workshop.title }})", active: true, multiple: false, system_stats: false,
    body: "The following URL allows participants to evaluate the workshop:\r\n\r\n        {{ workshop.short_eval_url }}\r\n\r\nPlease provide that URL to participants in the workshop and remind them to complete the evaluation there when the workshop has been completed.\r\n\r\nShould you need additional information about the workshop, you will find it here: {{ workshop.instructor_url }}" },
  { name: "workshop.create", subject: "{% capture count %}{{ workshops | size }}{% endcapture %}{{ count }} {% if count == '1' %}workshop{% else %}workshops{% endif %} added", active: true, multiple: true, system_stats: true,
    body: "{% capture count %}{{ workshops | size }}{% endcapture %}{{ count }} {% if count == '1' %}workshop{% else %}workshops{% endif %} added\r\n\r\n{% for workshop in workshops %}\r\n-------------------\r\nTitle: {{ workshop.title }}\r\nURL: {{ workshop.scheduler_url }}\r\nContact: {{ workshop.contact_user.display_name }} ({{ workshop.contact_user.uniqname }})\r\nRequested by: {{ workshop.requested_by.display_name }} ({{ workshop.requested_by.uniqname }})\r\n\r\nTopics:\r\n{% for topic in workshop.topics %}        {{ topic.title }}\r\n{% endfor %}\r\n{% if workshop.instructors_count > 0 %}Instructors:\r\n{% endif %}{% for instructor in workshop.instructors_list %}        {{ instructor.display_name }} ({{ instructor.uniqname }})\r\n{% endfor %}\r\n{% if workshop.requested_times_count > 0 %}Requested times:\r\n{% endif %}{% for rt in workshop.requested_times_list %}        {{ rt.time_range.display_time_range | replace: '&nbsp;&nbsp; ', ' -- ' }}\r\n{% endfor %}\r\n{% if workshop.notes and workshop.notes != '' %}Special requests:\r\n{{ workshop.notes }}{% endif %}\r\n{% endfor %}-------------------\r\n\r\nStatus update:\r\n        New requests:     {{ new_request_count }}\r\n        No time:          {{ no_time_count }}\r\n        No room:          {{ no_room_count }}\r\n        No instructor:    {{ no_instructor_count }}\r\n        Need stats:       {{ need_stats_count }}" },
  { name: "workshop.instructor.add", subject: "Instructors Removed for Library Workshop ({{ workshop.title }})", active: true, multiple: false, system_stats: false,
    body: "Title: {{ workshop.title }}\r\nURL: {{ workshop.instructor_url }}\r\nRequested by: {{ workshop.requested_by.display_name }}\r\nContact: {{ workshop.contact_user.display_name }}\r\n\r\nTopics:\r\n{% for topic in workshop.topics %}        {{ forloop.index }}: {{ topic.title }}\r\n{% endfor %}\r\n{% if workshop.scheduled_time and workshop.scheduled_time.valid? %}Scheduled time: \r\n        {{ workshop.scheduled_time.display_time_range | replace:'&nbsp;&nbsp; ', ' -- ' }}\r\n{% elsif workshop.requested_times_count > 0 %}Requested times:\r\n{% for rt in workshop.requested_times_list %}        {{ rt.time_range.display_time_range | replace:'&nbsp;&nbsp; ', ' -- ' }}\r\n{% endfor %}{% endif %}\r\n{% if workshop.instructors_count > 0 %}Instructors:\r\n{% for instructor in workshop.instructors_list %}        {{ instructor.display_name }}\r\n{% endfor %}{% else %}No instructors.{% endif %}" },
  { name: "email.sent", subject: "An email has been sent about a library workshop ({{ workshop.title }})", active: true, multiple: false, system_stats: false,
    body: "An email has been sent about a library workshop ({{ workshop.title }})" },
  { name: "email.received", subject: "An email has been received about a library workshop ({{ workshop.title }})", active: true, multiple: false, system_stats: false,
    body: "An email has been received about a library workshop ({{ workshop.title }})" },
  { name: "workshop.instructor.remove", subject: "Instructors Added for Library Workshop ({{ workshop.title }})", active: true, multiple: false, system_stats: false,
    body: "Title: {{ workshop.title }}\r\nURL: {{ workshop.instructor_url }}\r\nRequested by: {{ workshop.requested_by.display_name }}\r\nContact: {{ workshop.contact_user.display_name }}\r\n\r\nTopics:\r\n{% for topic in workshop.topics %}        {{ forloop.index }}: {{ topic.title }}\r\n{% endfor %}\r\n{% if workshop.scheduled_time and workshop.scheduled_time.valid? %}Scheduled time: \r\n        {{ workshop.scheduled_time.display_time_range | replace:'&nbsp;&nbsp; ', ' -- ' }}\r\n{% elsif workshop.requested_times_count > 0 %}Requested times:\r\n{% for rt in workshop.requested_times_list %}        {{ rt.time_range.display_time_range | replace:'&nbsp;&nbsp; ', ' -- ' }}\r\n{% endfor %}{% endif %}\r\n{% if workshop.instructors_count > 0 %}Instructors:\r\n{% for instructor in workshop.instructors_list %}        {{ instructor.display_name }}\r\n{% endfor %}{% else %}No instructors.{% endif %}" },
  { name: "workshop.room.add", subject: "Room Assigned for Library Workshop ({{ workshop.title }})", active: true, multiple: false, system_stats: false,
    body: "Title: {{ workshop.title }}\r\nURL: {{ workshop.instructor_url }}\r\nRequested by: {{ workshop.requested_by.display_name }}\r\nContact: {{ workshop.contact_user.display_name }}\r\n\r\nTopics:\r\n{% for topic in workshop.topics %}        {{ forloop.index }}: {{ topic.title }}\r\n{% endfor %}\r\n{% if workshop.scheduled_time and workshop.scheduled_time.valid? %}Scheduled time: \r\n        {{ workshop.scheduled_time.display_time_range | replace:'&nbsp;&nbsp; ', ' -- ' }}\r\n{% elsif workshop.requested_times_count > 0 %}Requested times:\r\n{% for rt in workshop.requested_times_list %}        {{ rt.time_range.display_time_range | replace:'&nbsp;&nbsp; ', ' -- ' }}\r\n{% endfor %}{% endif %}\r\n{% if workshop.instructors_count > 0 %}Instructors:\r\n{% for instructor in workshop.instructors_list %}        {{ instructor.display_name }}\r\n{% endfor %}{% else %}No instructors.{% endif %}" },
  { name: "workshop.room.remove", subject: "Room Unassigned for Library Workshop ({{ workshop.title }})", active: true, multiple: false, system_stats: false,
    body: "Title: {{ workshop.title }}\r\nURL: {{ workshop.instructor_url }}\r\nRequested by: {{ workshop.requested_by.display_name }}\r\nContact: {{ workshop.contact_user.display_name }}\r\n\r\nTopics:\r\n{% for topic in workshop.topics %}        {{ forloop.index }}: {{ topic.title }}\r\n{% endfor %}\r\n{% if workshop.scheduled_time and workshop.scheduled_time.valid? %}Scheduled time: \r\n        {{ workshop.scheduled_time.display_time_range | replace:'&nbsp;&nbsp; ', ' -- ' }}\r\n{% elsif workshop.requested_times_count > 0 %}Requested times:\r\n{% for rt in workshop.requested_times_list %}        {{ rt.time_range.display_time_range | replace:'&nbsp;&nbsp; ', ' -- ' }}\r\n{% endfor %}{% endif %}\r\n{% if workshop.instructors_count > 0 %}Instructors:\r\n{% for instructor in workshop.instructors_list %}        {{ instructor.display_name }}\r\n{% endfor %}{% else %}No instructors.{% endif %}" },
  { name: "workshop.update", subject: "Request Updated for Library Workshop ({{ workshop.title }})", active: true, multiple: false, system_stats: false,
    body: "Title: {{ workshop.title }}\r\nURL: {{ workshop.instructor_url }}\r\nRequested by: {{ workshop.requested_by.display_name }}\r\nContact: {{ workshop.contact_user.display_name }}\r\n\r\nTopics:\r\n{% for topic in workshop.topics %}        {{ forloop.index }}: {{ topic.title }}\r\n{% endfor %}\r\n{% if workshop.scheduled_time and workshop.scheduled_time.valid? %}Scheduled time: \r\n        {{ workshop.scheduled_time.display_time_range | replace:'&nbsp;&nbsp; ', ' -- ' }}\r\n{% elsif workshop.requested_times_count > 0 %}Requested times:\r\n{% for rt in workshop.requested_times_list %}        {{ rt.time_range.display_time_range | replace:'&nbsp;&nbsp; ', ' -- ' }}\r\n{% endfor %}{% endif %}\r\n{% if workshop.instructors_count > 0 %}Instructors:\r\n{% for instructor in workshop.instructors_list %}        {{ instructor.display_name }}\r\n{% endfor %}{% else %}No instructors.{% endif %}" }
])
