# inst_fmt.py used to format a csv file of instructors so that they can be loaded into the users table

import sys

debug = False
if len(sys.argv) > 1 and sys.argv[1] == 'debug':
	debug = True

# open the file, read a line of the form
# row = 'firstname lastname uniqname role'


sourcefile = raw_input('Enter source file name that contains csv separated first,last,uniqname,role: ')

num_lines = sum(1 for line in open(sourcefile))

if debug:
	print(num_lines)

try:
	f = open(sourcefile)

except:
	print('')
	print('Could not find file ' + sourcefile),
	exit()


# read each row in the  file

totalrows = 0
row = ''
stuff = []
stuff2 = []
for row in f:
	totalrows = totalrows + 1
	# lets check for proper format, output the row, row number if a problem and
	# quit so the input file can be corrected
	try:
		user = row.split(',')
		firstname = user[0]
		lastname = user[1]
		uniqname = user[2]
		calendar_url = uniqname + '@umich.edu'
		role = user[3]

		stuff2.append("userx = User.new :firstname => '" + firstname + "', :lastname => '" + lastname + "', :uniqname => '" + uniqname
		+ "', :calendar_url => '" + calendar_url + "', :role => '" + role + "' \nuserx.save\n")

		stuff.append("{ firstname: '" + firstname + "', lastname: '" + lastname + "', uniqname: '" + uniqname
		+ "', calendar_url: '" + calendar_url + "', role: '" + role + "' }")

		if totalrows != num_lines :
			stuff.append(",\n")
		else :
			stuff.append("\n])")

	except:
			print('')
			print('Something is wrong with the format. Each row of the file')
			print('each row should be 4 values separated by spaces')
			print('firstname lastname uniqname role')
			print('')
			print('The problem was]) encountered at row ' + str(totalrows))
			print('and the row in question is: ' + row)
			exit()

f.close()

try:
	f = open('libinstructors.seed.sql', 'w')
except:
	print('')
	print('Was not able to open file libinstructors.seed.sql for writing!'),
	exit()

# output rows like
# 0-4: xx  (where xx is value of bucket[0]
# 4-8: yy   and yy is value of bucket[1], etc.
# 8-12: zz ...
f.write('users = User.create([\n')

for i in range(0, len(stuff)):
	try:
		f.write(stuff[i])
	except:
		print('')
		print('There was a problem writing to libinstructors.seed.sql')
		print('This is what I was trying to write: ' + str(stuff[i]))
		print('')
		exit()

f.close()
print('\n\nThe file libinstructors.seed.sql can be used in the seeds.rb file to load instructors\n')

try:
	f = open('libscript.txt', 'w')
except:
	print('')
	print('Was not able to open file libscript.txt for writing!'),
	exit()

# output rows like
# 0-4: xx  (where xx is value of bucket[0]
# 4-8: yy   and yy is value of bucket[1], etc.
# 8-12: zz ...

for i in range(0, len(stuff2)):
	try:
		f.write(stuff2[i])
	except:
		print('')
		print('There was a problem writing to libscript.txt')
		print('This is what I was trying to write: ' + str(stuff2[i]))
		print('')
		exit()

f.close()
print('The file libscript.txt can be used in the rails console to load instructors')
# lets tell them how many rows were read
print('')
print(str(totalrows) + ' lines were processed')
