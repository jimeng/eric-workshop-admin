require 'active_support/inflector'

class Notifier < ActionMailer::Base

  default from: "ulib-workshopadmin@umich.edu"

  def email_contact(recipients, from, subject, body, workshop_id = nil)
    @body = body
    unless recipients.nil? || recipients.empty?
      list = []
      recipients.each do |user|
        list << address_with_name(user)
      end
      mail(:to => list, :from => address_with_name(from), :subject => "#{subject} (ws:#{workshop_id})", :reply_to => "ulib-workshopadmin@umich.edu")
    end
  end

  def new_request_email(recipients, subject, body, workshop_ids = [])
    Rails.logger.debug "Notifier.new_request_email \n#{subject}\n#{body}"
    unless recipients.empty?
      list = []
      recipients.each do |user|
        list << address_with_name(user)
      end
      list = list.sort.uniq

      @body = body

      mail(:to => list, :from => "ulib-workshopadmin@umich.edu", :subject => subject, :reply_to => "ulib-workshopadmin@umich.edu")
    end
  end

  private

  def address_with_name(user)
    return "#{user.display_name} <#{user.email_address}>"
  end

end
