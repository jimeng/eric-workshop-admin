class TopicCategoriesController < ApplicationController

  load_and_authorize_resource :except => [:name]

  include SimpleDataTables
  accept_data_filter :all, :ordered
  default_data_filter :ordered
  alias_data_mapping :topic_count, :topic_count
  alias_data_mapping :action_links, :category_action_links

  # GET /topic_categories
  # GET /topic_categories.json
  def index
    @head_title = "Topic Categories - #{@head_title}"
    Rails.logger.debug "TopicCategoriesController.index"
    @topic_categories = TopicCategory.ordered

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: mapped_json, 'data-filter' => 'ordered' }
    end
  end

  def choices
    Rails.logger.debug "TopicCategoriesController.choices"
    @choices = []
    TopicCategory.ordered.each do |category|
      unless category.topics.blank?
        category.topics.each do |topic|
          choice = { 'value' => "#{topic.category.id}_#{topic.id}", 'text' => topic.title }
          @choices << choice
        end
      end
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @choices }
    end
  end

  def reorder
    @head_title = "Reorder Topic Categories - #{@head_title}"
    @topic_categories = TopicCategory.ordered

    respond_to do |format|
      format.html # reorder.html.erb
      format.json { render json: @topic_categories }
    end
  end

  def order
    Rails.logger.debug "TopicCategoriesController.update_order #{params}"
    if params[:item].present?
      items = params[:item] 
    end
    Rails.logger.debug "TopicCategoriesController.update_order #{items}"
    if items.present?
      index = 1
      items.each.with_index do |item, index|
        TopicCategory.find(item.to_i).update_attribute :order, index + 1
      end
    end

    respond_to do |format|
      format.html { redirect_to @topic_category, notice: "We're working on saving the new order" }
      format.json { render json: mapped_json, 'data-filter' => 'ordered' }
    end
  end

  # GET /topic_categories/1
  # GET /topic_categories/1.json
  def show
    @head_title = "Topic Category - #{@head_title}"
    @topic_category = TopicCategory.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @topic_category }
    end
  end

  # GET /topic_categories/new
  # GET /topic_categories/new.json
  def new
    @head_title = "New Topic Category - #{@head_title}"
    @topic_category = TopicCategory.new
    @categories = TopicCategory.ordered
    @submit_form_path = topic_categories_path
    @submit_form_method = 'post'
    Rails.logger.debug "TopicCategoriesController.new #{@submit_form_method} #{@submit_form_path}"
 
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @topic_category }
    end
  end

  # GET /topic_categories/1/edit
  def edit
    @head_title = "Edit Topic Category - #{@head_title}"
    @topic_category = TopicCategory.find(params[:id])
    @submit_form_path = topic_category_path(@topic_category)
    @submit_form_method = 'put'
    Rails.logger.debug "TopicCategoriesController.edit #{@submit_form_method} #{@submit_form_path}"
  end

  # POST /topic_categories
  # POST /topic_categories.json
  def create
    @topic_category = TopicCategory.new(params[:topic_category])
    if params[:insert_after].present?
      insert_after = params[:insert_after].to_i
      TopicCategory.ordered.reverse_each do |category|
        if insert_after == category.id
          @topic_category.order = category.order + 1
          break
        else
          category.update_attribute :order, category.order + 1
        end
      end
    end

    respond_to do |format|
      if @topic_category.save
        format.html { redirect_to topic_categories_path, notice: 'Topic category was successfully created.' }
        format.json { render json: @topic_category, status: :created, location: @topic_category }
      else
        format.html { render action: "new" }
        format.json { render json: @topic_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /topic_categories/1
  # PUT /topic_categories/1.json
  def update
    @topic_category = TopicCategory.find(params[:id])

    respond_to do |format|
      if @topic_category.update_attributes(params[:topic_category])
        format.html { redirect_to topic_categories_path, notice: 'Topic category was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @topic_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /topic_categories/1
  # DELETE /topic_categories/1.json
  def destroy
    @topic_category = TopicCategory.find(params[:id])
    @topic_category.destroy

    respond_to do |format|
      format.html { redirect_to topic_categories_url }
      format.json { head :no_content }
    end
  end
end
