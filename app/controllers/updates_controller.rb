class UpdatesController < ApplicationController

  def index
    if params[:workshop_id].present?
      @workshop = Workshop.find params[:workshop_id], :include => :updates
      @updates = @workshop.updates
    elsif params[:user_id].present?
      @user = User.find params[:user_id], :include => :updates
      if params[:seen].present? && params[:seen] == 'false'
        @updates = @user.updates.order('created_at DESC').where(:seen => false)
      elsif params[:seen].present?
        @updates = @user.updates.order('created_at DESC').where(:seen => true)
      else
        @updates = @user.updates.order('created_at DESC').all
      end
      @unseen_update_count = Update.where(:user_id => @user, :seen => false).count

      Rails.logger.debug "UpdatesController.index \n#{@user.as_json}\n#{@updates.as_json}"
    else
      @updates = Update.all
    end



    respond_to do |format|
      format.html # index.html.erb  # { render :partial => "index", :layout => "fragment" } 
      format.json { render json: @updates.where(:seen => false).as_json }
    end
  end

  def update
    @update = Update.find params[:id]
    @update.update_attributes params[:updates]
    
    if params[:workshop_id].present?
      @workshop = Workshop.find params[:workshop_id], :include => :updates
      @unseen_update_count = Update.where(:seen => false, :workshop_id => @workshop).count
      # @update = @workshop.updates
    elsif params[:user_id].present?
      @user = User.find params[:user_id], :include => :updates
      @unseen_update_count = Update.where(:seen => false, :user_id => @user).count
      Rails.logger.debug "UpdatesController.index \n#{@user.as_json}\n#{@updates.as_json}"
    end


    respond_to do |format|
      format.html # index.html.erb  # { render :partial => "index", :layout => "fragment" } 
      format.json { render json: @update.as_json }
    end
  end

end
