class EmailTemplatesController < ApplicationController

  include SimpleDataTables
  accept_data_filter :all, :active, :archived
  alias_data_mapping :action_links, :email_template_action_links

  # GET /email_templates
  # GET /email_templates.json
  def index
    @head_title = "Email Templates - #{@head_title}"
    @email_templates = EmailTemplate.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: mapped_json }
    end
  end

  # GET /email_templates/1
  # GET /email_templates/1.json
  def show
    @head_title = "Email Template - #{@head_title}"
    @email_template = EmailTemplate.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @email_template }
    end
  end

  # GET /email_templates/new
  # GET /email_templates/new.json
  def new
    @head_title = "New Email Template - #{@head_title}"

    @liquid_methods = YAML::load_file(Rails.root.join('config', 'liquid_methods.yml'))
    @email_template = EmailTemplate.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @email_template }
    end
  end

  # GET /email_templates/1/edit
  def edit
    @head_title = "Edit Email Template - #{@head_title}"

    @liquid_methods = YAML::load_file(Rails.root.join('config', 'liquid_methods.yml'))
    @email_template = EmailTemplate.find(params[:id])
  end

  # POST /email_templates
  # POST /email_templates.json
  def create
    @email_template = EmailTemplate.new(params[:email_template])

    respond_to do |format|
      if @email_template.save
        format.html { redirect_to email_templates_url, notice: 'Email template was successfully created.' }
        format.json { render json: @email_template, status: :created, location: @email_template }
      else
        format.html { render action: "new" }
        format.json { render json: @email_template.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /email_templates/1
  # PUT /email_templates/1.json
  def update
    @email_template = EmailTemplate.find(params[:id])

    respond_to do |format|
      if @email_template.update_attributes(params[:email_template])
        format.html { redirect_to email_templates_url, notice: 'Email template was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @email_template.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /email_templates/1
  # DELETE /email_templates/1.json
  def destroy
    @email_template = EmailTemplate.find(params[:id])
    @email_template.destroy

    respond_to do |format|
      format.html { redirect_to email_templates_url }
      format.json { head :no_content }
    end
  end

  def docs
    @liquid_methods = YAML::load_file(Rails.root.join('config', 'liquid_methods.yml'))

    Rails.logger.debug "EmailTemplatesController.docs #{@liquid_methods}"

    respond_to do |format|
      format.html { render :partial => 'docs', :layout => 'fragment' }
      format.json { head :no_content }
    end    
  end
end
