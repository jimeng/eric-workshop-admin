class InstructorAssignmentsController < GoogleAwareController

  require 'event_notification'
  
  # POST /workshops/:workshop_id/instructor_assignments
  # POST /workshops/:workshop_id/instructor_assignments.json
  def create

    workshop = Workshop.find(params[:workshop_id])
    if params[:user_id].nil? && params[:uniqname].blank?
      user = current_user
    elsif params[:user_id].nil?
      user = User.find_by_uniqname(params[:uniqname])
    else
      user = User.find(params[:user_id])
    end

    if user.nil?
      user = current_user
    end

    if params[:order].nil?
      if workshop.instructors.blank?
        position = 0
      else
        ia = InstructorAssignment.order{ order.desc }.find_by_workshop_id(params[:workshop_id])
        if ia.order.blank?
          position = workshop.instructors.count
        else
          position = ia.order + 1
        end
      end
    else
      position = params[:order]
    end

    @instructor_assignment = InstructorAssignment.new :workshop => workshop, :user => user, order: position

    # Rails.logger.debug "InstructorAssignmentsController.create #{@instructor_assignment.inspect}"

    # Rails.logger.debug "InstructorAssignmentsController.create #{workshop.inspect}"

    respond_to do |format|
      if @instructor_assignment.save
        @workshop = Workshop.find(@instructor_assignment.workshop.id)
        update_accepted(@workshop)
        hash = @workshop.as_json(:include => [:instructors, :statistics, :location])
        Rails.logger.debug "InstructorAssignmentsController.create\n==============================\n#{hash.inspect}\n=============================="
        if @workshop.scheduled_time.present? && @workshop.scheduled_time.valid?
          hash['start_time_as_data'] = @workshop.scheduled_time.data_start_time
          hash['formatted_start_time'] = @workshop.scheduled_time.display_start_time
          hash['formatted_scheduled_time'] = @workshop.scheduled_time.display_time_range
          hash['scheduled_time'] = @workshop.scheduled_time.to_hash

          if @workshop.instruction_event_id.present?
            load_client
            if @client.nil? || @client.authorization.nil? || @client.authorization.access_token.nil?
              CalendarsController.update_participants @workshop
            else
              CalendarsController.add_or_update_instruction_calendar_event @client, @workshop, true
              if workshop.location.present? && workshop.location.calendar_url.present?
                CalendarsController.add_or_update_location_calendar_event @client, workshop.location.calendar_url, @workshop
              end
            end
          end
        end

        @workshop.add_units_for_instructor user

        notifier = EventNotification.new
        notifier.notify "workshop.instructor.add", [ @workshop ], current_user
        #format.html { redirect_to locations_path, notice: 'Location was successfully created.'

        # format.json { render json: hash.to_json
        # the above line was not working even with the missing } - waiting for Jim's return to resolve
        format.json { render json: @workshop.to_json(:include => [:instructors, :instructor_assignments]) }

      else
        Rails.logger.debug "InstructorAssignmentsController.create NOT SAVED"
        @workshop = Workshop.find(@instructor_assignment.workshop.id)
        format.html { redirect_to schedule_workshop_path(active_tab_id: 'instructor'),
          :flash => { :error => "That instructor has already been added - an instructor can only be added one time!" }}
        format.json { render json: { errors: @instructor_assignment.errors, user: user, workshop: @workshop.to_json(:include => [:instructors]) } }
      end
    end
  end

    # set accepted = true if a scheduler has set the room or sched time (instructor change handled in the instructor controller)
  def update_accepted(workshop)
    if can? :schedule, workshop
      workshop.update_attribute :accepted, true
    end
  end

  # PUT /workshops/:workshop_id/instructor_assignments/:id
  # PUT /workshops/:workshop_id/instructor_assignments/:id.json
  def update
    workshop = Workshop.find(params[:workshop_id])
    @instructor_assignment = InstructorAssignment.find(params[:id])

    respond_to do |format|
      if @instructor_assignment.update_attributes(params[:instructor_assignment])
        #format.html { redirect_to locations_path, notice: 'Location was successfully updated.' }
        format.json { head :no_content }
      else
        #format.html { render action: "edit" }
        format.json { render json: @instructor_assignment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /workshops/:workshop_id/instructor_assignments/:id
  # DELETE /workshops/:workshop_id/instructor_assignments/:id.json
  def destroy
    Rails.logger.debug "InstructorAssignmentsController.destroy #{params.inspect}"
    workshop = Workshop.find(params[:workshop_id])
    if params[:id].nil? || params[:id] == "0"
      @instructor_assignment = InstructorAssignment.find_by_workshop_id_and_user_id(workshop.id, current_user.id)
    else
      @instructor_assignment = InstructorAssignment.find(params[:id])
    end
    unless @instructor_assignment.nil?
      user = @instructor_assignment.user
      success = @instructor_assignment.destroy
    end
    Rails.logger.debug "InstructorAssignmentsController.destroy #{success.inspect}"

    @workshop = Workshop.find(params[:workshop_id])
    @workshop.instructor_url = workshop_url(@workshop)

    update_accepted(@workshop)

    if @workshop.instruction_event_id.present?
      load_client
      if @client.nil? || @client.authorization.nil? || @client.authorization.access_token.nil?
        CalendarsController.update_participants @workshop
      else
        CalendarsController.add_or_update_instruction_calendar_event @client, @workshop, true
        if workshop.location.present? && workshop.location.calendar_url.present?
          CalendarsController.add_or_update_location_calendar_event @client, workshop.location.calendar_url, @workshop
        end
      end
    end

    unless user.blank?
      @workshop.remove_units_for_instructor user
    end

    notifier = EventNotification.new
    notifier.notify "workshop.instructor.remove", [ @workshop ], current_user
  
    respond_to do |format|
      #format.html { redirect_to locations_url }
      format.json { render json: @workshop.to_json(:include => [:instructors, :instructor_assignments]) }
      #format.json { head :no_content }
    end
  end


end