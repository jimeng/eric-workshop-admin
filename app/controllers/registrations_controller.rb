class RegistrationsController < ApplicationController

  include SimpleDataTables
  accept_data_filter :all, :active, :archived
  alias_data_mapping :action_links, :registration_action_links

  default_data_filter :active

  def index
    @head_title = "Registration Connections - #{@head_title}"
    respond_to do |format|
      format.html
      format.json { render json: mapped_json }
    end
  end

  def show
    @head_title = "Registration Connection - #{@head_title}"
    @registration = Registration.find(params[:id])
  end

  def edit
    @head_title = "Edit Registration Connection - #{@head_title}"
    @registration = Registration.find(params[:id])
    @submit_form_path = registration_path(@registration)
    @submit_form_method = 'put'

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @registration }
    end
  end

  # GET /registrations/new
  # GET /registrations/new.json
  def new
    @head_title = "New Registration Connection - #{@head_title}"
    @registration = Registration.new
    @submit_form_path = registrations_path
    @submit_form_method = 'post'

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @registration }
    end
  end
  # POST /registrations
  # POST /registrations.json
  def create
    @registration = Registration.new(params[:registration])

    respond_to do |format|
      if @registration.save
        format.html { redirect_to registrations_path, notice: 'Registration was successfully created.' }
        format.json { render json: @registration, status: :created, registration: @registration }
      else
        format.html { render action: "new" }
        format.json { render json: @registration.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /registrations/1
  # PUT /registrations/1.json
  def update
    @registration = Registration.find(params[:id])

    respond_to do |format|
      if @registration.update_attributes(params[:registration])
        format.html { redirect_to registrations_path, notice: 'Registration was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @registration.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /registrations/1
  # DELETE /registrations/1.json
  def destroy
    @registration = Registration.find(params[:id])
    @registration.destroy

    respond_to do |format|
      format.html { redirect_to registrations_path }
      format.json { head :no_content }
    end
  end



end
