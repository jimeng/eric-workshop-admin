require 'json'
include WorkshopsHelper
require 'uri'

class WorkshopsController < GoogleAwareController

  include WorkshopsHelper
  include SimpleDataTables
  require 'event_notification'


  # When the non-RESTful actions are refactored, this :only spec will probably go away
  # and may be replaced with an :except spec.
  # Note that we include :schedule as a special case because the action matches the permission
  # and the Workshop should be loaded, just as with the standard actions. Also note that index
  # is a bit of a special case here since it handles authenticated and unauthenticated requests.
  load_and_authorize_resource :only => [:show, :edit, :new, :create, :update, :destroy, :schedule]

  skip_before_filter :force_ssl, :only => [:index]
  skip_before_filter :require_login, :only => [:index]

  accept_data_filter  :active, :mine, :assigned, :unassigned, :booked, :unbooked,
                      :scheduled, :unscheduled, :requested, :archived, :without_stats, :needs_stats,
                      :taughtnotarchived, :alreadytaught, :by_user, :archived_by_user, :needs_eval,
                      :archived_by_instructor, :default_landing, :recent, :deleted
  direct_data_mapping :contact_user_name, :contact_user_sort_name,
                      :requester_user_name, :requester_user_sort_name,
                      :location_name, :short_location_name, :format_duration,
                      :topic_list, :managed_by_initials, :all_requested_times,
                      :instructor_list, :instructor_ids, :instructor_names, :attachment_count,
                      :first_attachment_name, :first_attachment_url, :first_attachment_size
  alias_data_mapping  :has_statistics, :has_statistics?

  # FIXME: Figure out the user-specific stuff... We need to map out the
  #        logical filters (like "workshops assigned to me") and where they
  #        correspond to simple scopes, user-bound scopes, or may vary
  #        between roles. For now, this is a somewhat sloppy example.

  def filtered_scope
    (current_user.workshop_filter data_filter).includes(:instructors, :topics, :statistics, :attachments, :requested_times, :contact_user, :requested_by, :location, :managed_by)
  end

  def default_data_filter
    current_user.default_filter
  end

  def index
    if logged_in?
      authorize! :read, Workshop
      @records_per_page = current_user.records_per_page
      @head_title = "Dashboard - #{@head_title}"
      respond_to do |format|
        format.html
        format.json { render json: mapped_json }

      end
    else
      render 'static_pages/home'
    end
  end

  def dash
    respond_to do |format|
      format.html
      format.json { render json: mapped_json }
    end
  end

  def new
    @head_title = "New Request - #{@head_title}"
    @submit_form_path = workshops_path
    @submit_form_method = 'post'
    @session_types = SessionType.where(:active => true)
    @evaluations = Evaluation.where(:active => true).order(:id)
    @topic_categories = TopicCategory.ordered

    @workshop.contact_user = current_user
    @workshop.requested_by = current_user
    alt_count = 5
    for alt in 1..alt_count
      @workshop.requested_times.append RequestedTime.new(:time_range => TimeRange.new(Time.zone.now.to_datetime.advance(:weeks => 2).beginning_of_hour, Time.zone.now.to_datetime.advance(:weeks => 2).beginning_of_hour.advance(:hours => 1)))
    end
    att_count = 5
    for att in 1..att_count
      @workshop.attachments.append Attachment.new
    end

    @subjects = Registrar.subjects(Time.zone.now.year, time_to_term_code(Time.zone.now))
    @courses = []
    @sections = []

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @workshop }
    end
  end

  def show
    if can? :schedule, @workshop
      load_client
      @have_google_access = @client.present? && @client.authorization.present? && @client.authorization.access_token.present?
    end
    @head_title = "Instruction Request - #{@head_title}"
    @pager_base_path = workshop_path(@workshop)
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @workshop }
    end
  end

  def edit
    @head_title = "Edit Request - #{@head_title}"
    @submit_form_path = workshop_path(@workshop)
    @submit_form_method = 'put'
    @session_types = SessionType.where(:active => true)
    @evaluations = Evaluation.where(:active => true).order(:id)
    @topic_categories = TopicCategory.ordered

    rt_count = 5
    current_rt_count = @workshop.requested_times.size
    if current_rt_count < rt_count
      for alt in (current_rt_count + 1) .. rt_count
        @workshop.requested_times.append RequestedTime.new(:time_range => TimeRange.new(Time.zone.now.to_datetime.advance(:weeks => 2).beginning_of_hour, Time.zone.now.to_datetime.advance(:weeks => 2).beginning_of_hour.advance(:hours => 1)))
      end
    end

    att_count = 5
    current_att_count = @workshop.attachments.size
    if current_att_count < att_count
      for att in (current_att_count + 1) .. att_count
        @workshop.attachments.append Attachment.new
      end
    end

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @workshop }
    end
  end

  # POST /workshops
  # POST /workshops.json
  def create

    # contact
    if(params[:contact].nil? || params[:contact][:uniqname].nil? || params[:contact][:uniqname].empty?)
      contact_user = nil
    else
      contact_user = User.where(:uniqname => params[:contact][:uniqname]).first
      if(contact_user.nil?)
        contact_user = User.find_or_create_from_directory params[:contact][:uniqname]
      end
    end

    unless contact_user.nil?
      @workshop.contact_user = contact_user
    end

    if @workshop.requested_by.nil?
      @workshop.requested_by = current_user
    end

    if params.has_key?(:req_form_claim)
      if params[:req_form_claim] == '1'
        @workshop.instructors << current_user
        @workshop.add_units_for_instructor current_user
      end
    end

    if params.has_key?(:evaluation_id)
      eval_id = params[:evaluation_id]
      unless eval_id.blank?
        eval_id = eval_id.to_i
        unless eval_id < 1
          @workshop.evaluation_id = eval_id
        end
      end
    end

    # session_type
    session_type_id = params[:session_type]
    if session_type_id.nil? || session_type_id.empty?
      session_type_id = '1'
    end
    unless session_type_id.nil? || session_type_id.empty?
      session_type = SessionType.where(:id => session_type_id.to_i).first
    end
    unless session_type.nil?
      @workshop.session_type = session_type
    end

    # requested_times
    requested_times_hash = params[:requested_times]
    unless requested_times_hash.nil? || requested_times_hash.empty?
      requested_times_hash.each do |key, value|
        unless value[:day].blank? || value[:time0].blank? || value[:time1].blank?
          time_range = TimeRange.from_parts(value[:day], value[:time0], value[:time1])
        end
        unless time_range.blank?
          rt = RequestedTime.new(:time_range => time_range)
          @workshop.requested_times << (rt)
        end
      end
    end

    if params[:use_req_time_as_sched_time] && @workshop.requested_times.present? 
      @workshop.scheduled_time = @workshop.requested_times[0].time_range
    end

    if (defined? params[:library_location_required])
      locreq = params[:workshop][:library_location_required]
      @workshop.library_location_required = locreq
      if locreq == '0'
        @workshop.location_other = 'No Room Needed'
      end
    end

    topic_ids = params[:topics]
    if topic_ids.nil? || topic_ids.empty?
      @workshop.topics = []
    else
      topic_ids.each do |topic_id|
        topic = Topic.find(topic_id.to_i)
        unless topic.nil?
          @workshop.topics << topic
        end
      end
    end

    attachments_hash = params[:attachment]

    workshops = {}
    list = []
    errors = []
    one_workshop_per_requested_time = params['one-workshop-per-requested-time']
    if one_workshop_per_requested_time.nil? || one_workshop_per_requested_time.empty? || one_workshop_per_requested_time == 'false'
      unless attachments_hash.nil? || attachments_hash.empty?
        attachments_hash.each do |key, value|
          att = Attachment.new value
          @workshop.attachments << att
        end
      end
      # Rails.logger.debug("WorkshopsController.create #{@workshop.attachments.inspect}")
      if @workshop.save
        workshops[workshops_path(@workshop)] = @workshop
        list << @workshop
      else
        errors << 'error creating workshop'
      end
    else
      count = 1
      @workshop.requested_times.each do |rt|
        group_msg = "This request is #{count} of #{requested_times_hash.size} requests submitted together.\n\n"
        count = count + 1
        workshop = @workshop.dup
        workshop.title = workshop.title + '_' + (count-1).to_s
        workshop.requested_times << rt
        unless @workshop.topics.nil? || @workshop.topics.empty?
          workshop.topics << @workshop.topics
        end
        unless attachments_hash.nil? || attachments_hash.empty?
          attachments_hash.each do |key, value|
            att = Attachment.new value
            workshop.attachments << att
          end
        end
        if params[:use_req_time_as_sched_time]
          workshop.scheduled_time = rt.time_range
        end
        if params.has_key?(:req_form_claim)
          if params[:req_form_claim] == '1'
            workshop.instructors << current_user
            workshop.add_units_for_instructor current_user
          end
        end
        workshop.notes = "#{group_msg}#{@workshop.notes}"
        if workshop.save
          workshops[workshops_path(workshop)] = workshop
          list << workshop
        else
          errors << "error creating workshop with requested-time #{rt}"
        end
      end
    end

    if errors.empty?
      notifier = EventNotification.new
      notifier.notify "workshop.create", list, current_user
    end

    respond_to do |format|
      if errors.empty?
        if count.present? && count > 1
          format.html { redirect_to workshops_path, notice: 'Workshops were successfully created.'}
        else
          format.html { redirect_to workshops_path, notice: 'Workshop was successfully created.'}
        end
        format.json { render json: workshops, status: :created, location: @calendar_config }
      else
        format.html { render action: "new" }
        format.json { render json: @workshop.errors, status: :unprocessable_entity }
      end
    end
  end

  def schedule
    load_client
    @head_title = "Manage Requests - #{@head_title}"
    @pager_base_path = schedule_workshop_path(@workshop)

    # @show_manage_pager = CalendarConfig.find_by_name('show_manage_pager')
    @show_manage_pager = true

    # Rails.logger.debug("WorkshopsController.schedule #{@client.authorization.to_json}")
    @have_google_access = @client.present? && @client.authorization.present? && @client.authorization.access_token.present?
    @session_types = SessionType.where(:active => true)
    @evaluations = Evaluation.where(:active => true).order(:id)
    @registrations = Registration.where(:active => true).order(:name)
    @unitnames = Unit.where(:active => true).order(:name).pluck(:name)
    @topic_categories = TopicCategory.ordered
    @units = units_as_string
    @sub_choices = SessionSubChoice.where(:active => true)
    @active_tab_id = 'review'
    if params[:active_tab_id].present?
      @active_tab_id = params[:active_tab_id]
      if "email" == @active_tab_id || "events" == @active_tab_id
        page_num = params[:page_num]
        if page_num.nil?
          page_num = 0
        else
          page_num = page_num.to_i
        end

        page_size = params[:page_size]
        if page_size.nil?
          page_size = 16
        else
          page_size = page_size.to_i
        end
        if "email" == @active_tab_id
          @comments = @workshop.comments.order{ created_at.desc }.limit(page_size).offset(page_num * page_size)
          @total_count = @workshop.comments.count
        else
          page_num = params[:page_num]
          if page_num.nil?
            page_num = 0
          else
            page_num = page_num.to_i
          end

          page_size = params[:page_size]
          if page_size.nil?
            page_size = 16
          else
            page_size = page_size.to_i
          end
          events_list = WorkshopEvent.order{ created_at.desc }.where(:workshop_id => @workshop.id).limit(page_size).offset(page_num * page_size)

          @events = WorkshopEventsController.process_events(events_list)

          @total_count = WorkshopEvent.where(:workshop_id => @workshop.id).count
        end
        @page_num = page_num
        @page_size = page_size
        @last_page = @total_count / @page_size
      end
    end
    if(@workshop.statistics.nil?)
      @workshop.statistics = Statistics.new
    end
    unless params[:pg_order].nil?
      @pg_order = params[:pg_order]
      pg_order = Base64.decode64 @pg_order
      pg_order_list = JSON.parse pg_order
      @pg_count = pg_order_list.length
      unless @pg_count < 2
        @pg_index = pg_order_list.index @workshop.id
        unless @pg_index.nil?
          if @pg_index <= 0
            @next_link = schedule_workshop_url(pg_order_list[1], :pg_order => @pg_order)
          elsif @pg_index >= @pg_count- 1
            @prev_link = schedule_workshop_url(pg_order_list[@pg_index - 1], :pg_order => @pg_order)
          else
            @next_link = schedule_workshop_url(pg_order_list[@pg_index + 1], :pg_order => @pg_order)
            @prev_link = schedule_workshop_url(pg_order_list[@pg_index - 1], :pg_order => @pg_order)
          end
        end
      end
    end
    @recipients = []
    contact_user = EmailRecipient.new(:user => @workshop.contact_user)
    contact_user.role = :contact
    @recipients << contact_user
    unless @workshop.contact_user == @workshop.requested_by
      requested_by = EmailRecipient.new(:user => @workshop.requested_by)
      requested_by.role = :requested_by
      @recipients << requested_by
    end
    @workshop.instructors.each do |instructor|
      instr = EmailRecipient.new(:user => instructor)
      instr.role = :instructor
      @recipients << instr
    end
    me = EmailRecipient.new(:user => current_user)
    me.role = :scheduler
    @recipients << me
  end

  def statistics
    @workshop = Workshop.find params[:id]
    authorize! :schedule, @workshop
    if(@workshop.statistics.nil?)
      @workshop.statistics = Statistics.new
    end
    @session_types = SessionType.where(:active => true)
    @sub_choices = SessionSubChoice.where(:active => true)
    @units = units_as_string

    respond_to do |format|
      # format.html { render  :partial => "evaluation" , :layout => true}
      format.html { render  :partial => "statistics" }
      format.json { render :json =>  @workshop }
    end    
  end

  def claim_show_view

    @workshop = Workshop.find(params[:id])
    authorize! :claim, @workshop
    showview = params[:showview]
    notifier = EventNotification.new
    case params[:showview]
    when "addme"
      @workshop.instructors << current_user
      notifier.notify 'workshop.instructor.add', [ @workshop ], current_user
      respond_to do |format|
        if @workshop.update_attributes(params[:workshop])
          @workshop.add_units_for_instructor current_user
          flash[:notice] = "Instructor successfully added."
          format.html { redirect_to :action => "show" }
          format.json { render :json =>  @workshop.to_json(:include => [:instructors, :statistics, :location]) }
        else
          format.html
          format.json { render json: @workshop.errors, status: :unprocessable_entity }
        end
      end
    when "removeme"
      @workshop.instructors.delete(current_user)
      notifier.notify 'workshop.instructor.remove', [ @workshop ], current_user
      respond_to do |format|
        if @workshop.update_attributes(params[:workshop])
          @workshop.remove_units_for_instructor current_user
          flash[:notice] = "Instructor successfully removed."
          format.html { redirect_to :action => "show" }
          format.json { render :json =>  @workshop.to_json(:include => :instructors) }
        else
          format.html
          format.json { render json: @workshop.errors, status: :unprocessable_entity }
        end
      end
    else
      respond_to do |format|
        flash[:notice] = "Check Instructor claim logic"
        format.html { redirect_to :action => "show" }
        format.json { render :json =>  @workshop.to_json(:include => :instructors) }
      end
    end
  end

  def short
    # Rails.logger.debug "WorkshopsController.short #{params}"
    unless params[:code].nil?
      workshop_id = params[:code].to_i(36)
      # Rails.logger.debug "WorkshopsController.short #{workshop_id}"
      @workshop = Workshop.find(workshop_id)
      long_uri = @workshop.full_eval_url(false)
      if long_uri.blank?
        @page = render_eval_page(@workshop)
      end
    end
    respond_to do |format|
      if long_uri.present?
        format.html { redirect_to long_uri }
      else
        format.html # short.html.erb
      end
    end
  end

  def render_eval_page(workshop)
    et = EmailTemplate.find_by_name_and_active('eval_error_page', true)
    if et.present?
      liquify(workshop)
      params = {}
      params['workshop'] = workshop

      Rails.logger.debug "WorkshopsController.render_eval_page \n~~~~~~~----------~~~~~~~~~~\n#{params['workshop'].as_json}"
      # Rails.logger.debug "CommentsController.new_request_notification \n#{et}\n#{et.subject}\n#{et.body}"
      body_template = Liquid::Template.parse(et.body)
      body = body_template.render params
    end
    Rails.logger.debug "WorkshopsController.render_eval_page\n#{body}"
    body
  end


  def claim
    @workshop = Workshop.find(params[:id])
    authorize! :claim, @workshop
    # Rails.logger.info("WorkshopsController.claim #{@workshop}")
    @workshop.instructors << current_user
    notifier = EventNotification.new
    notifier.notify 'workshop.instructor.add', [ @workshop ], current_user

    respond_to do |format|
      if @workshop.update_attributes(params[:workshop])
        @workshop.add_units_for_instructor current_user
        format.html { redirect_to workshops_path, notice: 'Workshop was successfully updated.' }
        format.json { render :json =>  @workshop.to_json(:include => :instructors) }
      else
        format.html
        format.json { render json: @workshop.errors, status: :unprocessable_entity }
      end
    end
  end

  def copy
    @head_title = "Copy Request - #{@head_title}"
    @workshop = Workshop.find(params[:id])
    authorize! :read, @workshop
    authorize! :create, Workshop
    @submit_form_path = workshops_path
    @submit_form_method = 'post'
    @session_types = SessionType.where(:active => true)
    @evaluations = Evaluation.where(:active => true).order(:id)
    @topic_categories = TopicCategory.ordered

    # allow_split_on_request_times

    @workshop.contact_user = current_user
    @workshop.requested_by = current_user
    alt_count = 5
    for alt in 1..alt_count
      @workshop.requested_times.append RequestedTime.new
    end
    att_count = 5
    for att in 1..att_count
      @workshop.attachments.append Attachment.new
    end

    @subjects = Registrar.subjects(2012, 2)
    @courses = []
    @sections = []

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @workshop }
    end
  end

  # TODO: Handle in update-attributes method
  # https://bitbucket.org/botimer/workshop-admin/issue/86
  def toggle_manage
    @workshop = Workshop.find(params[:id])
    authorize! :schedule, @workshop
    if @workshop.managed_by.nil?
      @workshop.managed_by = current_user
      # uncomment this if we want to trigger accepted when a scheduler clicks the managed by me link
      # @workshop.accepted = true
    else
      @workshop.managed_by = nil
    end
    respond_to do |format|
      if @workshop.update_attributes(params[:workshop])
        format.html { redirect_to @workshop, notice: 'Workshop was successfully updated.' }
        format.json { render :json =>  @workshop.to_json(:include => [:managed_by]) }
      else
        format.html { render action: "schedule" }
        format.json { render json: @workshop.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /workshops/1
  # PUT /workshops/1.json
  def update

    # Rails.logger.debug("WorkshopsController.update params:\n#{params.inspect}")
    @workshop.update_attributes(params[:workshop])

    # contact
    if(params[:contact].nil? || params[:contact][:uniqname].nil? || params[:contact][:uniqname].empty?)
      contact_user = nil
    else
      contact_user = User.where(:uniqname => params[:contact][:uniqname]).first
      if(contact_user.nil?)
        contact_user = User.find_or_create_from_directory params[:contact][:uniqname]
      end
    end

    unless contact_user.nil?
      @workshop.update_attribute 'contact_user', contact_user
    end

    rt = update_requested_times(@workshop.requested_times, params[:requested_times])
    if( rt[:changes2save] )
      @workshop.update_attribute('requested_times', rt[:new_times])
    end

    # session_type
    session_type_id = params[:session_type]
    if session_type_id.nil? || session_type_id.empty?
      session_type_id = '1'
    end
    unless session_type_id.nil? || session_type_id.empty?
      session_type = SessionType.find(session_type_id.to_i)
    end
    unless session_type.nil? || @workshop.session_type == session_type
      @workshop.update_attribute :session_type, session_type
    end

    if params.has_key?(:evaluation_id)
      eval_id = params[:evaluation_id]
      if eval_id.present?
        eval_id = eval_id.to_i
        if eval_id < 1
          eval_id = nil
        end
      else
        eval_id = nil
      end
      unless eval_id == @workshop.evaluation_id
        @workshop.update_attribute :evaluation_id, eval_id
      end
    elsif @workshop.evaluation_id.present?
      @workshop.update_attribute :evaluation_id, nil
    end

    if params.has_key?(:req_form_claim)
      claim_by_requester = params[:req_form_claim]
      unless claim_by_requester.nil? ||  claim_by_requester == '0' || @workshop.instructors.include?(current_user)
        @workshop.instructors << current_user
        @workshop.add_units_for_instructor current_user
      end
    end

    # topics
    topic_ids = params[:topics]
    new_topics = []
    unless topic_ids.nil? || topic_ids.empty?
      topic_ids.each do |topic_id|
        topic = Topic.find(topic_id.to_i)
        unless topic.nil?
          new_topics << topic
        end
      end
    end
    unless @workshop.topics == new_topics
      @workshop.update_attribute :topics, new_topics
    end

    # attachments
    attachments_hash = params[:attachment]
    if attachments_hash.nil? || attachments_hash.empty?
      unless @workshop.attachments.nil? || @workshop.attachments.empty?
        @workshop.update_attribute :attachments, []
      end
    else
      attachments_hash.each do |key, value|
        att = Attachment.new value
        @workshop.attachments << att
      end
    end

    if params[:use_req_time_as_sched_time]
      @workshop.scheduled_time = @workshop.requested_times[0].time_range
    else
      @workshop.scheduled_time_start_time = nil
    end

    update_accepted(@workshop)

    respond_to do |format|
      if @workshop.save
        format.html { redirect_to workshops_path, notice: 'Workshop was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @workshop.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /workshops/1
  # DELETE /workshops/1.json
  def destroy
    load_client
    # Rails.logger.debug "WorkshopsController.destroy \n#{params.as_json}\n#{@workshop}"
    unless @workshop.instruction_event_id.blank?
      begin
        CalendarsController.remove_calendar_event @client, @workshop.instruction_event_id
        @workshop.update_attribute :instruction_event_id, ''
      rescue => e
        #jsonStr = e.message
        Rails.logger.warn "WorkshopsController.destroy ERROR Unable to remove event from instruction calendar for deleted workshop\n\tWorkshop: #{@workshop}\n\tError: #{e}"
      end
    end
    unless @workshop.location_event_id.blank?
      begin
        CalendarsController.remove_calendar_event @client, @workshop.location_event_id
        @workshop.update_attribute :location_event_id, '' 
      rescue => e
        #jsonStr = e.message
        Rails.logger.warn "WorkshopsController.destroy ERROR Unable to remove event from room calendar for deleted workshop\n\tWorkshop: #{@workshop}\n\tError: #{e}"
      end
    end

    @workshop.update_attribute :deleted, true

    respond_to do |format|
      format.html { redirect_to workshops_url }
      format.json { head :no_content }
    end
  end

  def update_accepted(workshop)
    # set accepted = true if a scheduler has set the room or sched time (instructor change handled in the instructor controller)
    if can? :schedule, workshop
      workshop.update_attribute :accepted, true
    end
  end

  def update_attribute
    # Rails.logger.debug "WorkshopsController.update_attribute #{params.inspect}"
    @workshop = Workshop.find(params[:id])

    authorize! :update, @workshop

    remove_events = false
    send_notifications = false
    if params[:pk].nil? && params[:name].nil?
      # not using x-editable pattern

      updates = {}
      # scheduled_time
      scheduled_times_hash = params[:scheduled_time]
      unless scheduled_times_hash.nil? || scheduled_times_hash.empty?
        if scheduled_times_hash[:day].blank? || scheduled_times_hash[:time0].blank? || scheduled_times_hash[:time1].blank?
          time_range = TimeRange.new nil, nil
          if @workshop.location.present?
            @workshop.update_attribute :location, nil
          end
        else
          time_range = TimeRange.from_parts(scheduled_times_hash[:day], scheduled_times_hash[:time0], scheduled_times_hash[:time1])
        end
        updates[:scheduled_time] = time_range
        send_notifications = true
        remove_events = true 
      end

      no_errors = @workshop.update_attributes! updates

    else
      # using x-editable pattern

      attr_name = params[:name]
      attr_value = params[:value]

      if attr_name == 'topics' && attr_value.present?
        topics = Topic.find(attr_value)
        no_errors = @workshop.update_attribute 'topics', topics
      elsif attr_name == 'topics'
        no_errors = @workshop.update_attribute 'topics', []
      elsif attr_name == "unit"
        attr_value = attr_value.join('|')
        no_errors = @workshop.statistics.update_attribute('unit', attr_value)
      elsif attr_name == "evaluation_id"
        # if a new eval choice is made, need to clear out the previous eval results url
        no_errors = @workshop.update_attribute('eval_results_url', nil) &&
                    @workshop.update_attribute(attr_name, attr_value)
      else
        no_errors = @workshop.update_attribute(attr_name, attr_value)
      end
    end

    # Rails.logger.debug "WorkshopsController.update_attribute no_errors? #{no_errors}\n#{@workshop.inspect}"
    if no_errors
      @workshop = Workshop.find @workshop.id
      @workshop.instructor_url = workshop_url @workshop
      hash = @workshop.as_json(:include => [:instructors, :statistics, :location, :evaluation])
      # Rails.logger.info "WorkshopsController.update_attribute\n==============================\n#{hash.inspect}\n=============================="
      if @workshop.scheduled_time.present? && @workshop.scheduled_time.valid?
        hash['start_time_as_data'] = @workshop.scheduled_time.data_start_time
        hash['formatted_start_time'] = @workshop.scheduled_time.display_start_time
        hash['formatted_scheduled_time'] = @workshop.scheduled_time.display_time_range
        hash['scheduled_time'] = @workshop.scheduled_time.to_hash
        hash['short_eval_url'] = short_url(@workshop.id.to_s(36))
        # if @workshop.evaluation.present?
        #   hash['evaluation'] = @workshop.evaluation.to_hash
        # end
        hash['full_eval_url'] = @workshop.full_eval_url
        load_client
        # Rails.logger.debug "WorkshopsController.update_attribute \n----------------------\ncalling add_or_update_instruction_calendar_event"
        unless @workshop.deleted
          CalendarsController.add_or_update_instruction_calendar_event @client, @workshop, send_notifications
          if @workshop.location.present? && @workshop.location.calendar_url.present?
            # Rails.logger.debug "WorkshopsController.update_attribute \n----------------------\ncalling add_or_update_location_calendar_event"
            CalendarsController.add_or_update_location_calendar_event @client, @workshop.location.calendar_url, @workshop
          end
        end
      elsif remove_events
        load_client
        unless @workshop.instruction_event_id.blank?
          begin
            CalendarsController.remove_calendar_event @client, @workshop.instruction_event_id
            @workshop.update_attribute :instruction_event_id, nil
          rescue => e
            #jsonStr = e.message
            Rails.logger.warn "WorkshopsController.update_attributes ERROR Unable to remove event from instruction calendar for workshop\n\tWorkshop: #{@workshop}\n\tError: #{e}"
          end
        end
        unless @workshop.location_event_id.blank?
          begin
            CalendarsController.remove_calendar_event @client, @workshop.location_event_id
            @workshop.update_attribute :location_event_id, nil 
          rescue => e
            #jsonStr = e.message
            Rails.logger.warn "WorkshopsController.update_attributes ERROR Unable to remove event from room calendar for workshop\n\tWorkshop: #{@workshop}\n\tError: #{e}"
          end
        end
      end
      notifier = EventNotification.new
      notifier.notify 'workshop.update', [ @workshop ], current_user

    end

    update_accepted(@workshop)

    respond_to do |format|
      if no_errors
        format.html
        format.json { render json: hash }
      else
        format.html
        format.json { render json: @workshop.errors, status: :unprocessable_entity }
      end
    end
  end

  def details
    @workshop = Workshop.find(params[:id])
    authorize! :read, @workshop
    render :layout => false #if params[:partial]
  end

  # Consider routing this to the Statistics controller as nested route (singleton resource)
  # TODO: Consolidate tab/page views with switching based on a parameter
  def stats
    @workshop = Workshop.find(params[:id])
    @session_types = SessionType.active
    @sub_choices = SessionSubChoice.active

    if(@workshop.statistics.nil?)
      @workshop.statistics = Statistics.new
    end
    @units = units_as_string

    authorize! :read, @workshop.statistics

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @workshop }
    end
  end

  def save_stats_tab
    @workshop = Workshop.find(params[:id])
    stats = params[:statistics]
    @session_types = SessionType.active
    @sub_choices = SessionSubChoice.active

    # Rails.logger.info("WorkshopsController.save_stats \n#{@workshop.inspect}\n#{@workshop.statistics.inspect}")

    # session_type
    session_type_id = params[:session_type]
    if session_type_id.nil? || session_type_id.empty?
      session_type_id = '1'
    end
    unless session_type_id.nil? || session_type_id.empty?
      session_type = SessionType.where(:id => session_type_id.to_i).first
    end
    unless session_type.nil? || @workshop.session_type == session_type
      @workshop.session_type = session_type
      # we've picked a new session type so remove previous sub picks
      SubChoicePick.delete_all(:workshop_id => @workshop.id)
      params[:sub_choice_picks] = nil
    end

    # The authorization here is a bit repetitive because of the nil handling.
    # The Statistics object should always exist because of the filters on the
    # Workshop model, but we should first put tests in place to verify them and
    # log and raise on nil until satisfied. Longer-term, this should be handled
    # through the Statistics controller, which should not have a create method.
    if @workshop.statistics.nil?
      @workshop.statistics = Statistics.new stats
      authorize! :update, @workshop.statistics
      @workshop.save
    else
      authorize! :update, @workshop.statistics
      unless (stats['unit'].present? && stats['participants'].present? )
        stats['completed'] = false
      end

      @workshop.statistics.update_attributes stats
      @workshop.save
    end

    # sub choice picks
    picks = []
    picks = params[:sub_choice_picks]
    unless picks.blank?
      # for whatever is checked, make a new record in the option-picks table if one isn't already there
      picks.each do |key, value|
        if SubChoicePick.ws_has_pick(@workshop, value).blank?
          c1 = SubChoicePick.new :workshop_id => @workshop.id, :session_sub_choice_id => value
          c1.save
        end
      end
    end
    # for each subpick in the db, delete the ones that aren't now picked (delete those that have been unchecked)
    curchoices = []
    curchoices = SubChoicePick.where(:workshop_id => @workshop.id)
      curchoices.each do | choice |
        if picks.blank?
          SubChoicePick.delete_all(:workshop_id => @workshop.id)
        else
          if !( picks.values.include?(choice.session_sub_choice_id.to_s) )
          SubChoicePick.where(:workshop_id => @workshop.id, :session_sub_choice_id => choice.session_sub_choice_id).delete_all
        end
      end
    end


    respond_to do |format|
      format.html { redirect_to schedule_workshop_path(active_tab_id: 'stats'), notice: 'Stats were successfully updated.' }
      format.json { render json:  @workshop  }
    end
  end

  def evaluation
    @workshop = Workshop.find(params[:id])
    @evaluations = Evaluation.where(:active => true).order(:id)

    respond_to do |format|
      # format.html { render  :partial => "evaluation" , :layout => true}
      format.html { render  :partial => "evaluation" }
      format.json { render :json =>  @workshop }
    end
  end

  def toggle_archive
    @workshop = Workshop.find(params[:id])
    authorize! :archive, @workshop

    if @workshop.archived?
      @workshop.archived = false
    else
      @workshop.archived = true
    end
    @workshop.save

    respond_to do |format|
      format.html { redirect_to schedule_workshop_path(active_tab_id: 'review') }
      format.json { render json:  @workshop  }
    end
  end

  # TODO: move to UsersController.  See issue #95
  # https://bitbucket.org/botimer/workshop-admin/issue/95
  # TODO: Consider a more factored search pattern and permission
  def instructors_like
    # Rails.logger.info "WorkshopsController.instructors_like #{params.inspect}"
    @users = []
    unless params[:uniqname].nil? || params[:uniqname].empty?
      users = User.find_by_sql [ 'select * from users where uniqname like ? order by firstname, lastname, uniqname', "#{params[:uniqname]}%" ]
      unless users.nil?
        users.each do |user|
          @users << "#{user.uniqname}"
        end
      end
    end

    respond_to do |format|
      format.html { render action: "index" }
      format.json { render json: @users }
    end
  end

  # TODO: Handle in update-attributes method
  # https://bitbucket.org/botimer/workshop-admin/issue/86
  def session_type
    @workshop = Workshop.find params[:id]
    # Rails.logger.debug("WorkshopsController.session_type #{params.inspect}")
    session_type_id = params[:value]
    # TODO: Use validates_associated so update does all existence checking and standard error handling on @workshop.save
    if session_type_id.nil? || session_type_id.empty? || session_type_id.to_i < 1
      # send error message

    else
      session_type = SessionType.find(session_type_id.to_i)
      if session_type.nil?
        # send error message
      else
        @workshop.session_type = session_type
        @workshop.save
      end
    end

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json =>  @workshop.to_json(:include => :session_type) }
    end
  end

  # TODO: Move to separate controller with stock load_and_authorize_resource
  def session_types
    stypes = [];
    SessionType.where(:active => true).each do |stype|
      item = {}
      item['value'] = stype.id
      item['text'] = stype.name
      stypes << item
    end

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json =>  stypes.to_json }
    end
  end

  # TODO: Handle in update-attributes method
  # https://bitbucket.org/botimer/workshop-admin/issue/86
  # TODO: Use validates_associated so update does all existence checking and standard error handling on @workshop.save
  def choose_location
    load_client
    @workshop = Workshop.find params[:id]

    changes = []
    # Rails.logger.debug "WorkshopsController.choose_location \n----------------------\nparams: #{params}\n----------------------\nchanges: #{changes}\n----------------------"

    change_map = {}
    send_notifications = false

    location_id = params[:location_id]
    location = Location.find(location_id)
    unless location == @workshop.location
      @workshop.update_attribute :location, location
      send_notifications = true
      changes << 'location'

      notifier = EventNotification.new
      notifier.notify 'workshop.room.add', [ @workshop ], current_user
    end

    if params[:scheduled_time].present?
      # Rails.logger.debug "WorkshopsController.choose_location \n---------------------------------------------------------------------------\nNOT changing scheduled_time\n#{params}"
    elsif params[:day].present? && params[:time0].present? && params[:time1].present?
      Rails.logger.debug "WorkshopsController.choose_location  #{params}"
      scheduled_time = TimeRange.from_parts params[:day], params[:time0], params[:time1]
      Rails.logger.debug "WorkshopsController.choose_location  #{scheduled_time}"
    elsif params[:duration_minutes].present? && params[:start_time].present?
      # Rails.logger.debug "WorkshopsController.choose_location \n---------------------------------------------------------------------------\nchanging scheduled_time"
      stime = DateTime.parse params[:start_time]
      duration_minutes = params[:duration_minutes].to_i
      etime = stime.advance(:minutes => duration_minutes)
      scheduled_time = TimeRange.new(stime, etime)
    else
      # Rails.logger.debug "WorkshopsController.choose_location \n---------------------------------------------------------------------------\nNOT changing scheduled_time\n#{params}"
    end
    unless scheduled_time.nil? || scheduled_time == @workshop.scheduled_time
      @workshop.update_attribute :scheduled_time, scheduled_time
      send_notifications = true
      changes << 'scheduled_time'
    end

    # Rails.logger.debug "WorkshopsController.choose_location \n----------------------\nparams: #{params}\n----------------------\nchanges: #{changes}\n----------------------"
    @workshop = Workshop.find(@workshop.id)
    @workshop.instructor_url = workshop_url(@workshop)
    unless @workshop.deleted
      CalendarsController.add_or_update_instruction_calendar_event @client, @workshop, send_notifications
      CalendarsController.add_or_update_location_calendar_event @client, location.calendar_url, @workshop, changes
    end

    update_accepted(@workshop)

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json =>  @workshop.to_json(:include => [:scheduled_time, :location]) }
    end
  end

  # TODO: Handle in update-attributes method
  # https://bitbucket.org/botimer/workshop-admin/issue/86
  # TODO: Move the trigger stuff into a model callback, possibly something like:
  #       before_save :location_required_toggled, :if => :library_location_required_changed?
  #       Full treatment of all location fields may require a more generic callback, though,
  #       since multiple of the fields may be supplied in an update and precedence needs to
  #       be determined. Something like this, maybe:
  #       before_save :fix_up_location_info
  def library_location_required
    load_client
    @workshop = Workshop.find params[:id]
    send_notifications = false
    changes = []
    # TODO: permit specific attributes so update can authorize them implicitly
    authorize! :update, @workshop
    required = params[:required]
    if required.nil? || required == 'false'
      @workshop.update_attribute :library_location_required, false
      @workshop.update_attribute :location_other, 'No Room Needed'
      unless @workshop.location.nil?
        @workshop.update_attribute 'location', nil
        send_notifications = false
      end
      unless @workshop.location_event_id.blank?
        begin
          CalendarsController.remove_calendar_event @client, @workshop.location_event_id
          @workshop.update_attribute 'location_event_id', nil
          send_notifications = true
        rescue => e
          Rails.logger.warn "WorkshopsController.library_location_required ERROR while removing calendar event\n\t#{e}"
        end
      end
    elsif required == 'true'
      @workshop.update_attribute :library_location_required, true
      unless @workshop.location_other.nil?
        @workshop.update_attribute 'location_other', nil
        send_notifications = true
      end
    end
    @workshop.instructor_url = workshop_url(@workshop)
    unless @workshop.deleted
      CalendarsController.add_or_update_instruction_calendar_event @client, @workshop, send_notifications
    end

    update_accepted(@workshop)

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json =>  @workshop.to_json(:include => [:scheduled_time, :location]) }
    end
  end

  # TODO: Move to a modelless/search controller
  def course_select
    subject = params[:subject]
    @courses = Registrar.courses(current_year,current_term,subject)
    unless @courses.nil? || @courses.empty?
      catalog_nbr = @courses[0][1]
      @sections = Registrar.sections(current_year,current_term,subject,catalog_nbr)
      section_string = render_to_string('workshops/_section_select', :layout => false)
    end
    course_string = render_to_string('workshops/_course_select', :layout => false)
    render :json => { :courses => course_string, :sections => section_string  }
  end

  # TODO: Move to a modelless/search controller
  def section_select
    subject = params[:subject]
    catalog_nbr = params[:course]
    @sections = Registrar.sections(current_year,current_term,subject,catalog_nbr)
    section_string = render_to_string('workshops/_section_select', :layout => false)
    render :json => {  :sections => section_string  }
  end

  # TODO: Move to a modelless/search controller
  def subjects_like
    # Rails.logger.info "WorkshopsController.subjects_like -----"
    @subjects = []
    subjects = Registrar. subjects(current_year, current_term)
    subjects.map!  { |subject| { :code => subject[0] , :name => subject[1]} }
    @subjects = subjects

    respond_to do |format|
      format.html { render action: "index" }
      format.json { render json: @subjects }
    end
  end

  # TODO: Move to a modelless/search controller
   def courses_like
    # Rails.logger.info "WorkshopsController.courses_like -----"
     subject = params[:subject]
     @courses = []
     courses = Registrar. courses(current_year, current_term,subject)
     courses.each do | list |
       courses   = list[1]
       @courses << courses
     end

     respond_to do |format|
       format.html { render action: "index" }
       format.json { render json: @courses }
     end
   end

  # TODO: Move to a modelless/search controller
  def sections_like
    @sections = []
    subject = params[:subject]
    catalog_nbr = params[:course]
    sections = Registrar. sections(current_year, current_term,subject,catalog_nbr)
    sections.each do | list |
        sections = list[2]
        @sections  << sections
    end

    respond_to do |format|
      format.html { render action: "index" }
      format.json { render json: @sections }
    end
  end

  # TODO: Handle in update-attributes method
  # https://bitbucket.org/botimer/workshop-admin/issue/86
  # TODO: Use validates_associated so update does all existence checking and standard error handling on @workshop.save
  def contact_user
    @workshop = Workshop.find(params[:id])
    # TODO: permit specific attributes so update can authorize them implicitly
    Rails.logger.debug "WorkshopsController.contact_user \n----------------------\nparams: #{params}\n---------------------------------"
    authorize! :update, @workshop
    # contact
    if( params['name'].blank? || params['value'].blank?)
      contact_user = nil
    elsif( params['name'] == 'contact-user-uniqname' )
      contact_user = User.where(:uniqname => params['value']).first
      if(contact_user.nil?)
        # report error?
        Rails.logger.info "WorkshopsController.contact_user \n----------------------\nparams: #{params}\n---------------------------------"
        contact_user = User.find_or_create_from_directory params['value']
      end
    end

    unless contact_user.nil?
      @workshop.update_attribute 'contact_user', contact_user
    end

    respond_to do |format|
      format.html # new.html.erb
      format.json{ render json: @workshop.to_json(:include => :contact_user) }
    end
  end

  private

  def current_term
    #  return present year
    #  needs to be improved for fall semester
    return time_to_term_code(Time.zone.now)

  end

  def current_year
    #  return present year
    #  needs to be improved for fall semester
    return Time.zone.now.year
  end

  # TODO: Move to appropriate module
  def time_to_term_code(time = Time.zone.now)
    #  return 1 summer
    #  return 2 fall -- July 15
    #  return 3 winter
    #  return 4 spring -- April 21
    #  return 5 summerspring

    date = time.to_date
    beg_year = date.at_beginning_of_year
    spring_start = beg_year + 3.months + 20.days
    summer_end   = beg_year + 6.months + 14.days

    if date.between?(beg_year, spring_start)
      return 3
    elsif date.between?(spring_start, summer_end)
      return 5
    else
      return 2
    end
  end

  def update_requested_times(old_times = [], new_times_hash = {})

    changes2save = false
    old_times_hash = {}
    old_times.each do |item|
      key = "#{item.time_range.data_start_time} #{item.time_range.data_end_time}"
      old_times_hash[key] = item
    end

    new_times = []
    unless new_times_hash.nil? || new_times_hash.empty?
      new_times_hash.each do |key, value|
        unless value[:day].blank? || value[:time0].blank? || value[:time1].blank?
          time_range = TimeRange.from_parts(value[:day], value[:time0], value[:time1])
        end
        unless time_range.blank?
          item = RequestedTime.new(:time_range => time_range)
        end
        unless item.blank?
          new_key = "#{item.time_range.data_start_time} #{item.time_range.data_end_time}"
          if old_times_hash.has_key? new_key
            new_times << old_times_hash.delete(new_key)
          else
            new_times << item
            changes2save = true
          end
        end
      end
    end

    unless old_times_hash.nil? || old_times_hash.empty?
      changes2save = true
    end

    return { :changes2save => changes2save, :new_times => new_times }
  end

  def liquify(workshop)
    workshop.instructor_url = workshop_url(workshop)
    workshop.short_eval_url = short_url(workshop.id.to_s(36))
    workshop.scheduler_url = schedule_workshop_url(workshop)
    workshop.requested_times_list = []
    workshop.requested_times.each do |rt|
      workshop.requested_times_list << rt
    end
    workshop.requested_times_count = workshop.requested_times_list.size
    workshop.instructors_list = []
    workshop.instructors.each do |i|
      workshop.instructors_list << i
    end
    workshop.instructors_count = workshop.instructors_list.size
    workshop
  end    

  def new_request_notification(workshop_map = {})
    # Rails.logger.debug "WorkshopsController.new_request_notification workshops: #{workshops.inspect}"
    recipients = User.want_notififications.all

    unless recipients.blank?
      et = EmailTemplate.find_by_name_and_active('new_request', true)
      if et.nil?
        Rails.logger.warn "WorkshopsController.new_request_notification ERROR: No new_request template found"
      elsif workshop_map.empty?
        Rails.logger.warn "WorkshopsController.new_request_notification ERROR: No workshops in list"
      else
        workshops = []
        workshop_map.each do |path, workshop|
          workshops << liquify(workshop)
        end
        params = {}
        params['workshops'] = workshops
        params['new_request_count'] = Workshop.requested.count
        params['no_time_count'] = Workshop.unscheduled.count
        params['no_room_count'] = Workshop.unbooked.count
        params['no_instructor_count'] = Workshop.unassigned.count
        params['need_stats_count'] = Workshop.needs_stats.count

        Rails.logger.debug "CommentsController.new_request_notification \n~~~~~~~----------~~~~~~~~~~\n#{params['workshops'].as_json}"
        # Rails.logger.debug "CommentsController.new_request_notification \n#{et}\n#{et.subject}\n#{et.body}"
        body_template = Liquid::Template.parse(et.body)
        body = body_template.render params
        subj_template = Liquid::Template.parse(et.subject)
        subject = subj_template.render params
        # Rails.logger.debug "CommentsController.new_request_notification \n#{recipients}\n#{subject}\n#{body}"

        Notifier.new_request_email(recipients, subject, body).deliver
      end
    end
  end

  def units_as_string
    units = Unit.active.order(:name).pluck(:name)
    units.join('|')
  end

end
