class CommentsController < ApplicationController
  
  require 'event_notification'

  # GET /comments
  # GET /comments.json
  def index
    # Rails.logger.debug "CommentsController.index #{params}"
    @head_title = "Recent Email - #{@head_title}"
    page_num = params[:page_num]
    if page_num.nil?
      page_num = 0
    else
      page_num = page_num.to_i
    end

    page_size = params[:page_size]
    if page_size.nil?
      page_size = 16
    else
      page_size = page_size.to_i
    end

    if params[:workshop_id].nil? && params[:user_id].nil?
      @comments = Comment.recent_active.order{ created_at.desc }.limit(page_size).offset(page_num * page_size)
      @total_count = Comment.recent_active.count
    elsif params[:user_id].nil?
      @workshop = Workshop.find(params[:workshop_id])
      # need to page?
      @comments = @workshop.comments.order{ created_at.desc }.limit(page_size).offset(page_num * page_size)
      @total_count = @workshop.comments.count
    # Rails.logger.debug "CommentsController.index #{@workshop.as_json}\n#{@comments.as_json(:include => ['recipients', 'user'])}"
    else
      # TODO: get comments for specified user (why is this needed? Maybe we should remove this route?)
    end

    @page_num = page_num
    @page_size = page_size
    @last_page = @total_count / @page_size

    respond_to do |format|
      if @workshop.nil?
        format.html # index.html.erb
        format.json { render json: @comments.as_json(:include => ['recipients', 'user']) }
      else
        format.html { render :partial => "index", :layout => "fragment" } # index.html.erb
        format.json { render json: @comments.as_json(:include => ['recipients', 'user']) }
      end
    end
  end

  def respond
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
    @comment = Comment.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @comment }
    end
  end

  # GET /comments/new
  # GET /comments/new.json
  def new
    if params[:workshop_id].nil?
      @comment = Comment.new
    else
      @workshop = Workshop.find(params[:workshop_id], :include => [:requested_times, :instructors, :topics, :location])
      @comment = @workshop.comments.new
    end

    Rails.logger.debug "CommentsController.new \n#{@workshop.as_json(:include => [:requested_times, :instructors, :topics])}\n@comment"

    check_contacts = params[:context].present? && params[:context] == "contacts"
    send_eval_urls = params[:context].present? && params[:context] == "evaluations"
    room_support = params[:context].present? && params[:context] == "room_support"
    schedulers = params[:context].present? && params[:context] == "schedulers"
    show_instructor_eval_url = params[:context].present? && params[:context] == "instructor_eval_url"
    instructor = params[:instructor]

    render_partial = check_contacts || room_support

    errors = []
    @recipients = []
    recipients_map = {} 
    unless params[:respond_to].nil?
      old_comment = Comment.find(params[:respond_to])
      add_recipient(old_comment.user, @recipients, recipients_map, true)
      old_comment.recipients.each do |ocr|
        add_recipient(ocr, @recipients, recipients_map, true)
      end
      @seed_subj = "Re: #{old_comment.subject}"
      @seed_msg = "\n\n\n> #{old_comment.content.gsub(/\r/, '').gsub(/(\n)\n+/, '\1').gsub(/(\n)/, '\1> ')}"
      render_partial = true
    end

    unless params[:instructor].nil? 
      instr = User.find(params[:instructor].to_i)
      unless instr.nil?
        add_recipient(instr, @recipients, recipients_map, true, 'possible instructor')
        render_partial = true
        if @workshop.present?
          result = render_email_template(@workshop, 'instructor')

          @seed_msg = result[:body]
          @seed_subj = result[:subject]
        end
      end
    end

    if send_eval_urls
      @workshop.instructors.each do |instructor|
        add_recipient(instructor, @recipients, recipients_map, true, 'instructor')
      end
      render_partial = true
      result = render_email_template(@workshop, 'eval_url')
      @seed_msg = result[:body]
      @seed_subj = result[:subject]
    end

    if show_instructor_eval_url
      @workshop.instructors.each do |instructor|
        add_recipient(instructor, @recipients, recipients_map, true, 'instructor')
      end
      render_partial = true
      result = render_email_template(@workshop, 'instructor_eval_url')
      @seed_msg = result[:body]
      @seed_subj = result[:subject]
    end

    if check_contacts
      result = render_email_template(@workshop, 'contact_user')
      @seed_msg = result[:body]
      @seed_subj = result[:subject]
    end

    unless @workshop.nil?
      add_recipient(@workshop.contact_user,@recipients, recipients_map, check_contacts, 'contact')
      unless @workshop.contact_user == @workshop.requested_by
        add_recipient(@workshop.requested_by, @recipients, recipients_map, check_contacts, 'requested-by')
      end
      @workshop.instructors.each do |instructor|
        add_recipient(instructor, @recipients, recipients_map, false, 'instructor')
      end
    end
    if can? :schedule, @workshop
      add_recipient(current_user, @recipients, recipients_map, true, 'scheduler')
    else
      add_recipient(current_user, @recipients, recipients_map, false, 'me')
    end

    if schedulers
      render_partial = true
      result = render_email_template(@workshop, 'email_schedulers')
      @seed_msg = result[:body]
      @seed_subj = result[:subject]
    end
  
    if room_support
      support_user = nil
      parts = @workshop.location.support_email.split('@')
      if parts.length >= 2
        if 'umich.edu' == parts[1]
          begin
            support_user = User.find_by_uniqname parts[0]
          rescue => e
            Rails.logger.debug "failed to find support_user by uniqname \"#{parts[0]}\""
          end
        end
      end
      if support_user.nil?
        begin
          support_user = User.find_by_uniqname @workshop.location.support_email
        rescue => e
          Rails.logger.debug "failed to find support_user by email address \"#{@workshop.location.support_email}\""
        end
      end

      if support_user.nil?
        # raise exception?
        errors << "Could not find #{@workshop.location.support_email} in user records. Please add a user with uniqname."
      else
        add_recipient(support_user, @recipients, recipients_map, true, 'room support')
        result = render_email_template(@workshop, 'room_support')
        @seed_msg = result[:body]
        @seed_subj = result[:subject]
      end
    end

    respond_to do |format|
      if errors.present?
        format.html { render text: errors[0], status: 400 }
        format.json { render json: @comment }
      elsif render_partial
        format.html { render :partial => "new", :layout => "fragment" } # index.html.erb
        format.json { render json: @comment }
      else
        format.html # new.html.erb
        format.json { render json: @comment }
      end
    end
  end

  # GET /comments/1/edit
  def edit
    @comment = Comment.find(params[:id])
  end

  # POST /comments
  # POST /comments.json
  def create
    
    @comment = Comment.new(params[:comment])
    unless params[:workshop_id].nil?
      @comment.workshop = Workshop.find(params[:workshop_id])
      unless @comment.workshop.nil?
        workshop_id = @comment.workshop.id
      end
    end
    if params[:user_id].nil?
      @comment.user = current_user
    else
      @comment.user = User.find(params[:user_id])
    end
    subject = @comment.subject

    email_recipients = params[:email_recipients]
    recipients = User.find(email_recipients)

    recipients.each do |recipient|
      @comment.recipients << recipient
    end

    unless recipients.empty?
      body = @comment.content
      Notifier.email_contact(recipients, current_user, subject, body, workshop_id).deliver

      notifier = EventNotification.new
      notifier.notify "email.sent", [ @comment.workshop ], current_user
    end

    respond_to do |format|
      if @comment.save
        format.html { redirect_to @comment, notice: 'Comment was successfully created.' }
        format.json { render json: @comment, status: :created }
      else
        format.html { render action: "new" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /comments/1
  # PUT /comments/1.json
  def update
    @comment = Comment.find(params[:id])

    respond_to do |format|
      if @comment.update_attributes(params[:comment])
        format.html { redirect_to @comment, notice: 'Comment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy

    respond_to do |format|
      format.html { redirect_to comments_url }
      format.json { head :no_content }
    end
  end

  def self.from_email(message, params)

    # we use mailman gem, and that brings in mail gem. 
    # RDocs are hard to find, so here are links:
    # http://rubydoc.info/gems/Mailman/frames
    # http://rubydoc.info/gems/mail/frames

    #Mailman.logger.debug "CommentsController.from_email \n#{message}"
    #Rails.logger.debug "CommentsController.from_email \n#{message}"
    workshop = Workshop.find(params['workshop_id'])

    from = CommentsController.find_user message.from.to_s
    # Mailman.logger.debug "message.from: #{message.from}\nfrom obj: #{from}"

    unless from.nil?
      body = ""
      if message.multipart?
        the_message = message.text_part.body.decoded
      else
        the_message = message.body.decoded
      end

      the_message.each_line {|s|
        break if s.include? "When replying, type your text above this line"
        body = "#{body}#{s}"
      }
      recipients = []
      also_sent = ""
      # Mailman.logger.debug "message.to: #{message.to}\nmessage.cc: #{message.cc}"
      addresses = []
      unless message.to.blank?
        addresses = addresses + message.to
      end
      unless message.cc.blank?
        addresses = addresses + message.cc
      end
      addresses.uniq!
      # Mailman.logger.debug "addresses: #{addresses}"

      # Mailman.logger.info "#{addresses}"
      addresses.each do |address|
        recipient = CommentsController.find_user address
        # Mailman.logger.debug "----------------\nprocessing address #{address} -- result: #{recipient} "
        if recipient.nil?
          if also_sent.empty?
            also_sent = "Also sent to: #{address.to_s}"
          else
            also_sent = "#{also_sent}, #{address.to_s}"
          end
        else
          recipients << recipient
        end
      end
      unless also_sent.empty?
        body = "#{body}\n\n#{also_sent}\n"
      end

      comment = workshop.comments.new(:content => body, :subject => message.subject)
      comment.user = from

      recipients.each do |recipient|
        comment.recipients << recipient
      end
      comment.save

      notifier = EventNotification.new
      notifier.notify "email.received", [ workshop ], from
    end
  end

  def self.find_user(str)
    # Mailman.logger.debug "CommentsController.find_user #{str}"
    address1 = str.to_s.gsub(/[\"\'\|\]\[]/, '')
    # Mailman.logger.debug "----------------\nprocessing address #{str.to_s} -- address1: #{address1} "

    address = Mail::Address.new address1

    unless address.nil? || address.domain.nil? || address.local.nil?
      if address.domain == "umich.edu"
        uniqname = address.local
        list = User.search_by_uniqname uniqname
        unless list.empty?
          if list[0].uniqname == uniqname
            return list[0]
          end
        end
      end
    end
    return nil
  end

  private

  def add_recipient(user, recipient_list, recipient_map, checked = false, role = nil)
    # Rails.logger.debug "CommentsController.add_recipient #{user.uniqname} #{checked} #{role}"
    if recipient_map.has_key? user.id
      # in this case, just update the user's role
      if recipient_map[user.id].role.nil?
        recipient_map[user.id].role = role
      else
        recipient_map[user.id].role = "#{recipient_map[user.id].role}, #{role}"
      end
    else
      # in this case, add a new EmailRecipient for the user
      er = EmailRecipient.new(:user => user)
      er.role = role
      er.checked = checked
      recipient_map[user.id] = er
      recipient_list << er
    end
  end

  def compose_email_body(workshop)
    descr = "Title: #{workshop.title}\nURL: #{workshop_url workshop}\n"
    unless workshop.requested_times.blank?
      descr = "#{descr}Requested time(s):\n"
      workshop.requested_times.each do |time|
        descr = "#{descr}\t#{time.start_time.strftime('%a, %m/%d/%Y from %-I:%M %p')} to #{time.end_time.strftime('%-I:%M %p')} (#{time.duration_minutes} minutes)\n"
      end
    end
    unless workshop.topics.blank?
      descr = "#{descr}Topics:\n"
      workshop.topics.each do |topic|
        descr = "#{descr}\t#{topic.title}\n"
      end
    end
    unless workshop.instructors.blank?
      descr = "#{descr}Instructors:\n"
      workshop.instructors.each do |instructor|
        descr = "#{descr}\t#{instructor.display_name} (#{instructor.uniqname})\n"
      end
    end
   return descr
  end

  def liquify(workshop)
    workshop.instructor_url = workshop_url(workshop)
    workshop.short_eval_url = short_url(workshop.id.to_s(36))
    workshop.scheduler_url = schedule_workshop_url(workshop)
    workshop.requested_times_list = []
    workshop.requested_times.each do |rt|
      workshop.requested_times_list << rt
    end
    workshop.requested_times_count = workshop.requested_times_list.size
    workshop.instructors_list = []
    workshop.instructors.each do |i|
      workshop.instructors_list << i
    end
    workshop.instructors_count = workshop.instructors_list.size
    workshop
  end    

  def render_email_template(workshop, template_name, default_email_template = nil)
    results = {}
    et = EmailTemplate.find_by_name_and_active(template_name, true)
    if et.nil?
      et = default_email_template
    end

    unless et.nil?
      params = {}
      params['workshop'] = liquify(workshop)
      params['new_request_count'] = Workshop.requested.count
      params['no_time_count'] = Workshop.unscheduled.count
      params['no_room_count'] = Workshop.unbooked.count
      params['no_instructor_count'] = Workshop.unassigned.count
      params['need_stats_count'] = Workshop.needs_stats.count
      Rails.logger.debug "CommentsController.render_email_template \n#{et}\n#{et.subject}\n#{et.body}"
      body_template = Liquid::Template.parse(et.body)
      results[:body] = body_template.render params
      subj_template = Liquid::Template.parse(et.subject)
      results[:subject] = subj_template.render params
      Rails.logger.debug "CommentsController.render_email_template #{results}"
    end
    results
  end

end
