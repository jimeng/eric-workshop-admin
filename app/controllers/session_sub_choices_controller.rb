class SessionSubChoicesController < ApplicationController
  load_and_authorize_resource
  include SessionSubChoicesHelper

  include SimpleDataTables
  accept_data_filter :all, :active, :archived
  alias_data_mapping :action_links, :session_sub_choice_action_links
  direct_data_mapping :session_type_name
  default_data_filter :active

  def index
    @head_title = "Session Sub-choices - #{@head_title}"
    respond_to do |format|
      format.html
      format.json { render json: mapped_json }
    end
  end

  def show
    @head_title = "Session Sub-choice - #{@head_title}"
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @session_sub_choice }
    end
  end

  def edit
    @head_title = "Edit Session Sub-choice - #{@head_title}"
    @session_types = SessionType.where(:active => true)

    @submit_form_path = session_sub_choice_path(@session_sub_choice)
    @submit_form_method = 'put'
    @session_sub_choice.save

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @session_sub_choice }
    end
  end


  def create
     # session_type
    session_type_id = params[:session_type]
    if session_type_id.nil? || session_type_id.empty?
      session_type_id = '1'
    end
    unless session_type_id.nil? || session_type_id.empty?
      session_type = SessionType.where(:id => session_type_id.to_i).first
    end
    unless session_type.nil?
      @session_sub_choice.session_type = session_type
    end

    respond_to do |format|
      if @session_sub_choice.save
        format.html { redirect_to session_sub_choices_path, notice: 'Session Sub-choice was successfully created.' }
        format.json { render json: @session_sub_choice, status: :created, session_sub_choice: @session_sub_choice }
      else
        format.html { render action: "new" }
        format.json { render json: @session_sub_choice.errors, status: :unprocessable_entity }
      end
    end
  end

  def new
    @head_title = "New Session Sub-choice - #{@head_title}"
    @session_types = SessionType.where(:active => true)
    @submit_form_path = session_sub_choices_path
    @submit_form_method = 'post'

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @session_sub_choice }
    end
  end

  def update
    session_type_id = params[:session_type]

    unless session_type_id.nil? || session_type_id.empty?
      session_type = SessionType.where(:id => session_type_id.to_i).first
    end
    unless session_type.nil?
      @session_sub_choice.session_type = session_type
    end
    respond_to do |format|
      if @session_sub_choice.update_attributes(params[:session_sub_choice])
        if @session_sub_choice.save
          format.html { redirect_to session_sub_choices_path, notice: 'Session Sub-choice was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { render action: "edit" }
          format.json { render json: @session_sub_choice.errors, status: :unprocessable_entity }
        end
      end
    end
  end


  def destroy
    @session_sub_choice.destroy

    respond_to do |format|
      format.html { redirect_to session_sub_choices_url }
      format.json { head :no_content }
    end
  end
end
