class RecentController < ApplicationController
include WorkshopsHelper
include SimpleDataTables

layout 'public'

datatables_model    :workshop
accept_data_filter  :recent
default_data_filter :recent
direct_data_mapping :workshop_scheduled_time,
                    :workshop_session_title,
                    :instructors_all,
                    :evaluation_link

def index
  @head_title = "Evaluation Links - #{@head_title}"
  respond_to do |format|
    format.html
    format.json { render json: mapped_json }
  end
end

end
