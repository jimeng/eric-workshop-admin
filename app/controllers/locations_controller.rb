class LocationsController < ApplicationController

  include SimpleDataTables
  accept_data_filter :all, :active, :archived
  alias_data_mapping :action_links, :location_action_links
  alias_data_mapping :support_email_link, :support_email_link

  default_data_filter :active

  def index
    @head_title = "Locations - #{@head_title}"
    respond_to do |format|
      format.html
      format.json { render json: mapped_json }
    end
  end

  def show
    @head_title = "Location - #{@head_title}"
    @location= Location.find(params[:id])
  end

  def edit
    @head_title = "Edit Location - #{@head_title}"
    @location= Location.find(params[:id])
    @submit_form_path = location_path(@location)
    @submit_form_method = 'put'

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @location }
    end
  end

  # GET /locations/new
  # GET /locations/new.json
  def new
    @head_title = "New Location - #{@head_title}"
    @location = Location.new
    @submit_form_path = locations_path
    @submit_form_method = 'post'

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @location }
    end
  end
  # POST /locations
  # POST /locations.json
  def create
    @location = Location.new(params[:location])

    respond_to do |format|
      if @location.save
        format.html { redirect_to locations_path, notice: 'Location was successfully created.' }
        format.json { render json: @location, status: :created, location: @location }
      else
        format.html { render action: "new" }
        format.json { render json: @location.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /locations/1
  # PUT /locations/1.json
  def update
    @location = Location.find(params[:id])

    respond_to do |format|
      if @location.update_attributes(params[:location])
        format.html { redirect_to locations_path, notice: 'Location was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @location.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /locations/1
  # DELETE /locations/1.json
  def destroy
    @location = Location.find(params[:id])
    @location.destroy

    respond_to do |format|
      format.html { redirect_to locations_url }
      format.json { head :no_content }
    end
  end



end
