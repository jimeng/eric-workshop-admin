require 'rubygems'
require 'json'
require 'google/api_client'


class CalendarsController < GoogleAwareController

  CAL_DELIM = "/"

  def grant_access
    # Rails.logger.debug "CalendarsController.grant_access \n#{@client.to_json} "

    load_client

    if @client.authorization.nil?
      # Rails.logger.debug "CalendarsController.grant_access @client.authorization.nil "

    elsif @client.authorization.access_token.nil?
      # Rails.logger.debug "CalendarsController.grant_access @client.authorization.access_token.nil"
      @client.authorization.state = params[:state]

      redirect_to @client.authorization.authorization_uri.to_s
    elsif @client.authorization.expired?
      # Rails.logger.debug "WorkshopsController.schedule @client.authorization.expired"
      @client.authorization.state = params[:state]

      redirect_to @client.authorization.authorization_uri.to_s
    else
      redirect_to params[:state]
    end
  end

  def revoke_access
    # Rails.logger.debug "CalendarsController.revoke_access "

    # Rails.logger.debug "session == #{session.inspect}"
    session.delete(:access_token)
    session.delete(:refresh_token)
    session.delete(:expires_at)
    # Rails.logger.debug "session == #{session.inspect}"

    current_user.update_attribute(:access_token, nil)
    current_user.update_attribute(:refresh_token, nil)
  
    redirect_to params[:state]
  end

  def instructor_calendar
    # Rails.logger.debug "CalendarsController.instructor_calendar \n#{params.inspect}\n#{session.inspect}\n--------------"

    day = params[:day]
    calendar_id = params[:calendar_id]

    if day.blank? || calendar_id.blank?
      # report error
      @event_hash = {}
      @event_hash[:errors] = [ 'Missing value for calendar-id or day' ]

    else
      load_client

      query_params = {}

      query_params[:calendarId] = calendar_id
      query_params[:timeMin] = "#{day}T00:00:00-05:00"
      query_params[:timeMax] = "#{day}T23:59:59-05:00"
      query_params[:access_token] = URI.encode_www_form_component(@access_token)
      query_params[:maxResults] = 200
      query_params[:singleEvents] = "true"
      query_params[:orderBy] = "startTime"

      # Rails.logger.debug "CalendarsController.instructor_calendar \n--------------------------\n#{query_params.inspect}\n--------------------------"

      method = @client.discovered_method('calendar.events.list', 'calendar', 'v3')

      result = @client.execute(
        :api_method => method,
        :parameters => query_params,
        :headers => { "Content-type" => 'application/json' }
      )
      if result.error?
        Rails.logger.error "CalendarsController.instructor_calendar ERROR code: #{result.status} -- error_message: #{result.error_message}"
      end

      @event_hash = ActiveSupport::JSON.decode(result.body)

    end

    # Rails.logger.debug "CalendarsController.instructor_calendar \n#{@event_hash.inspect}\n--------------"

    respond_to do |format|
      format.html { render :partial => "index", :layout => "fragment" }
      format.json { render json: google2fullcalendar( @event_hash['items'] ) }
    end    
  end

  def room_calendar
    # Rails.logger.debug "CalendarsController.room_calendar \n#{params.inspect}\n#{session.inspect}\n--------------"

    day = params[:day]
    calendar_id = params[:calendar_id]

    if day.blank? || calendar_id.blank?
      # report error
      @event_hash = {}
      @event_hash[:errors] = [ 'Missing value for calendar-id or day' ]

    else
      load_client

      query_params = {}

      year, month, day_of_month = day.split('-')
      now = Time.zone.now.to_datetime
      date = DateTime.new(year.to_i, month.to_i, day_of_month.to_i, 0, 0, 0, now.offset)

      query_params[:calendarId] = calendar_id
      query_params[:timeMin] = date.iso8601
      query_params[:timeMax] = date.end_of_day.iso8601
      query_params[:access_token] = URI.encode_www_form_component(@access_token)
      query_params[:maxResults] = 200
      query_params[:singleEvents] = "true"
      query_params[:orderBy] = "startTime"

      # Rails.logger.debug "CalendarsController.room_calendar \n--------------------------\n#{query_params.inspect}\n--------------------------"

      method = @client.discovered_method('calendar.events.list', 'calendar', 'v3')

      result = @client.execute(
        :api_method => method,
        :parameters => query_params,
        :headers => { "Content-type" => 'application/json' }
      )

      if result.error?
        Rails.logger.error "CalendarsController.room_calendar ERROR code: #{result.status} -- error_message: #{result.error_message}"
      end

      @event_hash =  ActiveSupport::JSON.decode(result.body)

    end
    # Rails.logger.debug "CalendarsController.room_calendar \n#{@event_hash.inspect}\n--------------"

    respond_to do |format|
      format.html { render :partial => "index", :layout => "fragment" }
      format.json { render json: google2fullcalendar( @event_hash['items'] ) }
    end    
  end

  def find_rooms
    # Rails.logger.debug "CalendarsController.find_rooms \n#{params.inspect}\n#{session.inspect}\n--------------"

    calendar_ids = []
    location_index = {}
    locations_no_calendar = []
    locations = Location.where(:active => true)
    locations.each do |location|
      if location.calendar_url.nil? || location.calendar_url.empty?
        locations_no_calendar << location
      else
        calendar_ids << location.calendar_url
        location_index[location.calendar_url] = location
      end
    end

    unless params[:workshop_id].blank?
      @workshop = Workshop.find(params[:workshop_id])
    end

    unless params[:day].blank? || params[:time0].blank? || params[:time1].blank?
      @time_range = TimeRange.from_parts params[:day], params[:time0], params[:time1]
    end

    # Rails.logger.debug "CalendarsController.find_rooms --> #{start_time.inspect} #{end_time.inspect}"

    if session[:access_token].nil? 
      errors = [ 'Google Calendar Access Not Authorized' ]
    elsif @time_range.present? && @time_range.valid? 
      load_client 

      method = @client.discovered_method('calendar.freebusy.query', 'calendar', 'v3')

      query = {}
      query['items'] = []
      calendar_ids.each do |calendar_id|
        query['items'] << { 'id' => calendar_id }
      end
      query['timeMin'] = @time_range.start_time.strftime("%Y-%m-%dT%H:%M:%S%:z")
      query['timeMax'] = @time_range.end_time.strftime("%Y-%m-%dT%H:%M:%S%:z")
      query['timeZone'] = "NorthAmerica/Detroit"

      json = query.to_json.to_s

      # Rails.logger.debug "CalendarsController.find_rooms ... free_busy_list: #{query.inspect}\n+++++++++++++++++++++\n#{json}\n+++++++++++++++++++++"

      result = @client.execute(
        :api_method => method,
        :body => json,
        :headers => { "Content-type" => 'application/json', "Content-length" => json.length.to_s }
      )

      if result.error?
        Rails.logger.error "CalendarsController.find_rooms ERROR code: #{result.status} -- error_message: #{result.error_message}"
        errors = [ result.error_message ]
      else

        free_busy_list = JSON.parse(result.body)

        # Rails.logger.debug "CalendarsController.find_rooms ... free_busy_list: #{free_busy_list.inspect}"
        
        ActiveRecord::Base.include_root_in_json = false

        locations_obj = {}
        unless free_busy_list.nil? || free_busy_list['calendars'].nil?
          # Rails.logger.debug "CalendarsController.find_rooms calendars: #{free_busy_list['calendars'].inspect}"
          free_busy_list['calendars'].each do |calendar_id, busyObj|
            location = location_index[calendar_id].as_json

            if busyObj.nil? || busyObj['busy'].nil? || busyObj['busy'].empty?
              location['status'] = 'free'
            else
              location['status'] = 'busy'
            end
            locations_obj[location['name']] = location 
          end
        end
        unless locations_no_calendar.nil? || locations_no_calendar.empty?
          locations_no_calendar.each do |location|
            l = location.as_json
            l['status'] = 'no-calendar'
            locations_obj[location.name] = l
          end
        end
      end
    else
      errors = ["Invalid Time Range: #{@time_range.display_time_range}"]
    end

    hash = {}
    unless locations_obj.nil?
      hash[:locations] = locations_obj
      @rooms = locations_obj.values
    end
    unless errors.nil?
      hash[:errors] = errors
    end
    hash['formatted_day'] = @time_range.display_day

    @formatted_day = @time_range.display_day

    Rails.logger.debug "CalendarsController.find_rooms #{@rooms}\n#{hash}"

    respond_to do |format|
      if errors.blank?
        format.html { render :partial => "index", :layout => "fragment" } # index.html.erb
        format.json { render :json =>  hash.to_json }
      else
        format.html { render text: errors[0], status: 400 } # index.html.erb
        format.json { render :json =>  hash.to_json }
      end
    end    
  end

  def find_instructors
    # Rails.logger.debug "CalendarsController.find_instructors \n#{params.inspect}\n--------------"
    hash = {}
    calendar_ids = []
    instructor_index = {}
    instructors_no_calendar = []
    instructors = User.where( role: ['instructor', 'admin', 'scheduler'] ).order{ 'lastname, firstname' }
    instructors.each do |instructor|
      if instructor.calendar_url.nil? || instructor.calendar_url.empty?
        instructors_no_calendar << instructor
      else
        calendar_ids << instructor.calendar_url
        instructor_index[instructor.calendar_url] = instructor
      end
    end

    unless params[:workshop_id].blank?
      @workshop = Workshop.find(params[:workshop_id])
    end

    unless params[:day].blank? || params[:time0].blank? || params[:time1].blank?
      @time_range = TimeRange.from_parts params[:day], params[:time0], params[:time1]
    end

    if session[:access_token].nil? 
      errors = [ 'Google Calendar Access Not Authorized' ]
    else
      load_client 

      method = @client.discovered_method('calendar.freebusy.query', 'calendar', 'v3')

      query = {}
      query['items'] = []
      calendar_ids.each do |calendar_id|
        query['items'] << { 'id' => calendar_id }
      end
      query['timeMin'] = @time_range.start_time.strftime("%Y-%m-%dT%H:%M:%S%:z")
      query['timeMax'] = @time_range.end_time.strftime("%Y-%m-%dT%H:%M:%S%:z")
      query['timeZone'] = "NorthAmerica/Detroit"

      json = query.to_json.to_s

      # Rails.logger.debug "CalendarsController.find_instructors ... free_busy_list: #{query.inspect}\n+++++++++++++++++++++\n#{json}\n+++++++++++++++++++++"

      result = @client.execute(
        :api_method => method,
        :body => json,
        :headers => { "Content-type" => 'application/json', "Content-length" => json.length.to_s }
      )

      if result.error?
        Rails.logger.error "CalendarsController.find_instructors ERROR code: #{result.status} -- error_message: #{result.error_message}"
        errors = [ result.error_message ]
      else

        free_busy_list = JSON.parse(result.body)

        # Rails.logger.debug "CalendarsController.find_instructors ... free_busy_list: #{free_busy_list.inspect}"
        
        ActiveRecord::Base.include_root_in_json = false

        instructors_obj = {}
        unless free_busy_list.nil? || free_busy_list['calendars'].nil?
          # Rails.logger.debug "CalendarsController.find_instructors calendars: #{free_busy_list['calendars'].inspect}"
          free_busy_list['calendars'].each do |calendar_id, busyObj|
            instructor = instructor_index[calendar_id].as_json

            if busyObj.nil? || busyObj['busy'].nil? || busyObj['busy'].empty?
              instructor['status'] = 'free'
            else
              instructor['status'] = 'busy'
            end
            instructor['display_name'] = instructor_index[calendar_id].display_name
            instructor['email_address'] = instructor_index[calendar_id].email_address
            instructors_obj[instructor['uniqname']] = instructor 
          end
        end
        unless instructors_no_calendar.nil? || instructors_no_calendar.empty?
          instructors_no_calendar.each do |instructor|
            i = instructor.as_json
            i[:status] = 'no-calendar'
            i['display_name'] = instructor.display_name
            i['email_address'] = instructor.email_address
            instructors_obj[instructor.uniqname] = i
          end
        end
      end
    end

    @instructors = []
    unless instructors_obj.nil?
      hash[:instructors] = instructors_obj
      instructors.each do |instructor|
        @instructors << instructors_obj[instructor.uniqname]
      end
    end
    unless errors.nil?
      hash[:errors] = errors
    end
    hash['formatted_day'] = @time_range.display_day

    @formatted_day = @time_range.display_day

    # Rails.logger.debug "CalendarsController.find_instructors #{@instructors}"

    respond_to do |format|
      if errors.blank?
        format.html { render :partial => "index", :layout => "fragment" } # index.html.erb
        format.json { render :json =>  hash.to_json }
      else
        format.html { render text: errors[0], status: 400 } 
        format.json { render :json =>  hash.to_json }
      end         
    end    
  end

  def access

    success = false
    # Rails.logger.debug("CalendarsController.access 1\n#{params.inspect}")
    unless params[:code].blank?

      begin

        load_client

        Rails.logger.info "CalendarsController.access\n  client: #{@client}\n    code: #{params[:code]}"
        @client.authorization.code = params[:code]

        Rails.logger.info "CalendarsController.access\n  client: #{@client.as_json}"
        @client.authorization.fetch_access_token!

        calendar = @client.discovered_api('calendar', 'v3')

        method = @client.discovered_method('calendar.calendarList.list', 'calendar', 'v3')

        # Rails.logger.debug "\n***********\n#{method}\n***********\n"

        result = @client.execute(
          :api_method => method,
          :parameters => {}
        )

        if result.error?
          Rails.logger.error "CalendarsController.access ERROR code: #{result.status} -- message: #{result.error_message}"
        end
        #session[:authorization_code] = params[:code]

        #Rails.logger.debug("CalendarsController.access 2\n#{@client.inspect}\n------------\n#{result.inspect}")
        # Rails.logger.debug("deferred_url == #{deferred_url}")

        unless @client.authorization.access_token.nil?
          # Rails.logger.debug "CalendarsController.access 3\n#{@client.inspect}"
          session[:access_token] = @client.authorization.access_token
          session[:expires_at] = @client.authorization.expires_at
          session[:refresh_token] = @client.authorization.refresh_token
          # can we store tokens and restore the client as needed?
          current_user.update_attribute(:refresh_token, @client.authorization.refresh_token)
          current_user.update_attribute(:access_token, @client.authorization.access_token)
          success = true
        else
          logger.warn("CalendarsController.access unknown error on fetching access token")
        end
      rescue => e
        logger.error "\nCalendarsController.access\nError on fetching access token: #{e.class} -- #{e.message}\n"
        logger.error e.backtrace.join("\n")
      end
 
    end
    if success
      message = "You have granted calendar access."
    else
      message = "An error occurred.  Calendar access is required to schedule workshops."
    end
    if params[:state].present?
      deferred_url = params[:state]
      redirect_to deferred_url, notice: message
    else
      redirect_to root_url, notice: message
    end
  end

  def google2fullcalendar items
    events = []
    unless items.nil?
      items.each do |item|
        # Rails.logger.debug("CalendarsController.google2fullcalendar #{item.inspect} | #{item['summary']} | #{item['id']} |")
        event = {}
        unless item['id'].nil?
          event['id'] = item['id']
        end
         if item['summary'].nil?
          event['title'] = '-- busy --'
         else
          event['title'] = item['summary']
        end
        unless item['start'].nil? || item['start']['dateTime'].nil?
          event['start'] = item['start']['dateTime']
          event['allDay'] = false
        end
        unless item['end'].nil? || item['end']['dateTime'].nil?
          event['end'] = item['end']['dateTime']
          event['allDay'] = false
        end
        unless item['start'].nil? || item['start']['date'].nil?
          event['start'] = item['start']['date']
          event['allDay'] = true
        end
        unless item['end'].nil? || item['end']['date'].nil?
          event['end'] = item['end']['date']
          event['allDay'] = true
        end
        events << event
      end
    end
    # Rails.logger.debug("CalendarsController.google2fullcalendar\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n#{events.inspect}\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+")
    return events
  end

  def self.google_event_hash(workshop, title = nil, include_attendees = true)
    event = {}
    event['guestsCanInviteOthers'] = false
    if title.nil?
      event['summary'] = workshop.title
    else
      event['summary'] = title
    end
    event['start'] = { 'dateTime' => "#{workshop.scheduled_time.start_time.as_json}" }
    event['end'] = { 'dateTime' =>  "#{workshop.scheduled_time.end_time.as_json}" }
    # Rails.logger.debug "CalendarsController.google_event_hash #{workshop.instructors}"
    if include_attendees && workshop.instructors.present?
      event['attendees'] = []
      workshop.instructors.each do |instructor|
        email = instructor.uniqname
        unless email.include? '@'
          email = "#{email}@umich.edu"
        end
        event['attendees'] << { 'email' => email }
      end
    end

    event['description'] = compose_description workshop

    if workshop.location.blank?
      if workshop.location_other.blank?
        event['location'] = "---"
      else
        event['location'] = workshop.location_other
      end
    else
      event['location'] = "#{workshop.location.name} - #{workshop.location.building}"
    end
    event
  end

  def self.update_participants workshop
    updates = 0
    Rails.logger.debug "CalendarsController.update_participants\n----------------------\nworkshop: #{workshop.as_json}"
    unless workshop.blank? || workshop.instruction_event_id.blank?
      calendar_id, event_id = workshop.instruction_event_id.split(CAL_DELIM)
    end

    unless calendar_id.blank? || event_id.blank?

      #Rails.logger.debug "CalendarsController.update_participants\n#{calendar_id}\n#{event_id}"
      
      private_key_filepath = Settings.calendar_config.service_account.private_key
      client_id = Settings.calendar_config.service_account.client_id
      client_email = Settings.calendar_config.service_account.client_email

      client = Google::APIClient.new( :application_name => 'm-lib-calendar', :application_version => '0.0.x' )
      calendar = client.discovered_api('calendar', 'v3')

      Rails.logger.info "CalendarsController.update_participants -- calling load_from_pkcs12(#{private_key_filepath})\n#{client_id}\n#{client_email}"
      begin
        key = Google::APIClient::KeyUtils.load_from_pkcs12(private_key_filepath, 'notasecret')
      rescue => e
        Rails.logger.info "CalendarsController.update_participants -- call to load_from_pkcs12 failed; retrying\n#{e}"
        key = Google::APIClient::KeyUtils.load_from_pkcs12(private_key_filepath, 'notasecret')
      end
      client.authorization = Signet::OAuth2::Client.new(
        :token_credential_uri => 'https://accounts.google.com/o/oauth2/token',
        :audience => 'https://accounts.google.com/o/oauth2/token',
        :scope => "https://www.googleapis.com/auth/calendar",
        :issuer => client_email,
        :signing_key => key)
      
      # Rails.logger.debug "\n----------------------\n#{key}\n----------------------\n#{client.as_json}\n----------------------"
      begin
        client.authorization.fetch_access_token!
        if workshop.instructors.present?
          event_title_in_instruction_calendar = "#{workshop.title} - #{workshop.instructors.first.display_name}"
          event_title_in_location_calendar = "Library Workshop - #{workshop.instructors.first.display_name}"
          #title = "Library Workshop - #{workshop.instructors.collect{|i| i.display_name}.join(', ')}"
        else
          event_title_in_instruction_calendar = workshop.title
          event_title_in_location_calendar = "Library Workshop"
        end
        event = self.google_event_hash(workshop, event_title_in_instruction_calendar)
        current_event = current_event client, calendar_id, event_id
        # Rails.logger.debug "CalendarsController.add_or_update_instruction_calendar_event \ncurrent_event: #{current_event}"
        if current_event.present? && current_event['sequence'].present?
          event[:sequence] = current_event['sequence']
        end
        method = client.discovered_method('calendar.events.update', 'calendar', 'v3')
        query_params = { :calendarId => calendar_id, :eventId => event_id, :sendNotifications => true }
        result = process_google_request(client, query_params, method, event.to_json.to_s)
        unless result.blank?
          updates = updates + 1
        end
        unless workshop.blank? || workshop.location_event_id.blank?
          c_id, e_id = workshop.location_event_id.split(CAL_DELIM)
          event = self.google_event_hash(workshop, event_title_in_location_calendar, false)
          current_event = current_event client, c_id, e_id
          if current_event.present? && current_event['sequence'].present?
            event[:sequence] = current_event['sequence']
          end
          query_params = { :calendarId => c_id, :eventId => e_id, :sendNotifications => false }
          result = process_google_request(client, query_params, method, event.to_json.to_s)
          unless result.blank?
            updates = updates + 1
          end
        end

      rescue => e
        Rails.logger.error "ERROR -----\n#{e.class}\n----------------------\n#{e.response.as_json}\n----------------------\n#{e.request.as_json}"
      end
    end
    Rails.logger.debug "CalendarsController.update_participants: #{updates} calendar events were updated"
    updates
  end 

  def self.add_or_update_instruction_calendar_event client, workshop, send_notifications = false
    # Rails.logger.debug "CalendarsController.add_or_update_instruction_calendar_event\n----------------------\nworkshop: #{workshop.as_json}\nclient.present? #{client.present?}"
    unless workshop.blank?
      if workshop.instruction_event_id.blank?
        calendar_id = CalendarConfig.find_by_name('instruction_calendar_id').value
      else
        calendar_id, event_id = workshop.instruction_event_id.split(CAL_DELIM)
      end
      Rails.logger.debug "CalendarsController.add_or_update_instruction_calendar_event\n----------------------\ncalendar_id: #{calendar_id}\nevent_id: #{event_id}"

      if workshop.instructors.present?
        event_title_in_instruction_calendar = "#{workshop.title} - #{workshop.instructors.first.display_name}"
        #title = "#{workshop.title} - #{workshop.instructors.collect{|i| i.display_name}.join(', ')}"
      else
        event_title_in_instruction_calendar = workshop.title
      end
      event = self.google_event_hash(workshop, event_title_in_instruction_calendar)

      if event_id.present?
        current_event = current_event client, calendar_id, event_id
        # Rails.logger.debug "CalendarsController.add_or_update_instruction_calendar_event \ncurrent_event: #{current_event}"
        if current_event.present? && current_event['sequence'].present?
          event[:sequence] = current_event['sequence']
        end

        method = client.discovered_method('calendar.events.update', 'calendar', 'v3')
        query_params = { :calendarId => calendar_id, :eventId => event_id, :sendNotifications => send_notifications }
      else
        method = client.discovered_method('calendar.events.insert', 'calendar', 'v3')
        query_params = { :calendarId => calendar_id, :sendNotifications => send_notifications }
      end
      # Rails.logger.debug "CalendarsController.add_or_update_instruction_calendar_event \n ----------------------------------\n ++++++++++++++++++++++++++++++++++++\n#{query_params}\n#{event.to_json}\n#{method.name}\n ----------------------------------\n++++++++++++++++++++++++++++++++++++"
      result = process_google_request(client, query_params, method, event.to_json.to_s)
      unless result.blank?
        result_hash = ActiveSupport::JSON.decode(result.body)
      end
      # Rails.logger.debug "CalendarsController.add_or_update_instruction_calendar_event #{result_hash} \n----------------------\n#{result.body}\n----------------------"
      unless result_hash.nil? || result_hash['id'].nil?
        workshop.update_attribute 'instruction_event_id', "#{calendar_id}#{CAL_DELIM}#{result_hash['id']}"
      end
    end
  end

  def self.add_or_update_location_calendar_event client, calendar_id, workshop, changes = []
    unless workshop.blank?

      # Rails.logger.debug "CalendarsController.add_or_update_location_calendar_event \n----------------------\nchanges: #{changes}"
      if changes.present? && changes.include?('location') && workshop.location_event_id.present?
        update_location_calendar_url client, calendar_id, workshop
      end

      if workshop.location_event_id.blank?
        # use param calendar_id
      else
        calendar_id, event_id = workshop.location_event_id.split(CAL_DELIM)
      end

      if workshop.instructors.present?
        event_title_in_location_calendar = "Library Workshop - #{workshop.instructors.first.display_name}"
        #event_title_in_location_calendar = "Library Workshop - #{workshop.instructors.collect{|i| i.display_name}.join(', ')}"
      else
        event_title_in_location_calendar = "Library Workshop"
      end
      event = google_event_hash(workshop, event_title_in_location_calendar, false)

      if event_id.present?
        current_event = current_event client, calendar_id, event_id
        # Rails.logger.debug "CalendarsController.add_or_update_location_calendar_event \ncurrent_event: #{current_event}"
        if current_event.present? && current_event['sequence'].present?
          event[:sequence] = current_event['sequence']
        end

        method = client.discovered_method('calendar.events.update', 'calendar', 'v3')
        query_params = { :calendarId => calendar_id, :eventId => event_id, :sendNotifications => false }
      else
        method = client.discovered_method('calendar.events.insert', 'calendar', 'v3')
        query_params = { :calendarId => calendar_id, :sendNotifications => false }
      end
      result = process_google_request(client, query_params, method, event.to_json.to_s)
      unless result.blank?
        result_hash = ActiveSupport::JSON.decode(result.body)
      end
      # Rails.logger.debug "CalendarsController.add_or_update_location_calendar_event \n----------------------\n#{result}\n----------------------"
      unless result_hash.nil? || result_hash['id'].nil?
        workshop.update_attribute 'location_event_id', "#{calendar_id}#{CAL_DELIM}#{result_hash['id']}"
      end
    end
    result
  end

  def self.current_event client, calendar_id, event_id
    # Rails.logger.debug "CalendarsController.current_event #{calendar_id} #{event_id}"
    method = client.discovered_method('calendar.events.get', 'calendar', 'v3')
    # Rails.logger.debug "CalendarsController.current_event #{calendar_id} #{event_id} #{method.name}"
    query_params = { :calendarId => calendar_id, :eventId => event_id }
    result = process_google_request client, query_params, method
    # Rails.logger.debug "CalendarConfig.current_event result: #{result}"
    event_hash = result_hash = ActiveSupport::JSON.decode(result.body)
    # Rails.logger.debug "CalendarConfig.current_event event_hash: #{event_hash}"
    event_hash
  end

  def self.process_google_request(client, query_params, method, body = nil)
    unless method.blank? || query_params.blank?

      if body.present?
        headers = { "Content-type" => 'application/json', "Content-length" => body.length.to_s }
      else
        headers = { "Content-type" => 'application/json' }
      end
      

      # Rails.logger.debug "\n----------------------\nCalendarsController.process_google_request\n----------------------\n#{query_params}\n----------------------\n#{body}\n----------------------\n#{headers}\n----------------------"

      request = client.generate_request(
        :api_method => method,
        :parameters => query_params,
        :body => body,
        :headers => headers
      )

      # Rails.logger.debug "\n----------------------\nrequest: #{request}"

      result = client.execute(request)

      # Rails.logger.debug "   Response code: #{result.status}"

      if result.error?
        Rails.logger.error "CalendarsController.process_google_request ERROR code: #{result.status} -- error_message: #{result.error_message}"
      else
        unless result.data.blank?
          case result.data
          when Google::APIClient::Schema::Calendar::V3::Event
            # Rails.logger.debug "     update.response.data :: Event: #{result.data}"
          when Hash
            # Rails.logger.debug "     update.response.data :: Hash: #{result.data}"
            result.data.each do |key, value|
              # Rails.logger.debug "                  #{key} == #{value}"
            end
          when String
            # Rails.logger.debug "     update.response.data :: String: #{result.data}"
          else
            # Rails.logger.debug "     update.response.data :: Other: #{result.class} #{result.data}"
          end
        else
          Rails.logger.warn "     NO PARSABLE DATA"
        end
      end
      if result.error?
        Rails.logger.warn "     Request led to error\n       #{result.error_message}"
      else
        # Rails.logger.debug "     No error found"
      end
    end
    # Rails.logger.debug "CalendarsController.process_google_request\n----------------------\n#{result.inspect}\n---------------"
    return result
  end

  # TODO: Implement authorization
  def self.update_location_calendar_url(client, calendar_id, workshop)
    # Rails.logger.debug "CalendarsController.update_location_calendar_url\n----------------------\ncalendar_id: #{calendar_id}\n----------------------\nworkshop: #{workshop}\n------------------------"
    # First: if workshop already has an event in a room calendar
    # and location changes, move the event to the new room's calendar
    # or delete it (if moved to non-library location)
    unless workshop.location_event_id.nil?
      parts = workshop.location_event_id.split(CAL_DELIM)
      unless parts.nil? || parts.length < 2
        if workshop.location.nil? || workshop.location.calendar_url.nil?
          begin
            remove_calendar_event client, workshop.location_event_id
            workshop.update_attribute 'location_event_id', nil
          rescue => e
            Rails.logger.warn "CalendarsController.update_location_calendar_url ERROR Unable to remove event from room calendar\n\tWorkshop: #{@workshop}\n\tError: #{e}"
          end
        else
          query_params = {}
          query_params[:calendarId] = parts[0]
          query_params[:eventId] = parts[1]
          query_params[:destination] = workshop.location.calendar_url
          query_params[:sendNotifications] = false
          method = client.discovered_method('calendar.events.move', 'calendar', 'v3')
          # Rails.logger.debug "CalendarsController.update_location_calendar_url query_params:\n----------------------\n#{query_params}\n----------------------"

          result = client.execute(
            :api_method => method,
            :parameters => query_params,
            :headers => { "Content-type" => 'application/json' }
          )

          if result.error?
            Rails.logger.error "CalendarsController.update_location_calendar_url ERROR code: #{result.status} -- error_message: #{result.error_message}"
          end

          # Rails.logger.debug "CalendarsController.update_location_calendar_url result:\n----------------------\n#{result.body}\n----------------------"
          result_hash = ActiveSupport::JSON.decode(result.body)

          # Rails.logger.debug "WorkshopsController.add_or_update_event_in_location_calendar result_hash:\n----------------------\n#{result_hash}\n----------------------"
          # Now update workshop.location_event_id to reference the new calendar
          # e.g. workshop.location.calendar_url + CAL_DELIM + result_hash[:id]
          workshop.update_attribute 'location_event_id', "#{workshop.location.calendar_url}#{CAL_DELIM}#{result_hash['id']}"
        end
      end
      # workshop.location_event_id = nil
    end
  end

  # TODO: Implement authorization
  def self.remove_calendar_event(client, full_event_id)
    unless full_event_id.blank?
      parts = full_event_id.split(CAL_DELIM)
      if parts.nil? || parts.length < 2
        Rails.logger.warn "CalendarsController.remove_calendar_event -- INVALID CALENDAR/EVENT ID -- #{parts}"
      else
        # Rails.logger.debug "WorkshopsController.remove_calendar_event #{parts}"
        query_params = {}
        query_params[:calendarId] = parts[0]
        query_params[:eventId] = parts[1]

        method = client.discovered_method('calendar.events.delete', 'calendar', 'v3')

        result = client.execute(
          :api_method => method,
          :parameters => query_params,
          :headers => { "Content-type" => 'application/json' }
        )

        if result.error?
          Rails.logger.error "CalendarsController.remove_calendar_event ERROR code: #{result.status} -- error_message: #{result.error_message}"
        end

      end
    end
  end


  def self.compose_description(workshop)
    descr = "Title: #{workshop.title}\nURL: #{workshop.instructor_url}\n"
    descr << "Contact: #{workshop.try('contact_user').try('display_name')} (#{workshop.try('contact_user').try('email_address')})\n"

    unless workshop.topics.blank?
      descr << "Topics:\n"
      workshop.topics.each do |topic|
        descr << "\t#{topic.title}\n"
      end
    end
    unless workshop.instructors.blank?
      descr << "Instructors:\n"
      workshop.instructors.each do |instructor|
        descr << "\t#{instructor.display_name} (#{instructor.uniqname})\n"
      end
    end

    begin
      disclaimer = CalendarConfig.find_by_name('disclaimer').value
    rescue => e
      Rails.logger.warn "CalendarsController.compose_description ERROR: #{e}"
    end

    if disclaimer.nil?
      disclaimer = "This event was created through the University Library's Workshop Administration system. To change any aspect of the workshop, please use the Workshop Administration system or contact the schedulers. Editing events here will not change plans made in the Workshop Administration system."
    end

    "#{descr}\n#{disclaimer}\n\n".html_safe
  end

end