class UsersController < ApplicationController

  # When the non-RESTful actions are refactored, this :only spec will probably go away
  # and may be replaced with an :except spec.
  load_and_authorize_resource :only => [:index, :show, :edit, :new, :create, :update, :destroy]

  include SimpleDataTables
  direct_data_mapping :user_calendar_link, :sort_name, :display_name, :role
  alias_data_mapping :action_links, :user_action_links
  alias_data_mapping :unit_assigned, :user_unit_assigned

  def index
    @head_title = "Users - #{@head_title}"
    respond_to do |format|
      format.html
      format.json { render json: mapped_json }
    end
  end

  # TODO: Consider a more factored search pattern and permission
  def uniqname_like
    uniqname = params[:query]
    @users = User.search_by_uniqname uniqname

    respond_to do |format|
      format.html
      format.json { render json: @users }
    end
  end

  def search_by_name
    @searchTerms = params[:query]
    Rails.logger.debug "UsersController.search #{@searchTerms}"
    if @searchTerms.blank?
      # send the search form
    else
      @results = UserDirectory.search_by_name @searchTerms
    end
    # Rails.logger.debug "UsersController.search #{@results}"

    if @results.blank?
      @results = { :people => [], :errors => [] }
    elsif @results[:people].blank? && @results[:errors].blank?
      @results[:errors] = []
      @results[:people] = []
    elsif @results[:people].blank?
      @results[:people] = []
    elsif @results[:errors].blank?
      @results[:errors] = []
    end

    respond_to do |format|
      format.html { render :partial => 'search', :layout => 'fragment' }
      format.json { render json: @results }
    end
  end

  def show
    @head_title = "User - #{@head_title}"
    @user  = User.find(params[:id])
  end

  def edit
    @head_title = "Edit User - #{@head_title}"
    @submit_form_path = user_path(@user)
    @submit_form_method = 'put'
    @units = units_as_string
    set_can_have_notification_preferences(@user)

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    @head_title = "New User - #{@head_title}"
    @submit_form_path = users_path
    @submit_form_method = 'post'
    @units = units_as_string
    set_can_have_notification_preferences(@user)

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end

  # POST /users
  # POST /users.json
  def create
    Rails.logger.debug "UsersController.create #{params.inspect}"

    respond_to do |format|
      if @user.save
        format.html { redirect_to users_path, notice: 'User was successfully created.' }
        format.json { render json: @user, status: :created, user: @user }
      else
        flash.now[:error] = "Cannot use that uniqname - #{@user.uniqname} is already in the system!"
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to users_path, notice: 'User was successfully updated.' }
        format.json { render json: @user  }
      else
        format.html { redirect_to edit_user_path(@user),
          :flash => { :error => "Cannot use that uniqname - #{@user.uniqname} is already in the system!" }}
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end

  # TODO: Consider a UserPreferences controller
  def options
    # available only for current_user
    @head_title = "User Preferences - #{@head_title}"
    Rails.logger.debug "UsersController.options #{params.inspect}"
    @user = current_user

    respond_to do |format|
      format.html # options.html.erb
      format.json { render json: @user }
    end
  end

  def revise_options
    # available only for current_user
    Rails.logger.debug "UsersController.options #{params.inspect}"
    @user = current_user

    if params[:name] == "calendar_url"
      @user.update_attribute(params[:name], params[:value])
    end

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to workshops_path, notice: 'User Options were successfully saved.' }
        format.json { render json: @user }
      else
        format.html
        format.json { render json: @workshop.errors, status: :unprocessable_entity }
      end
    end
  end

  def units_as_string
    units = Unit.active.order(:name).pluck(:name)
    units.join('|')
  end

  def set_can_have_notification_preferences(user)
    ability = Ability.new(user)
    @can_have_notification_preferences = ability.can? :schedule, Workshop
  end
end
