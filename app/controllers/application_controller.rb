class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :set_timezone
  before_filter :force_ssl, if: ->{ Settings.force_ssl }, except: [:not_authenticated]
  before_filter :sso_check, :except => [:not_authenticated]
  before_filter :require_login, :except => [:not_authenticated]
  before_filter :cache_current_user
  before_filter :set_default_title

  layout 'standard'

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to error_url, :alert => exception.message
  end

  def set_default_title
    @head_title = "Workshops - MLibrary"
  end

  protected

  def not_authenticated
    redirect_to login_path, alert: 'You must login to view this page.'
  end


  private

    def set_timezone
      # This should set the default timezone for the application,
      # but should it let a user's preferences override ??  
      Time.zone = 'America/Detroit'
    end

    # This check detects application sessions that have the underlying SSO
    # session change and terminates them. Because of the ordering, the
    # require_login check that follows will initiate the authentication flow
    # if needed, so we do not force a redirect here.
    def sso_check
      if logged_in? && sso_session_id != sso_prior_session_id
        Rails.logger.debug("==> Terminating session for #{current_user.uniqname} because of SSO session ID mismatch.")
        sso_auto_logout
        logout
      end
    end

    # Remember the SSO username and session ID for detecting changes
    def sso_remember(username)
      session[:remote_user] = username
      session[:sso_session_id] = sso_session_id
      Rails.logger.debug("==> Remembering user #{username} with SSO session: #{sso_session_id}")
    end

    def sso_forget
      Rails.logger.debug("==> Forgetting user #{sso_prior_username} with SSO session: #{sso_prior_session_id}")
      session.delete :remote_user
      session.delete :sso_session_id
    end

    def sso_prior_username
      session[:remote_user]
    end

    def sso_prior_session_id
      session[:sso_session_id]
    end

    def sso_username
      remote_user
    end

    def sso_session_id
      cookies["cosign-#{request.host}"]
    end

    def sso_auto_login(username)
      sso_remember username
    end

    def sso_auto_logout
      sso_forget
    end

    # Trigger the SSO logout workflow with a logged-out, destination URL
    def sso_logout
      redirect_to "https://weblogin.umich.edu/cgi-bin/logout?#{logout_now_url}"
    end

    def remote_user
      username = request.env["servlet_request"].try(:get_attribute, "REMOTE_USER")
      # Strip pollution by a literal '%{REMOTE_USER}' from JkEnvVar
      username unless ['%{REMOTE_USER}', ':'].include?(username)
    end

    def cache_current_user
      if logged_in?
        RequestStore.store[:current_user] = current_user
      else
        # Anonymous user, never used for updates for now
        # System updates are identified by nil
        RequestStore.store[:current_user] = false
      end
    end

    def force_ssl
      unless request.ssl?
        redirect_to protocol: 'https'
      end
    end


end
