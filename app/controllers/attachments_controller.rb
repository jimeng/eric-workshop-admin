class AttachmentsController < ApplicationController

  def file
    Rails.logger.info("AttachmentsController.file \n#{params}")
    file_path = Rails.root.join("attachments/#{params[:path]}")
    unless params[:format].nil?
      file_path = "#{file_path}.#{params[:format]}"
    end
    Rails.logger.info("AttachmentsController.file \n#{file_path}") 
    if File.exist?(file_path)
      send_file file_path
    else
      raise ActionController::MissingFile.new(file_path)
    end
  end

end