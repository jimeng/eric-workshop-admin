class SessionsController < ApplicationController
  skip_before_filter :require_login, except: [:destroy, :logout_now]

  def new
    # Note that CoSign intercepts and rejects requests that are not
    # authenticated, so there is no call to an sso_login flow.
    # If there is no SSO user, we assume we are not behind CoSign at all,
    # as in development mode, and render the login form. Users will generally
    # not have passwords set, so they will not be able to log in unless CoSign
    # is active or a password is expressly set at the console.
    username = sso_username
    if username.present?
      Rails.logger.debug("==> User lookup from SSO for uniqname: #{username}")
      user = User.find_by_uniqname(username)
      unless user
        email = username + "@umich.edu"
        Rails.logger.debug("==> User lookup from SSO for email: #{email}")
        user = User.find_by_uniqname(email)
      end

      if user
        # We trust the SSO, so do auto_login for found users
        Rails.logger.debug("==> User lookup from SSO successful: #{user.uniqname}, id: #{user.id}")
        finish_login user
      else
        Rails.logger.debug("==> User lookup from SSO unsuccessful, creating user '#{username}' from directory.")
        user = User.create_from_directory(username)
        if user
          finish_login user
        else
          access_denied!
        end
      end
    else
      Rails.logger.debug("==> SSO user not found, rendering login form.")
      render 'new'
    end
  end

  def create
    respond_to do |format|
      if @user = login(params[:username], params[:password])
        format.html { redirect_back_or_to root_url }
      else
        format.html { flash.now[:alert] = "Login failed."; render :action => "new" }
      end
    end
  end

  def logout_now
    logout
    sso_auto_logout
    redirect_to root_url
  end

  def destroy
    if remote_user.present?
      sso_logout
    else
      logout_now
    end
  end

  private

    def finish_login(user)
      sso_auto_login sso_username
      auto_login user
      redirect_back_or_to root_url
    end

    def access_denied!
      redirect_to root_url, :alert => "You are not permitted to log into this application."
    end

end
