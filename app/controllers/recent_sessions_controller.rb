class RecentSessionsController < ApplicationController
include WorkshopsHelper
include SimpleDataTables

datatables_model    :workshop
accept_data_filter  :recent_for, :allusersessions
default_data_filter :recent_for
direct_data_mapping :workshop_scheduled_time,
                    :workshop_session_title,
                    :instructors_all,
                    :evaluation_link

def filtered_scope
  datatables_model.send data_filter, current_user
end

def index
  @head_title = "Evaluation Links - #{@head_title}"
  respond_to do |format|
    format.html
    format.json { render json: mapped_json }
  end
end

end
