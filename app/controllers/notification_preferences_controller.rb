class NotificationPreferencesController < ApplicationController

  require 'event_notification'
  
  def index
    page_num = params[:page_num]
    if page_num.nil?
      page_num = 0
    else
      page_num = page_num.to_i
    end

    page_size = params[:page_size]
    if page_size.nil?
      page_size = 16
    else
      page_size = page_size.to_i
    end
    @user_id = params[:user_id]
    @workshop_id = params[:workshop_id]
    if @user_id.present?
      @notification_preferences = NotificationPreference.order{ created_at.asc }.limit(page_size).offset(page_num * page_size).where(:user_id => @user_id)
    elsif @workshop_id.present?
      @notification_preferences = NotificationPreference.order{ created_at.asc }.limit(page_size).offset(page_num * page_size).where(:workshop_id => @workshop_id)
    else
      # TODO: Remove? This route is not currently supported and may not be needed.
      @notification_preferences = NotificationPreference.order{ created_at.asc }.limit(page_size).offset(page_num * page_size).all
    end    
    @event_types = EventNotification.map_event_types
    @notifiers = EventNotification.map_notifiers
    @scopes = EventNotification.map_scopes

    respond_to do |format|
      format.html { render :partial => "index", :layout => "fragment" } # index.html.erb
      format.json { render json: @notification_preferences.as_json }
    end
  end

  def show
    Rails.logger.debug "NotificationPreferencesController.show #{params.as_json}"
    @user_id = params[:user_id]
    @workshop_id = params[:workshop_id]
    @notification_preference = NotificationPreference.find(params[:id])
    @event_types = EventNotification.map_event_types
    @notifiers = EventNotification.map_notifiers
    @scopes = EventNotification.map_scopes
    Rails.logger.debug "NotificationPreferencesController.show #{@notification_preference.as_json}"

    respond_to do |format|
      format.html { render :partial => "show", :layout => "fragment" } # index.html.erb
      format.json { render json: @notification_preferences.as_json }
    end
  end

  def new
    @notification_preference = NotificationPreference.new
    @event_types = EventNotification.list_event_types
    @notifiers = EventNotification.list_notifiers
    @scopes = EventNotification.list_scopes
    unless params[:user_id].blank?
      @user_id = params[:user_id]
      @submit_url = user_notification_preferences_path(@user_id)
    end
    unless params[:workshop_id].blank?
      @workshop_id = params[:workshop_id]
      @submit_url = user_notification_preferences_path(@workshop_id)
    end
    @submit_method = 'post'
    @success_msg = "Your rule has been added."

    respond_to do |format|
      format.html { render :partial => "new", :layout => "fragment" } # index.html.erb
      format.json { render json: @notification_preference.as_json }
    end    
  end

  def create
    @notification_preference = NotificationPreference.new(params[:np])
    @notification_preference.user = current_user
    unless params[:np][:active].present?
      @notification_preference.active = false
    end
    @notification_preference.save

    @user_id = params[:user_id]

    respond_to do |format|
      format.html { redirect_to user_notification_preference_path(@user_id, @notification_preference), notice: 'Rule was successfully created.' } # index.html.erb
      format.json { render json: @notification_preference.as_json }
    end    
  end

  def edit
    @notification_preference = NotificationPreference.find(params[:id])
    @event_types = EventNotification.list_event_types
    @notifiers = EventNotification.list_notifiers
    @scopes = EventNotification.list_scopes
    unless params[:user_id].blank?
      @user_id = params[:user_id]
      @submit_url = user_notification_preference_path(@user_id, @notification_preference)
    end
    unless params[:workshop_id].blank?
      @workshop_id = params[:workshop_id]
      @submit_url = user_notification_preference_path(@workshop_id, @notification_preference)
    end
    @submit_method = 'put'
    @success_msg = "Your rule has been updated."

    respond_to do |format|
      format.html { render :partial => "edit", :layout => "fragment" } # index.html.erb
      format.json { render json: @notification_preference.as_json }
    end    
  end

  def update
    @notification_preference = NotificationPreference.find(params[:id])
    Rails.logger.debug "NotificationPreferencesController.update #{params.as_json}"
    unless params[:np][:active].present?
      params[:np][:active] = false
    end

    @notification_preference.update_attributes params[:np]

    @user_id = params[:user_id]
    @workshop_id = params[:workshop_id]
    @event_types = EventNotification.map_event_types
    @notifiers = EventNotification.map_notifiers
    @scopes = EventNotification.map_scopes

    respond_to do |format|
      format.html { render :partial => "show", :layout => "fragment", notice: "Your rule was successfully updated"  } # index.html.erb
      format.json { render json: @notification_preference.as_json }
    end    
  end

  def destroy
    @notification_preference = NotificationPreference.find(params[:id])
    Rails.logger.debug "NotificationPreferencesController.destroy #{params.as_json}"
    @notification_preference.destroy
    if params[:user_id].present?
      redirect_url = user_notification_preferences_url(params[:user_id])
    elsif params[:workshop_id].present?
      redirect_url = workshop_notification_preferences_url(params[:workshop_id])
    else
      redirect_url = notification_preferences_url
    end
    
    respond_to do |format|
      format.html { redirect_to redirect_url }
      format.json { head :no_content }
    end
  end

end
