class StaticPagesController < ApplicationController
  skip_before_filter :require_login
  skip_before_filter :force_ssl

  def home
  end

  def help
  end
end
