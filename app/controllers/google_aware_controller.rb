require 'google/api_client'
require 'google/api_client/client_secrets'
require 'google/api_client/auth/installed_app'

class GoogleAwareController < ApplicationController

  def load_client
    Rails.logger.debug "GoogleAwareController.load_client "
    if session[:refresh_token].nil? && current_user.refresh_token.present?
      session[:refresh_token] = current_user.refresh_token
    end
    if session[:access_token].nil? && current_user.access_token.present?
      session[:access_token] = current_user.access_token
    end

    Rails.logger.debug "GoogleAwareController.load_client refresh_token: #{session[:refresh_token]}"
    if @client.nil?
      @client = Google::APIClient.new
      @client.authorization.client_id = CalendarConfig.where(:name => 'client_id').first.value
      @client.authorization.client_secret = CalendarConfig.where(:name => 'client_secret').first.value
      @client.authorization.redirect_uri = oauth2callback_url
      @client.authorization.scope = "https://www.googleapis.com/auth/calendar"
      @client.key = CalendarConfig.where(:name => 'api_key').first.value
      unless session[:refresh_token].nil?
        @client.authorization.update_token! session
      end

      # Rails.logger.debug "GoogleAwareController.load_client \n-----\n#{@client.to_json}\n-----"
      
    end
    Rails.logger.debug "GoogleAwareController.load_client client: #{@client}"
  end

end