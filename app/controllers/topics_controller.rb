class TopicsController < ApplicationController
  # We will probably fold the title action into regular update,
  # but for now, authorize it manually
  load_and_authorize_resource :except => [:title, :xeditable_list]

  include SimpleDataTables
  accept_data_filter :all, :active, :archived
  alias_data_mapping :action_links, :topic_action_links
  alias_data_mapping :title_and_category, :topic_title_and_category_name

  default_data_filter :active

  def index
    @head_title = "Topics - #{@head_title}"
    respond_to do |format|
      format.html
      format.json { render json: mapped_json }
    end
  end

  def xeditable_list
    authorize! :read, Topic
    topics = []
    Topic.where(:active => true).each do |t|
      begin
        topics << {:value => t.id, :text => t.title, :class => t.category.id, "data-category" => t.category.id }
      rescue => e
        Rails.logger.error "TopicsController.xeditable_list ERROR #{e}"
        Rails.logger.error e.backtrace.join('\n')
        Rails.logger.error "TopicsController.xeditable_list ERROR #{t.try('as_json')}"
      end
    end
    respond_to do |format|
      format.json { render json: topics }
    end    
  end

  def show
    @head_title = "Topic - #{@head_title}"
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @topic }
    end
  end

  def edit
    @head_title = "Edit Topic - #{@head_title}"
    @categories = TopicCategory.all
    @submit_form_path = topic_path(@topic)
    @submit_form_method = 'put'

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @topic }
    end
  end

  def title
    @topic = Topic.find params[:id]
    authorize! :update, @topic
    title = params[:value]
    @topic.update_attribute 'title', title

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json =>  @topic.to_json }
    end
  end

  # POST /topics
  # POST /topics.json
  def create
    respond_to do |format|
      if @topic.save
        format.html { redirect_to topics_path, notice: 'Topic was successfully created.' }
        format.json { render json: @topic, status: :created, topic: @topic }
      else
        format.html { render action: "new" }
        format.json { render json: @topic.errors, status: :unprocessable_entity }
      end
    end
  end
  # GET /topics/new
  # GET /topics/new.json
  def new
    @head_title = "New Topic - #{@head_title}"
    @categories = TopicCategory.all
    @submit_form_path = topics_path
    @submit_form_method = 'post'

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @topic }
    end
  end

  # PUT /topics/1
  # PUT /topics/1.json
  def update
    respond_to do |format|
      if @topic.update_attributes(params[:topic])
        if @topic.save
          format.html { redirect_to topics_path, notice: 'Topic was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { render action: "edit" }
          format.json { render json: @topic.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /topics/1
  # DELETE /topics/1.json
  def destroy
    @topic.destroy

    respond_to do |format|
      format.html { redirect_to topics_url }
      format.json { head :no_content }
    end
  end

end
