class WorkshopEventsController < ApplicationController

  def index
    @workshop = Workshop.find(params[:workshop_id])
    unless @workshop.nil?
      page_num = params[:page_num]
      if page_num.nil?
        page_num = 0
      else
        page_num = page_num.to_i
      end

      page_size = params[:page_size]
      if page_size.nil?
        page_size = 16
      else
        page_size = page_size.to_i
      end

      events_list = WorkshopEvent.order{ created_at.desc }.where(:workshop_id => @workshop.id).limit(page_size).offset(page_num * page_size)

      @events = WorkshopEventsController.process_events(events_list)

      @total_count = WorkshopEvent.where(:workshop_id => @workshop.id).count
      @page_num = page_num
      @page_size = page_size
      @last_page = @total_count / @page_size

      Rails.logger.debug("WorkshopEventsController.index #{@total_count} #{@page_num} #{@page_size} #{@last_page}")
    else
      @events = []
    end

    respond_to do |format|
      format.html { render :partial => "index", :layout => "fragment" }
      format.json { render :json =>  special_hash(@workshop).to_json }
    end

  end

  private

  def self.process_events(events_list = [])
    Rails.logger.debug "WorkshopEventsController.process_events #{events_list.length}"

    events = []

    events_list.each do |event|
      # TODO: move this stringification to WorkshopEvent, probably using localization over the switch block and string building
      if event.name.blank?
        # report error
        event.items = [ 'Unknown event' ]
      elsif event.name == 'workshop.create'
        details = JSON.parse event.details
        Rails.logger.debug "WorkshopEventsController.process_events\n----------------------\n#{details}\n----------------------"
        name = find_display_name(details['requested_by_id'], 'null')
        items = [ "Workshop \"#{details['title']}\" was requested by #{name[0]}" ]
        sec = 'blank';crs = 'blank';sub = 'blank'
        details.each do |key, value|
          unless key.blank? || value.blank? || key == 'title'
            items << "#{key}* #{value}"
            if key == 'section'
              sec = value
            elsif key == 'course'
              crs = value
            elsif key == 'subject'
              sub = value
            end
          end
        end
        sec.present?  ? items << 'sub_crs_sec*' + sub + ' ' + crs + ' ' + sec : ''
        event.items = items

      elsif event.name == 'workshop.update'
        # Rails.logger.debug "WorkshopEventsController.process_events event == #{event.inspect}"
        changes = []
        details = JSON.parse event.details.gsub('[nil,', '[null,')
        # Rails.logger.debug "WorkshopEventsController.process_events event.details == #{event.details}\ndetails == #{details}"
        details.each do |key, value|
          begin
            if value.include? '['
              value = JSON.parse value
            end
            case key
            # when 'accepted'
            #   changes << "accepted changed from #{show_nulls value[0]} to #{show_nulls value[1]}"
            when 'archived'
               value[1] == true ? changes << 'Workshop was archived' : changes << 'Workshop was un-archived'
            when 'class_related'
              changes << "Class Related changed from #{show_nulls translate(value[0].class)} to #{show_nulls translate(value[1].class)}"
            when 'course'
              changes << event_display_if_null("Course", value[0], value[1])
            when 'day'
              changes << "day changed from #{show_nulls value[0]} to #{show_nulls value[1]}"
            when 'expected_attendance'
              changes << event_display_if_null("Expected Attendance", value[0], value[1])
            when 'scheduler_notes'
              event.items = ['Scheduler notes changed', value[1]]
            when 'notes'
              if (value[0].present? || value[1].present?)
                changes << event_display_if_null("Notes", value[0], value[1])
              end
            when 'requester_questions'
              if (value[0].present? || value[1].present?)
                changes << "Requester Questions changed from #{show_nulls value[0]} to #{show_nulls value[1]}"
              end
            when 'section'
              changes << event_display_if_null("Section", value[0], value[1])

           when 'start_time'
              if value[1].blank? && value[0].blank?
                # just log it
                Rails.logger.warn "WorkshopEventsController.process_events ERROR: Event has null values for start_time: #{event.inspect}"
              elsif value[1].blank?
                changes << "Start time #{Date.parse(value[0])} removed"
              else
                changes << event_display_if_null("Starttime time",
                  value[0].blank? ? value[0] : Time.zone.parse(value[0]).strftime("%-I:%M %P"),
                  Time.zone.parse(value[1]).strftime("%-I:%M %P"))
              end

            when 'scheduled_time_start_time'
              unless value[0].blank?
                date0 = Time.zone.parse(value[0]).strftime("%A, %B %d %Y")
                time0 = Time.zone.parse(value[0]).strftime("%-I:%M %P")
              end
              unless value[1].blank?
                date1 = Time.zone.parse(value[1]).strftime("%A, %B %d %Y")
                time1 = Time.zone.parse(value[1]).strftime("%-I:%M %P")
              end
              if value[1].blank? && value[0].blank?
                # just log it
                Rails.logger.warn "WorkshopEventsController.process_events ERROR: Event has null values for start_time: #{event.inspect}"
              elsif value[1].blank?
                changes << "Scheduled day  #{Date.parse(value[0])} removed"
              elsif value[0].blank? # setting it from null to something
                changes << 'Scheduled day set to ' + date1
                changes << 'Start time set to ' + time1
              else # setting it from something to something
                if date0 != date1
                  changes <<  'Scheduled day changed from ' + date0 + ' to ' + date1
                end
                if time0 != time1
                  changes << 'Start time changed from ' + time0 + ' to ' + time1
                end
              end

            when 'scheduled_time_end_time'
              if value[1].blank? && value[0].blank?
                # just log it
                Rails.logger.warn "WorkshopEventsController.process_events ERROR: Event has null values for start_time: #{event.inspect}"
              elsif value[1].blank?
                changes << "End time #{Date.parse(value[0])} removed"
              else
                changes << event_display_if_null("End time",
                  value[0].blank? ? value[0] : Time.zone.parse(value[0]).strftime("%-I:%M %P"),
                  Time.zone.parse(value[1]).strftime("%-I:%M %P"))
              end

            when 'duration_minutes'
              changes << event_display_if_null("Workshop duration", value[0], value[1]) + ' minutes'
            when 'subject'
              changes << event_display_if_null("Subject", value[0], value[1])
            when 'title'
              changes << "Title Changed From '#{show_nulls value[0]}' to '#{show_nulls value[1]}'"
            when 'instruction_event_id'
              Rails.logger.debug "WorkshopEventsController.process_events #{key} :: #{value[0]} :: #{value[1]}" 
              if value[0].nil?
                val0 = ''
              else
                val0 = value[0].strip
              end
              if value[1].nil?
                val1 = ''
              else
                val1 = value[1].strip
              end
              if (val0 == "" || val0 == "/") && (val1 == "" || val1 == "/")
                # Not sure what happened here
              elsif val0 == "" || val0 == "/"
                changes << "Event added to instruction calendar"
              elsif val1 == "" || val1 == "/"
                changes << "Event removed from instruction calendar"
              else
                changes << "Event updated in instruction calendar"
              end
            when 'location_event_id'
              Rails.logger.debug "WorkshopEventsController.process_events #{key} :: #{value[0]} :: #{value[1]}" 
              if value[0].nil?
                val0 = ''
              else
                val0 = value[0].strip
              end
              if value[1].nil?
                val1 = ''
              else
                val1 = value[1].strip
              end
              if (val0 == "" || val0 == "/") && (val1 == "" || val1 == "/")
                # Not sure what happened here
              elsif val0 == "" || val0 == "/"
                changes << "Event added to room calendar"
              elsif val1 == "" || val1 == "/"
                changes << "Event removed from room calendar"
              elsif val0 == val1
                changes << "Event updated in room calendar"
              else 
                changes << "Event removed from one room calendar and added to another"
              end
            when 'library_location_required'
              changes << event_display_if_null("Library Location Required", translate(value[0].class), translate(value[1].class))
            when 'evaluation_setup'
              # changes << "evaluation_setup changed from #{show_nulls value[0]} to #{show_nulls value[1]}"
              value[1]  ? changes << "Evaluation was setup" :
                          changes << "Evaluation setting was changed to not yet setup"
            when 'registration_setup'
              # changes << "registration_setup changed from #{show_nulls value[0]} to #{show_nulls value[1]}"
              value[1] ? changes <<  "Registration was setup" :
                         changes <<  "Registration was changed to not needed or not yet setup"
            when 'contact_user_id'
              # changes << "Contact ID: changed from #{show_nulls value[0]} to #{show_nulls value[1]}"
              name = find_display_name(value[0], value[1])
              changes << event_display_if_null("Contact Name", name[0], name[1])
            when 'managed_by_id'
              name = find_display_name(value[0], value[1])
              (name[0] == "null") ? changes << "#{name[1]} started managing this request" :
                                    changes << "#{name[0]} stopped managing this request"
            when 'requested_by_id'
              name = find_display_name(value[0], value[1])
              changes << event_display_if_null("Requested by", name[0], name[1])
            when 'location_id'
               room = find_external_sys_name('Location', value[0], value[1])
               changes << event_display_if_null('Location', room[0], room[1])
            when 'location_other'
              changes << event_display_if_null("Location", value[0], value[1])
            when 'session_type_id'
              name = find_external_sys_name('SessionType', value[0], value[1])
              changes << event_display_if_null('Session Type', name[0], name[1])
            when 'attachments'
              changes << "Attachments changed from #{show_nulls value[0]} to #{show_nulls value[1]}"
            when 'topics'
              changes << "Topics changed changed from #{show_nulls value[0]} to #{show_nulls value[1]}"
            when 'statistics'
              changes << "Statistics changed from #{show_nulls value[0]} to #{show_nulls value[1]}"
            when 'instructor_assignments'
              changes << "Instructors assigned changed from #{show_nulls value[0]} to #{show_nulls value[1]}"
            when 'instructors'
              changes << "Instructors changed from #{show_nulls value[0]} to #{show_nulls value[1]}"
            when 'evaluation_id'
              name = find_external_sys_name('Evaluation', value[0], value[1])
              changes << event_display_if_null("Evaluation system", name[0], name[1])
            when 'registration_id'
              name = find_external_sys_name('Registration', value[0], value[1])
              changes << event_display_if_null('Registration System', name[0], name[1])
            when 'requested_times'
              changes << "requested_times changed from #{show_nulls value[0]} to #{show_nulls value[1]}"
            when 'deleted'
              Rails.logger.debug "WorkshopEventsController.process_events processing 'deleted' #{value[0]} #{value[1]}"
              if value[1] == 'true' || value[1] == true
                changes << "Workshop deleted"
              elsif value[1] == 'false' || value[1] == false
                changes << "Workshop undeleted"
              else
                changes << "Workshop deleted"
              end
            else
              changes << "#{key} changed from #{show_nulls value[0]} to #{show_nulls value[1]}"
            end

          rescue => e
            begin
              changes << "#{key} changed from #{show_nulls value[0]} to #{show_nulls value[1]}"
              Rails.logger.warn "WorkshopEventsController.process_events ERROR: #{e}\n----------------------\n#{event.inspect}\n\t#{e.backtrace.join("\n\t")}\n----------------------"
            rescue => e1
              changes << "#{key} changed, but details are not available"
              Rails.logger.warn "WorkshopEventsController.process_events UNRECOVERABLE ERROR: #{e}\n----------------------\n#{event.inspect}\n\t#{e.backtrace.join("\n\t")}\n----------------------"
            end

          end
        end

        unless changes.blank?
          event.items = changes
        end
      elsif event.name == 'workshop.remove-instructor'
        if event.details.blank?
          event.items = [ 'Instructor removed', 'Unknown Error' ]
        else
          Rails.logger.debug "WorkshopEventsController.process_events \nevent.details: #{event.details.inspect} \nevent: #{event.inspect}"
          details = JSON.parse event.details
          instructor = User.find(details['user_id'])
          event.items = [ "Removed #{instructor.display_name} as instructor" ]
        end
      elsif event.name == 'workshop.add-instructor'
        if event.details.blank?
          event.items = [ 'Instructor added', 'Unknown Error' ]
        else
          details = JSON.parse event.details
          instructor = User.find(details['user_id'])
          event.items = [ "Added #{instructor.display_name} as Instructor" ]
        end
      elsif event.name == 'workshop.deleted'
        event.items = [ 'Workshop deleted' ]
      else 
         Rails.logger.warn "WorkshopEventsController.process_events UNKNOWN EVENT: \n----------------------\n#{event.inspect}\n----------------------"
        
      end
      if event.items.blank?
        Rails.logger.warn "WorkshopEventsController.process_events UNKNOWN ERROR: \n----------------------\n#{event.inspect}\n----------------------"
        event.title = event.name
      # elsif event.items.length > 1
      #   # event.title = "#{event.items.length} attributes changed"
      else

        event.title = event.items[0]
      end
      events << event

    end

    Rails.logger.debug "WorkshopEventsController.process_events #{events.length} #{events_list.length}"

    return events

  end

  def self.event_display_if_null(title, val0, val1)
      str = ''
      (val0 == "null" || val0.blank?) ? str << title + " set to #{val1}" : str << title + " changed from #{val0} to #{val1}"
    return str
  end

  def self.find_display_name(val0, val1)
    if val0.blank? || val0.to_i < 1
      dn0 = "null"
    else
      dn0 = User.find(val0.to_i).try('display_name')
    end
    if val1.blank? || val1.to_i < 1
      dn1 = "null"
    else
      dn1 = User.find(val1.to_i).try('display_name')
    end
    return [dn0, dn1]
  end

  def self.find_external_sys_name(model, val0, val1)
    if val0.blank? || val0.to_i < 1
        ex0 = "null"
    else
      ex0 = model.constantize.find(val0.to_i)
      if ex0.nil?
        ex0 = "'not found'"
      else
        ex0 = "'#{ex0.name}'"
      end
    end
    if val1.blank? || val1.to_i < 1
      ex1 = 'null'
    else
      ex1 = model.constantize.find(val1.to_i)
      if ex1.nil?
        ex1 = "'not found'"
      else
        ex1 = "'#{ex1.name}'"
      end
    end
    return [ex0, ex1]
  end

  def self.show_nulls( str )
    if str.blank?
      str = 'null'
    end
    return str
  end

end
