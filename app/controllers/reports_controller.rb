class ReportsController < ApplicationController
include WorkshopsHelper
include SimpleDataTables

layout 'report_layout'

datatables_model    :workshop
accept_data_filter  :not_deleted, :active, :archived
direct_data_mapping :contact_user_name, :title,:location_name,:requester_user_name,
                    :format_created_date, :format_course ,:format_duration,
                    :instructors_for_report, :format_date,
                    :get_stats,:workshops_statistics_participants,:format_phys_items, :contact_email,
                    :workshops_session_type,:workshops_statistics_notes, :session_type_options, :format_id

default_data_filter :active

def index
  # TODO: Consider integrating check into CanCan, but be careful with extrinsic state
  authorize! :read, :reports unless library_staff?
  @head_title = "Instruction Summary - #{@head_title}"
  respond_to do |format|
    format.html
    format.json { render json: mapped_json }
  end
end

  def library_staff?
    # FIXME: Refactor into a better place -- Session, User, Ability, Helpers... Not sure where yet
    authzd_coll = request.env['servlet_request'].try(:get_attribute, 'AUTHZD_COLL')
    public_coll = request.env['servlet_request'].try(:get_attribute, 'PUBLIC_COLL')
    colls = (authzd_coll.to_s + public_coll.to_s).split(':').delete_if {|x| x.empty? }
    staff_token = Settings.libauth_tokens.staff_only
    Rails.logger.debug "User viewing reports, viewable collections: #{colls} -- Looking for #{staff_token}"
    colls.include?(staff_token)
  end

end
