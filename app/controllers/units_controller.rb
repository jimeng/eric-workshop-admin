class UnitsController < ApplicationController

  include SimpleDataTables
  accept_data_filter :all, :active, :archived
  alias_data_mapping :action_links, :unit_action_links

  default_data_filter :active

  def index
    @head_title = "Unit Name Choices - #{@head_title}"
    respond_to do |format|
      format.html
      format.json { render json: mapped_json }
    end
  end

  def show
    @head_title = "Unit Choice - #{@head_title}"
    @unit  = Unit.find(params[:id])
  end

  def edit
    @head_title = "Edit Unit Choice - #{@head_title}"
    @unit = Unit.find(params[:id])
    @submit_form_path = unit_path(@unit)
    @submit_form_method = 'put'

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @unit }
    end
  end

  def name
    @unit = Unit.find params[:id]
     name = params[:value]
    @unit.update_attribute 'name', name

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json =>  @unit.to_json }
    end
  end

  def create
    @unit = Unit.new(params[:unit])

    respond_to do |format|
      if @unit.save
        format.html { redirect_to units_path, notice: 'New Unit choice was successfully created.' }
        format.json { render json: @unit, status: :created, unit: @unit }
      else
        format.html { render action: "new" }
        format.json { render json: @unit.errors, status: :unprocessable_entity }
      end
    end
  end

  def new
    @head_title = "New Unit Choice - #{@head_title}"
    @unit =  Unit.new
    @submit_form_path = units_path
    @submit_form_method = 'post'

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @unit }
    end
  end

  def update
    @unit = Unit.find(params[:id])

    respond_to do |format|
      if @unit.update_attributes(params[:unit])
        format.html { redirect_to units_path, notice: 'Unit choice was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @unit.errors, status: :unprocessable_entity }
      end
    end
  end

  def delete
    @unit = Unit.find params[:id]
    @unit.destroy

    respond_to do |format|
      format.html { redirect_to units_path, :notice => "Unit choice was successfully deleted." }
      format.json { render json: @unit }
    end
  end

   def destroy
    @unit = Unit.find params[:id]
    @unit.destroy

    respond_to do |format|
      format.html { redirect_to units_path, :notice => "Unit choice was successfully deleted." }
      format.json { render json: @unit }
    end
  end

end
