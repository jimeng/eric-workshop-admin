class EvaluationsController < ApplicationController

  include SimpleDataTables
  accept_data_filter :all, :active, :archived
  alias_data_mapping :action_links, :evaluation_action_links

  default_data_filter :active

  def index
    @head_title = "Evaluation Applications - #{@head_title}"

    respond_to do |format|
      format.html
      format.json { render json: mapped_json }
    end
  end

  def show
    @head_title = "Evaluation Application - #{@head_title}"
    @evaluation = Evaluation.find(params[:id])
  end

  def edit
    @head_title = "Edit Evaluation Application - #{@head_title}"
    @evaluation  = Evaluation.find(params[:id])
    @submit_form_path = evaluation_path(@evaluation)
    @submit_form_method = 'put'

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @evaluation }
    end
  end

  # GET /evaluations/new
  # GET /evaluations/new.json
  def new
    @head_title = "New Evaluation Application - #{@head_title}"
    @evaluation = Evaluation.new
    @submit_form_path = evaluations_path
    @submit_form_method = 'post'

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @evaluation }
    end
  end
  # POST /evaluations
  # POST /evaluations.json
  def create
    @evaluation = Evaluation.new(params[:evaluation])

    respond_to do |format|
      if @evaluation.save
        format.html { redirect_to evaluations_path, notice: 'Evaluation was successfully created.' }
        format.json { render json: @evaluation, status: :created, evaluation: @evaluation }
      else
        format.html { render action: "new" }
        format.json { render json: @evaluation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /evaluations/1
  # PUT /evaluations/1.json
  def update
    @evaluation = Evaluation.find(params[:id])

    respond_to do |format|
      if @evaluation.update_attributes(params[:evaluation])
        format.html { redirect_to evaluations_path, notice: 'Evaluation was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @evaluation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /evaluations/1
  # DELETE /evaluations/1.json
  def destroy
    @evaluation = Evaluation.find(params[:id])
    @evaluation.destroy

    respond_to do |format|
      format.html { redirect_to evaluations_path }
      format.json { head :no_content }
    end
  end



end
