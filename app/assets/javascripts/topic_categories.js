$(document).ready(function() {

  var columnOpts = jQuery.extend({}, CommonCols.DefaultSet, {
    topic_count:  {sClass: "text-center"},
    order:        {sClass: "text-center"}
  });

  var topicCategoriesDT = setupDataTable('#categories',  { columnOpts: columnOpts }, {
    // This removes the top div where filtering and page size controls would be and
    // the bottom section where the record count and pagination controls would be.
    // The default sDom value to integrate with Bootstrap is:
    // "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
    sDom: "t",
    aaSorting: [],
    bPaginate: false,
    bFilter: false
  });

});
