$(document).ready(function() {

  var columnOpts = {
    workshop_scheduled_time:  { "sWidth": "20%", mRender: sortByOther('scheduled_time_start_time')  },
    workshop_session_title:   { "sWidth": "45%"  },
    instructors_all:          { "sWidth": "25%"  }
    // evaluation_link:          { sClass: "text-center" , "sWidth": "15%" }
  };

  var options = {
    "aaSorting": [[0, 'desc']],
    iDisplayLength: -1
  };

  var recentDT = setupDataTable('#recent', { columnOpts: columnOpts }, options );
});
