$(document).ready(function() {

  var columnOpts = {
    workshop_scheduled_time:  { "sWidth": "20%"  },
    workshop_session_title:   { "sWidth": "45%"  },
    instructors_all:          { "sWidth": "25%"  }
    // evaluation_link:            { sClass: "text-center" , "sWidth": "5%" }
  };

  var recent_sessionsDT = setupDataTable('#recent-sessions', { columnOpts: columnOpts, filters: '#recent-sessions-filters'}, { aaSorting: [[0, 'desc']] } );

});
