$(document).ready(function() {

  var topic_title = $('#topic_title').val() || '';
  var script_may_change_title = topic_title.length < 1;

  var columnOpts = jQuery.extend({}, CommonCols.DefaultSet, {
    title_and_category: CommonCols.NameRowHeader
  });

  var topicsDT = setupDataTable('#topics', { columnOpts: columnOpts, filters: '#topics-filters' });

});
