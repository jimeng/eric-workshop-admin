//FIXME: Put this function somewhere reasonable for the application and namespace everything.
//       Think about making it a jQuery extension currying the table object.

// Look through the table headers and build the column array, mapping the
// data-col property in as "mData", which must correspond to the dot-notation
// key in the data source. Also map in the options for this column by key.
// Any column that does not have a data-col property will use the default value.
// You may pass either a string selector or a jQuery object as the datatable.
// In case there is a weird table, the property can be overridden with the
// optional third parameter, e,g., ("col-id" to use data-col-id).
  function makeColumnDefs(datatable, options, dataProp) {
    options = options || {};
    dataProp = dataProp || 'col';
    return $('th', datatable).map(function() {
      var columnKey = $(this).data(dataProp);
      return $.extend({ mData: columnKey }, options[columnKey] || {});
    }).get();
  }

  function sortByOther(sortKey) {
    return function(data, type, row) {
      if (type === 'sort') {
        return row[sortKey] || '';
    
      } else {
        return data;
      }
    };
  }

  function friendlyBoolean(data, type, row) {
    return data ? 'Yes' : 'No';
  }

  function activateFilter(filters, btn) {
      $('li', filters).removeClass('active');
      $(btn).parentsUntil(filters, 'li').addClass('active');
      // TODO: Put up a spinner or some wait indicator...
    return function() {
      // TODO: Hide spinner
    };
  }

  function setupFilters(table, filters) {
    var source = $(table).data('source');
    if (source) {
      $('.btn-filter', filters).click(function(evt) {
        evt.preventDefault();
        var filter = $(this).data('filter');
        var filterUrl = source + "?filter=" + filter;
        $(table).dataTable().fnReloadAjax(filterUrl, activateFilter(filters, this));
      });
    }

    //FIXME: Pick up a filter query parameter if it exists and apply it to the data source on first run.
    //       It should be pretty rare that there is one at all, since the defaults will be smart and the
    //       switching is fast and done via AJAX (not changing the URL).
    return source;
  }

  function setupDataTable(selector, opts, nativeOpts) {
    opts = jQuery.extend({}, opts);
    columnOpts = opts.columnOpts || {};
    filters = opts.filters || false;

    nativeOpts = jQuery.extend({
      oLanguage: {
        sAccessibility: false
      }
    }, nativeOpts);

    var table = $(selector);
    $.extend(nativeOpts, {
      sAjaxSource: table.data('source'),
      aoColumns: makeColumnDefs(table, columnOpts),
    });


    if (opts.filters) {
      var filterSource = setupFilters(table, opts.filters);
      if (filterSource) {
        nativeOpts.sAjaxSource = filterSource;
        nativeOpts.fnStateSaveParams = saveStateWithFilters(table, filters);
        nativeOpts.fnStateLoadParams = loadStateWithFilters(table, filters);
      }
    }

    return table.dataTable(nativeOpts);
  }

  function setupDefaultDataTable(selector, filterSelector) {
    var defaultCols = jQuery.extend({}, CommonCols.DefaultSet);
    var opts = { columnOpts: defaultCols };
    if (filterSelector) {
      opts.filters = filterSelector;
    }
    return setupDataTable(selector, opts);
  }

  function saveStateWithFilters(table, filters) {
    return function(settings, data) {
      var activeFilter = $('li.active:first a.btn-filter', filters).data('filter');
      if (activeFilter) {
        data.activeFilter = activeFilter;
      }
    };
  }

  function loadStateWithFilters(table, filters) {
    return function(settings, data) {
      if (data.activeFilter) {
        var btn = $('li a.btn-filter#filter-' + data.activeFilter, filters);
        var filter = btn.data('filter');
        if (filter) {
          var filterUrl = $(table).data('source') + "?filter=" + filter;
          settings.sAjaxSource = filterUrl;
          activateFilter(filters, btn);
        }
      }
    };
  }

  // TODO: Look into properties for read-only definitions
  var CommonCols = {};
  CommonCols.Active = {
    sClass: 'active-attr',
    mRender: friendlyBoolean
  };
  CommonCols.ActionLinks = {
    bSortable: false,
    sClass: 'action-links'
  };
  CommonCols.NameRowHeader = {
    sClass: 'normal-text text-left',
    sCellType: 'th'
  };

  CommonCols.DefaultSet = {
    display_name: jQuery.extend({ mRender: sortByOther('sort_name') }, CommonCols.NameRowHeader),
    name:         CommonCols.NameRowHeader,
    active:       CommonCols.Active,
    action_links: CommonCols.ActionLinks
  };

  var ColRendering = {};
  ColRendering.Multi = function(funcs) {
    return function(data, type, row) {
      if (funcs[type]) {
        return funcs[type](data, type, row);
      } else if (funcs["default"]) {
        return funcs["default"](data, type, row);
      } else {
        return data;
      }
    };
  };
  ColRendering.Join = function(cols, delim) {
    return function(data, type, row) {
      return jQuery.map(cols, function(col) {
        return row[col];
      }).join(delim);
    };
  };

var utils = {};
utils.workshop_completed = function(w) {
  var s = w.scheduled_time_start_time;
  if (s) {
    var now = moment();
    s = moment(s);
    return now > s;
  }
  return false;
};

utils.prefixes = {
  'workshop': '/workshops'
};

utils.build_path = function(prefix, index, suffix) {
  return function(context) {
    var beg = App.root_prefix + (utils.prefixes[prefix] || '');
    var mid = (context && index) ? (context[index] || '') : '';
    mid = mid ? '/' + mid : '';
    var end = suffix ? ('/' + suffix) : '';
    return beg + mid + end;
  };
};

utils.build_workshop_path = function(suffix) {
  return utils.build_path('workshop', 'id', suffix);
};

utils.short_day_long_month = function(time) {
  var time = moment(time);
  if (time.isValid()) {
    return time.format('ddd MMMM D, YYYY');
  }
  return null;
};

utils.standard_time = function(time) {
  var time = moment(time);
  if (time.isValid()) {
    return time.format('h:mm a');
  }
  return null;
};

var paths = {};
paths.workshop          = utils.build_workshop_path();
paths.schedule_workshop = utils.build_workshop_path('schedule');
paths.stats_workshop    = utils.build_workshop_path('stats');
paths.instructors       = utils.build_workshop_path('instructor_assignments');

function asset_path(asset) {
  return App.asset_path + '/' + asset;
};

Handlebars.registerHelper('ifmany', function(count, options) {
  if (count && count > 1) {
    return options.fn(this);
  } else {
    return options.inverse(this);
  }
});

Handlebars.registerHelper('path', function(name) {
  return paths[name](this);
});

Handlebars.registerHelper('asset', function(asset) {
  return asset_path(asset);
});

Handlebars.registerHelper('joined_list', function(items, options) {
  var list = $.map(items, function(item) {
    return options.fn(item)
  }).join(options.inverse(items));
  return list;
});

Handlebars.registerHelper('short_day_long_month', function(time) {
  return utils.short_day_long_month(time);
});

Handlebars.registerHelper('standard_time', function(time) {
  return utils.standard_time(time);
});

$(function() {
  var input = document.createElement("input");
    if(('placeholder' in input)===false) {
    $('[placeholder]').focus(function() {
      var i = $(this);
      if(i.val() == i.attr('placeholder')) {
        i.val('').removeClass('placeholder');
        if(i.hasClass('password')) {
          i.removeClass('password');
          this.type='password';
        }
      }
    }).blur(function() {
      var i = $(this);
      if(i.val() === '' || i.val() === i.attr('placeholder')) {
        if(this.type=='password') {
          i.addClass('password');
          this.type='text';
        }
        i.addClass('placeholder').val(i.attr('placeholder'));
      }
    }).blur().parents('form').submit(function() {
      $(this).find('[placeholder]').each(function() {
        var i = $(this);
        if(i.val() == i.attr('placeholder'))
          i.val('');
      });
    });
  }
});
