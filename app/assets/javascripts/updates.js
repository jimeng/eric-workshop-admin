
var check_for_updates = function(timeout, url){
  // request updates
  $.ajax(url, {
    type: 'get',
    dataType : 'json',
    data : { seen : false },
    success : function(data) {
      var unseen_update_count = data.length;
      $('.unseen_update_count').text(unseen_update_count);
      if(unseen_update_count > 0) {
        $('.updates-reload').show();
        $('#noti_div').show();
        $('#no_noti').hide();
      } else {
        $('.updates-reload').hide();
        $('#noti_div').hide();
        $('#no_noti').show();
      }
    }, 
    error : function(jqXHR, textStatus, errorThrown){
      alert("ERROR " + textStatus);
    }
  });
  setTimeout(function() { check_for_updates(timeout, url); }, timeout);
};
$(document).ready(function (){
  var updates_link = $(".user_updates");
  if( updates_link.length > 0 ) {
    var timeout = updates_link.data('timeout');
    var url = updates_link.data('url');

    check_for_updates(timeout, url);
  }
  var mark_seen_links = $('.mark_seen_link');
  if( mark_seen_links.length > 0 ) {
    mark_seen_links.each(function(index, element) {
      var url = $(element).data('url');
      $.ajax(url, {
        type : 'put',
        dataType : 'json',
        data : { updates : { seen : 'true' } },
        success : function(data) {
          var unseen_update_count = $('.unseen_update_count').text() - 1;
          $('.unseen_update_count').text(unseen_update_count);
        }, 
        error : function(jqXHR, textStatus, errorThrown){
          alert("ERROR " + textStatus);
        }
      });
    });
  }
  $('.updates-reload').on('click', function(eventObj){
    var url = $(this).data('url');
    window.location = url;
  });
});