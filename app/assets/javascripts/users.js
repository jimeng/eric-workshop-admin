$(document).ready(function() {

  var columnOpts = jQuery.extend({}, CommonCols.DefaultSet, {
    unit_assigned:  {sClass: "text-center"}
  });

  var locationsDT = setupDataTable('#users', { columnOpts: columnOpts} );
});
