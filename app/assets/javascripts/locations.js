
$(document).ready(function() {
  var columnOpts = jQuery.extend({}, CommonCols.DefaultSet, {
    seats:     { sClass: "text-center" },
    computers: {
      mRender: ColRendering.Multi({
        sort: ColRendering.Join(['computer_type', 'computers'], ' - '),
        default: ColRendering.Join(['computers', 'computer_type'], ' / ')
      })
    }
  });
  var filters = '#locations-filters'

  var locationsDT = setupDataTable('#locations', { columnOpts: columnOpts, filters: filters } );
});
