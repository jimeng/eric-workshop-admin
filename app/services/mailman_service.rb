class MailmanService
  require 'mailman'

  def initialize(opts={})
    ::Rails.logger.info "MailmanService.new"
    @done = false
  end

  def start
    ::Rails.logger.info "MailmanService.start "
    @thread = Thread.new do
      ::Rails.logger.info "MailmanService.starting in new thread #{Thread.current.inspect}"
      launch_mailman
    end
    ::Rails.logger.info "MailmanService.start #{@thread.inspect}"

  end
  
  def stop
    ::Rails.logger.info "MailmanService.stop"
    @done = true
    @thread.join
  end

  def alive?
    @thread.alive?
  end

  def join
    @thread.join
  end

  private

  def launch_mailman
    # we use mailman gem, and that brings in mail gem. 
    # RDocs are hard to find, so here are links:
    # http://rubydoc.info/gems/mailman/frames
    # http://rubydoc.info/gems/mail/frames

    # Defaults to log/mailman.log, see config/settings.yml
    ::Rails.logger.info "MailmanService.launch_mailman"

    Mailman.config.logger = Logger.new(Settings.mailman.custom.log_path)

    Mailman.logger.info "#{Time.zone.now} -- Configuring Mailman"

    Mailman.config.poll_interval = Settings.mailman.config.poll_interval

    Mailman.config.pop3 = Settings.mailman.config.pop3.to_hash

    Mailman.config.ignore_stdin = true

    Mailman.config.graceful_death = Settings.mailman.config.graceful_death

    Mailman::Application.run do
      # routes here
      subject(/\(ws\:\s*(\d+)\)/) do |workshop_id|
        begin
          # this filters out messages that do not have the ws-id in the subject
          # Mailman.logger.info "Mailman.run  --  New message 0\nworkshop-id: #{workshop_id}\n--------------------------------------\nto: #{message.to}\nfrom: #{message.from}\nsubject: #{message.subject}\nbody: #{message.body}\n--------------------------------------\n#{Time.now}"
          params[:workshop_id] = workshop_id.to_i
          #
          CommentsController.from_email(message, params)
        rescue => e
          Mailman.logger.warn "#{Time.zone.now} mailman_app.run error processing email for workshop #{workshop_id} #{message.subject} #{e}"
        end
      end

      default do
        Mailman.logger.warn "#{Time.zone.now} mailman_app.run error processing email (No workshop specified) -- #{message.subject}"
      end
    end
  end

end
