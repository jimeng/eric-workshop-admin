
module SessionSubChoicesHelper

  def session_sub_choice_action_links(session_sub_choice)
   link_to(image_tag("edit16.png"), edit_session_sub_choice_path(session_sub_choice),
      {:class => "action-links" , :title => 'Edit information about this Sub-choice',  :alt => ''})
  end

  def session_type_name(session_sub_choice)
      session_sub_choice.try(:session_type).try(:name)
  end

end
