module LocationsHelper

  def location_action_links(location)
    can?(:update, location) ? link_to(image_tag("edit16.png"), edit_location_path(location),
    	{:class => "action-link", :title => "Edit information about this room",  :alt => "Edit information about this room"}) : ''
  end

  def support_email_link(location)
    mail_to location.support_email, location.support_email
  end

end
