module TopicCategoriesHelper
  def category_action_links(category)
    can?(:update, category) ? link_to(image_tag("edit16.png"), edit_topic_category_path(category),
    	{:class => "action-links" , :title => 'Edit information about this Category',  :alt => 'Edit information about this Category'}) : ''
  end

  def topic_count(category)
  	category.topics.count
  end

end
