module WorkshopsHelper
  def instructor_list(workshop)
    workshop.instructors.map{|i| h i.display_name}.join("<br/> ").html_safe
  end

  def instructor_ids(workshop)
    workshop.instructors.map(&:id)
  end

  def instructor_names(workshop)
    workshop.instructors.map(&:display_name)
  end

  def short_location_name(workshop)
    workshop.location_name(false)
  end

  def topic_list(workshop)
    workshop.topics.map {|t| h t.title}.join("<br/>").html_safe
  end

  def topic_id_list(workshop)
    list = []
    workshop.topics.each {|t| list << t.id}
    list.to_s
  end

  def details_list(workshop)
    render :partial => 'workshops/details' , :locals  => { :workshop => workshop }
  end

  def contact_user_name(workshop)
    workshop.try(:contact_user).try(:display_name) || "(Unknown)"
  end

  def contact_user_uniqname(workshop)
    workshop.try(:contact_user).try(:uniqname) || "(Unknown)"
  end

  def contact_user_sort_name(workshop)
    workshop.try(:contact_user).try(:sort_name) || "(Unknown)"
  end

  def workshops_statistics_participants(workshop)
    workshop.try(:statistics).try(:participants)
  end

  def workshops_statistics_unit2(workshop)
    workshop.try(:statistics).try(:unit2)
  end

  def workshops_statistics_notes(workshop)
    workshop.try(:statistics).try(:notes)
  end

  def workshops_session_type(workshop)
    workshop.try(:session_type).try(:name)
  end

  def requester_user_name(workshop)
    workshop.try(:requested_by).try(:display_name) || "(Unknown)"
  end

  def requester_user_sort_name(workshop)
    workshop.try(:requested_by).try(:sort_name) || "(Unknown)"
  end

  def contact_user_email(workshop)
    user = workshop.contact_user
    "#{user.uniqname}@umich.edu" if user
  end

  def workshop_scheduled_time(workshop)
    l workshop.scheduled_time_start_time, format: :short_datetime if workshop.try(:scheduled_time_start_time)
  end

  def workshop_session_title(workshop)
    if workshop.title.present?
      link_to(workshop.title, short_url(workshop.id.to_s(36)),
      {:class => "action-links" , :title => 'Open the evaluation for this Library Instruction session', :target => '_blank'})
    end
  end

  def evaluation_link(workshop)
    if workshop.id.present?
      link_to(image_tag("view_eval_results.png"), short_url(workshop.id.to_s(36)),
      {:class => "action-links" , :title => 'Open the evaluation for this Library Instruction session',
        :alt => 'Open the evaluation for this Library Instruction session'})
    end
  end

  def space_out_linebreaks(text)
    if text.nil?
      return ''
    else
      return simple_format(text.gsub(/([\n\f\r])/,' \1'))
    end
  end

  # needed in _details partial
  # FIXME: Delete this once the details partial is removed and we
  #        confirm that nothing else uses it.
  def requested_times(workshop)
    req_times = []
    workshop.requested_times.each do |rt|
      req_times << rt.time_range.display_short_d_long_m.html_safe
    end
    return req_times.join("<br/> ").html_safe
  end

  # NOTE: This is temporary duplication so we can ship performance
  #       improvements for the dashboard without soak testing the
  #       app for uses of the requested_times helper.
  def all_requested_times(workshop)
    workshop.requested_times.map do |t|
      {
        start_time: t.start_time,
        end_time: t.end_time
      }
    end
  end

  # needed for created_at in "Recent Activity" tab and show-view
  def short_day_display(text)
    if text.blank?
    else
      text.strftime("%a, %m/%d/%Y")
    end
  end

  # needed for created_at in "Recent Activity" tab
  def simple_time(datetime_obj)
    str = ""
    unless datetime_obj.nil?
      str = datetime_obj.strftime("%-I:%M %P")
    end
    return str
  end

  def managed_by_name(workshop)
    workshop.try(:managed_by).try(:display_name) || "(None)"
  end

  def managed_by_initials(workshop)
    initials = '  '
    if workshop.managed_by.present?
      names = workshop.try(:managed_by).try(:display_name).split
      if names.length > 0
        initials[0] = names.first[0]
      end
      if names.length > 1
        initials[1] = names.last[0]
      end
    end
    initials
  end

  def active_tab_class(tab_id, active_tab_id = 'review', in_list = true)
    active_tab_class = ''
    if tab_id == active_tab_id
      if in_list
        active_tab_class = "class=active"
      else
        active_tab_class = 'active'
      end
    end
    return active_tab_class
  end

# put the roles in here for now - so just 1 place to define them
  def get_roles()
    return [
      ['requester', 'requester'],
      ['librarian', 'librarian'],
      ['instructor', 'instructor'],
      ['scheduler', 'scheduler'],
      ['admin', 'admin']
    ]
  end

# how many instructors are there for a workshop
  def how_many_instructors(workshop)
    unless workshop.instructor_assignments{:workshop_id}.empty? ||
        # maybe the user_id (instructror's id) was removed somehow - there's an instructor_assignments record but no user
           workshop.instructor_assignments{:user_id}.empty?

      return workshop.instructor_assignments.count
    else
      return 0
    end
  end

# some things we don't need to report on the workshop create event detail
# item passed in is something like 'column_name*value'
  def massage_item(item)
    case item.split('*')[0]
      when 'class_related'
        return "Class Related*" + ( item.split('*')[1] ? 'Yes' : 'No' )
      when 'library_location_required'
        return "Library Location Required*" + ( item.split('*')[1] ? 'Yes' : 'No')
      when 'contact_user_id'
        return "Contact Name*" + User.find((item.split('*')[1]).to_i).try('display_name')
      when 'sub_crs_sec'
        if item.split('*')[1].count('blank') < 3
          return "Subject Course Section* " + item.split('*')[1]
        else
          return ''
        end
      when 'session_type_id'
        st0 = SessionType.find(item.split('*')[1].to_i).name
        return "Session Type*" + st0
      when 'expected_attendance'
        return "Expected Attendance* " +  item.split('*')[1]
      when 'notes'
        return "Notes*" +  item.split('*')[1]
      when 'requester_questions'
        return "Requester question(s)*" +  item.split('*')[1]
      when 'evaluation_id'
        eval_id = item.split('*')[1].to_i
        if eval_id < 1
          return "Evaluation system*Not yet set"
        else
          return "Evaluation system*#{Evaluation.find(eval_id).name}"
        end
      when 'created_at', 'updated_at', 'day', 'start_time', 'end_time', 'course', 'subject', 'section', 'id', 'requested_by_id'
        return ''
    else
      return item
    end
  end

  def workshop_link(workshop)
    link_to(workshop.title, workshop, :class => "workshop-link")
  end

  def stat_link(workshop)
    # the stats link is either not there at all (if today < sched date), or
    # Add Stats (if today >= today and stats not complete) or View Stats (if stats complete)
    # also, has to be 'scheduled' in the first place - so if workshop.day, end_time is nill then no show

    #the workshop has been taught, so can either add (if stats not complete) or view (if completed)
    if today_gt_workshop(workshop)
      if can?(:add_stats, workshop) && !workshop.statistics.completed
        link_to(image_tag("add16.png", :alt => "Add stats for this workshop"), stats_workshop_path(workshop),
         {:class => "action-links", :title => 'Add statistics for this session', :alt => 'Add statistics for this session'})
      elsif can? :read, workshop.statistics
        link_to(image_tag("view16.png", :alt => "View this workshop's stats"), stats_workshop_path(workshop),
         {:class => "action-links", :title => 'View  statistics for this session', :alt => 'View statistics for this session'})
      end
    end
  end

  def manage_link(workshop)
    workshop_join = []
    if workshop.eval_results_url.present?
      paddit = false
      if can?(:unclaim, workshop) || can?(:schedule, workshop)
        paddit = true
        workshop_join << link_to(image_tag("view_eval_results.png", :alt => ""), workshop.eval_results_url,
          { :title => "View Evaluation results for the #{workshop.title} instruction session", :target => "_blank",
            :class => "pull-left"})
      end
    end

    if can? :schedule, workshop
      #to line all 3 things up whether they are there or not, if there isnt an eval results icon, space the initials over more
      if paddit
        workshop_join << "<span class='manage-link'>"
      else
        workshop_join << "<span class='manage-link' style='padding-left:20px'>"
      end
      workshop_join << managed_by_initials(workshop) << "</span>"

      workshop_join << link_to('Manage', schedule_workshop_path(workshop), {:class => "action-links floatright",
        :title => "Manage the #{workshop.title} session request"})
    end
    workshop_join.join.html_safe
  end

  def workshop_action_links(unit)
    workshop_join = []
    workshop_join << manage_link(unit)
    workshop_join << '&nbsp;&nbsp;&nbsp;'
    workshop_join << stat_link(unit)
    workshop_join.join.html_safe
  end

  def claim_link(workshop)
    if workshop.instructors.length >= 1 || cannot?(:claim, workshop)
      instructor_for_table(workshop)
    else
      link_to '#', 'data-url' => workshop_instructor_assignments_path(workshop), :class => "btn btn-success btn-mini claim-workshop-button" do
      'Claim <i class="icon-thumbs-up"></i>'.html_safe
      end
    end
  end

  def details_link(workshop)
    link_to image_tag("info.png", :alt => "Show additional request information"),  '#',  :class => "show-details-button"
  end

  def instructors_all(workshop)
    insts = workshop.instructors
    text = []
    i = 0;
    while i < insts.length
        text << insts[i].display_name
        i +=1
    end
    text.join("<br/>").html_safe
  end

  def instructor_for_table(workshop)
    insts = workshop.instructors
    text = ''
    num_instr = insts.length
    if insts.length > 0
      text = insts[0].display_name
    end
    if insts.length > 1
      text = "#{text} (#{insts.length})"
    end
    return text
  end

  def instructors_for_report(workshop)
    insts = workshop.instructors
    text = []
    i = 0;
    while i < insts.length
      if i == (insts.length - 1 )
        text << "#{insts[i].display_name}"
      else
        text << "#{insts[i].display_name},"
      end
        i +=1
    end
    text.join("<br/>").html_safe
  end

  ## TODO: This needs to be moved somewhere else and made consistent
  def format_duration(workshop)
    if workshop.scheduled_time.present? && workshop.scheduled_time.valid?
      "<span title='Duration: #{workshop.scheduled_time.duration_minutes.to_i} min'> #{workshop.scheduled_time.display_short_day}
      <br/><small>#{workshop.scheduled_time.display_time0}-#{workshop.scheduled_time.display_time1}</small></span>"
    end
  end

  def format_date(workshop)
    if workshop.scheduled_time.present? && workshop.scheduled_time.valid?
       "#{workshop.scheduled_time.display_short_day} <br/><small>#{workshop.scheduled_time.display_time0}-#{workshop.scheduled_time.display_time1}</small>"
    end
  end

  def today_gt_workshop(workshop)
    workshop.scheduled_time.present? && workshop.scheduled_time.valid? && (workshop.scheduled_time.start_time.to_datetime <= Time.zone.now.to_datetime)
  end

  def workshop_filter_button(body, filter, title)
    return if cannot? "filter_#{filter}".to_sym, Workshop
    active = current_user.default_filter == filter.to_sym
    filter_button(body, filter, title, active)
  end

  def filter_button(body, filter, title, active = false)
    content_tag 'li', class: ('active' if active) do
      link_to body, '#', id: "filter-#{filter}", class: 'btn-filter', 'data-filter' => filter, title: title
    end
  end

  def a_helper_method(workshop)
    "This is the ID from the helper - #{workshop.id}"
  end

  def first_instructor(workshop)
      text = ''
      if workshop.instructors[0].present?
        text = workshop.instructors[0].uniqname
      end
      return text
  end

  def format_id(workshop)
    "L#{workshop.id}"
  end

  def contact_email(workshop)
    email = ""
    if workshop.contact_user.present? && workshop.contact_user.uniqname.present?
      if workshop.contact_user.uniqname.include? '@'
        email = workshop.contact_user.uniqname
      else
        email = workshop.contact_user.uniqname
        email << '@umich.edu'
      end
    end
    return email
  end

  def format_created_date(workshop)
    day   = I18n.l  workshop.created_at , format: :day_only
    stime = I18n.l  workshop.created_at , format: :time_0min
    "#{day} <br/><small>#{stime}</small>" if day && stime
  end

  def format_course(workshop)
    if workshop.course.present?
      course = workshop.subject + ' ' + workshop.course + ' ' + workshop.section
    else
      course = ''
    end
  end

  # for the session type options column in the instruction activity report
  def session_type_options(workshop)
    picks = SubChoicePick.where(:workshop_id => workshop.id).pluck(:session_sub_choice_id)
    name = []
    if picks.length > 0
      picks.each do | pick |
      name << SessionSubChoice.where(:id => pick).pluck(:name)
      end
    end
    return name
  end

  def get_stats(workshop)
    text = ''
    if workshop.statistics.present?
      # if workshop.statistics.unit2.present?
      #    text << "#{workshop.statistics.unit},"
      #    text << "#{workshop.statistics.unit2}"
      #    text.join("<br/>").html_safe
      #  else
         text << "#{workshop.statistics.unit}".gsub('|',';<br/>')
       # end
    else
      return ''
    end
  end

  def format_phys_items(workshop)
    if workshop.statistics.physical_items?
      return 'Yes'
    else
      return 'No'
    end
  end

  def thirty_days_ago_str
    I18n.l Time.zone.now - 30.days, format: :short
  end

  def one_year_ago_str
    I18n.l Time.zone.now - 1.year, format: :short
  end

  def tomorrow_str
    I18n.l Time.zone.now + 1.day, format: :short
  end

  def first_attachment_name(workshop)
    if workshop.attachments.present?
      workshop.attachments[0].attached_file_file_name
    end
  end

  def first_attachment_url(workshop)
    if workshop.attachments.present?
      asset_path(workshop.attachments[0].attached_file.url)
    end
  end

  def first_attachment_size(workshop)
    if workshop.attachments.present?
      number_to_human_size(workshop.attachments[0].attached_file_file_size)
    end
  end

  def attachment_count(workshop)
    workshop.attachments.try(:length) || 0
  end

  def format_scheduled_time(workshop)
    if workshop.scheduled_time.present? && workshop.scheduled_time.valid?
      workshop.scheduled_time.display_short_d_long_m.html_safe
    end
  end

  # for normal requester, if any sched time, location, instructor is set, then is is being managed.
  # If none of those is set, then it is not yet being managed
  def not_managed(workshop)
    if workshop.accepted ||
      workshop.scheduled_time_start_time.present? || workshop.instructors.present? ||  workshop.location_other.present? ||
      (workshop.library_location_required == true && workshop.location_id.present?)
    return false
    else
    return true
    end
  end

  def can_edit_ws(workshop)
    # requesters can edit up to the time when a sched time or instructor or room has been set (if it's not_managed)
    # or instructors can edit up to the time a scheduler makes an edit
    # or schedulers can edit it anytime.
    if ((can? :update, workshop) && (! workshop.deleted) && (not_managed(workshop)) ) ||
      (can? :schedule, workshop) || ( (can? :claim, Workshop) && (! workshop.accepted) )
      return true
    else
      return false
    end
  end

end
