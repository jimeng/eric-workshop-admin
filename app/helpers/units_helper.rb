module UnitsHelper

  def delete_unit_link(unit)
    can?(:update, unit) ? link_to(image_tag("delete_16.png"), unit_path(unit), {
          method:     "delete",
          controller: "units" ,
          confirm:    "Are you really really sure you want to delete the  #{unit.name}  unit choice?",
          alt:        'Delete the unit choice',
          title:      'Delete the unit choice' } ) : ' '
  end

  def edit_unit_link(unit)
    can?(:update, unit) ? link_to(image_tag("edit16.png"), edit_unit_path(unit),
      {:class => "action-link", :title => 'Edit this Unit name',  :alt => 'Edit this Unit name'}) : ''
  end

  def unit_action_links(unit)
    edit_unit_link(unit) + '&nbsp&nbsp&nbsp'.html_safe + delete_unit_link(unit)
  end
end
