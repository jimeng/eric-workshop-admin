module EvaluationsHelper
  def evaluation_action_links(evaluation)
    can?(:update, evaluation) ? link_to(image_tag("edit16.png"), edit_evaluation_path(evaluation),
     {:class => "action-link", :title => 'Edit this Evaluation connection info',  :alt => 'Edit this Evaluation connection info'}) : ''
  end

end
