module TopicsHelper

  def topic_action_links(topic)
    can?(:update, topic) ? link_to(image_tag("edit16.png"), edit_topic_path(topic),
    	{:class => "action-links" , :title => 'Edit information about this Topic',  :alt => 'Edit information about this Topic'}) : ''
  end

  def topic_title_and_category_name(topic)
  	"<strong>#{topic.title}</strong><br /><small>(Category: #{topic.category.name})</small>".html_safe
  end

end
