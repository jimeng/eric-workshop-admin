module RegistrationsHelper

  def registration_action_links(registration)
    can?(:update, registration) ? link_to(image_tag("edit16.png"), edit_registration_path(registration),
    	{:class => "action-link", :title => 'Edit this Registration connection setup',  :alt => 'Edit this Registration connection setup'}) : ''
  end

end
