module UpdatesHelper

  def update_title update
    title = ''
    case update.event_type
    when 'workshop.create'
      title << "New session requested by #{update.perpetrator.display_name}" 
    when 'workshop.instructor.add'
      title << "Instructor added by #{update.perpetrator.display_name}" 
    when 'workshop.instructor.remove'
      title << "Instructor removed by #{update.perpetrator.display_name}" 
    when 'workshop.room.add'
      title << "Room assigned by #{update.perpetrator.display_name}" 
    when 'workshop.instructor.remove'
      title << "Room unassigned by #{update.perpetrator.display_name}" 
    when 'email.sent'
      title << "Email sent by #{update.perpetrator.display_name}" 
    when 'email.received'
      title << "Email received from #{update.perpetrator.display_name}" 
    when 'workshop.update'
      title << "Session request updated by #{update.perpetrator.display_name}" 
    else
      title << "Unknown action by #{update.perpetrator.display_name} (#{update.event_type})"
    end
    title
  end

  def update_detail update

  end

end
