module UsersHelper
  def user_calendar_link(user)
    link_to user.calendar_url, user
  end

  def user_action_links(user)
    can?(:update, user) ? link_to(image_tag("edit16.png"), edit_user_path(user),
     {:class => "action-links", :title => 'Edit information about this User',  :alt => 'Edit information about this User'}) : ''
  end

  def user_unit_assigned(user)
    if !!user.unit
      'Yes'
    else
      'No'
    end
  end

end
