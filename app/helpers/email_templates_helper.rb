module EmailTemplatesHelper
  def email_template_action_links(email_template)
    can?(:update, email_template) ? link_to(image_tag("edit16.png"), edit_email_template_path(email_template),
     {:class => "action-link", :title => 'Edit this Email Template',  :alt => 'Edit this Email Template'}) : ''
  end

end
