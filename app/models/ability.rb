class Ability
  include CanCan::Ability

  def initialize(user)

    # We are currently not granting any anonymous permissions
    return unless user

    @user = user

    if respond_to? @user.role
      send @user.role
      check_maintain
    else
      Rails.logger.debug("Unrecognized role '#{@user.role}' for user '#{@user.id} (#{@user.uniqname})'")
    end
  end

  # define action :maintain as a synonym for (:create || :update)
  # define object :any to mean ability to maintain at least one of the model classes
  def check_maintain
    models = [EmailTemplate, Evaluation, Location, Registration, SessionType, Topic, Unit, User]
    models.each do |m| 
      if can?(:create, m) || can?(:update, m) 
        can :maintain, m
        can :maintain, :any
      end
    end
  end

  def admin
    scheduler
    # can [:create, :update], [EmailTemplate]
    can :manage, :all
  end

  def scheduler
    instructor
    can [:create, :update], [Evaluation, Location, Registration, SessionType, Topic, Unit, User, Attachment, Comment]
    can :manage, [Statistics, InstructorAssignment, TopicCategory, SessionSubChoice]
    # TODO: enforce archived flag changes as an attribute permission rather than an action
    # TODO: Decide if this is idiomatic enough in views (probably so):
    #       can? :update, @workshop, :archived
    #           as opposed to
    #       can? :archive, @workshop
    can [:archive, :schedule, :read, :add_stats, :destroy], Workshop
    can [:filter_unscheduled, :filter_unbooked, :filter_needs_stats, :filter_requested, :filter_needs_eval], Workshop

  end

  def instructor
    librarian
    can [:read, :update], Workshop
    can [:read], [Attachment, Comment, Evaluation, Location, Registration, SessionType, Topic, User]
    can :create, Comment
    can :update, Comment, { :user_id => @user.id }

    # Redundant only because scope without block does not check for scope
    # membership of the parameter, CanCan issue #715.
    can :add_stats, Workshop, Workshop.taught_by(@user) do |workshop|
      Workshop.taught_by(@user).include? workshop
    end

    can [:create], Statistics
    can :read, Statistics, Statistics.for_instructor(@user) do |statistics|
      statistics.completed || Statistics.for_instructor(@user).merge(Workshop.active).include?(statistics)
    end
    can :update, Statistics, Statistics.for_instructor(@user) do |statistics|
      Statistics.for_instructor(@user).merge(Workshop.active).include?(statistics)
    end

    can :email_contact, Workshop



    ##
    ## Permissions that do not make sense to fetch with accessible_by
    ##

    # Claiming is OK for any workshop that is not already instructed by a
    # particular user, since "claim" is a short-hand for assign-self.
    # Whether a claim action should be displayed based on if there is at
    # least one instructor (assigned?) is view logic, not permission logic.

    # We do not supply a scope because finding all workshops available for
    # self-assignment is not useful for any of our workflows. The common
    # scenario would be to find unassigned workshops, whether "shopping" as an
    # instructor or looking for coverage gaps as a scheduler.
    can :claim, Workshop do |workshop|
      true unless workshop.archived? || workshop.instructors.include?(@user)
    end
    can :unclaim, Workshop do |workshop|
      true if !workshop.archived? && workshop.instructors.include?(@user)
    end

    # Allow instructors to modify only their assignments. This is a companion
    # to :claim, but supports the RESTful assignment model.
    can [:create, :destroy], InstructorAssignment, user_id: @user.id

    can [:filter_mine, :filter_unassigned, :filter_recent], Workshop

  end

  def librarian
    requester
    can :read, :reports

  end

  def requester

    can :create, Workshop
    can :read, Workshop, contact_user_id: @user.id
    can :read, Workshop, requested_by_id: @user.id
    can :create, Comment do |comment|
      workshop = comment.workshop
      workshop.present? && [workshop.contact_user_id, workshop.requested_by_id].include?(@user.id)
    end

    # Requesters may update their own workshops until accepted
    can :update, Workshop, accepted: false, contact_user_id: @user.id

    can :read, User, id: @user.id

    can [:filter_active, :filter_archived], Workshop
  end

end
