class SubChoicePick < ActiveRecord::Base

  belongs_to :workshop
  belongs_to :session_sub_choice

  attr_accessible :session_sub_choice, :workshop, :session_sub_choice_id, :workshop_id

  def self.ws_has_pick(workshop, ssc_id)
    where(:workshop_id => workshop.id, :session_sub_choice_id => ssc_id)
  end

end
