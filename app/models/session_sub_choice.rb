class SessionSubChoice < ActiveRecord::Base
  has_many :sub_choice_picks
  has_many :workshops, :through => :sub_choice_picks
  belongs_to :session_type
  attr_accessible :active,  :name

  scope :active, -> {
    where active: true
  }

  def self.ws_has_options(workshop)
    active.where( :session_type_id => workshop.session_type)
  end

end
