class SessionType < ActiveRecord::Base
  has_many :workshops
  has_many :session_sub_choices
  attr_accessible :active, :description, :name

  scope :active, -> { where { active == true } }

end
