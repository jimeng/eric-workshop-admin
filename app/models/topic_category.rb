class TopicCategory < ActiveRecord::Base
  attr_accessible :name, :order

  has_many :topics, :foreign_key => 'category_id'

  def self.ordered
  	TopicCategory.order(:order, :name).all
  end

end
