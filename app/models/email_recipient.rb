class EmailRecipient < ActiveRecord::Base

  belongs_to :comment
  belongs_to :user

  attr_accessible :user, :comment

  # checked and role are not persisted
  attr_accessor :checked, :role
end
