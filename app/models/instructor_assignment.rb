class InstructorAssignment < ActiveRecord::Base
  include Audited

  belongs_to :workshop
  belongs_to :user

  attr_accessible :order, :user, :workshop
  validates :workshop_id, :uniqueness => {:scope => :user_id}

  liquid_methods(*(YAML::load_file(Rails.root.join('config', 'liquid_methods.yml'))['instructor_assignment'].keys))

end
