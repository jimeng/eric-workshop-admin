class Unit < ActiveRecord::Base
  attr_accessible :name, :active

  scope :active, -> {
    where active: true
  }

  scope :archived, -> {
    where active: false
  }

end
