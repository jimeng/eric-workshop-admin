require 'json'

class WorkshopObserver < ActiveRecord::Observer

  def after_update(record)
    Rails.logger.debug("WorkshopObserver.after_update \nrecord.inspect :: #{record.inspect} \nrecord.changes :: #{record.changes} \nrecord.changed :: #{record.changed} \nrecord.updated_by :: #{record.updated_by}")
    changes = {}
    unless record.nil?
      record.changes.each do |name, value|
        case name
        when 'updated_at'
          #ignore
        when 'timestamp'
          #ignore
        # when 'user_id'
          #ignore
        when 'created_at'
          #ignore
        else 
          changes[name] = value
        end
      end

      Rails.logger.debug("WorkshopObserver.after_update \n---------------------------\nchanges :: #{changes.inspect} \n---------------------------")
      unless changes.empty?
        begin
          event = WorkshopEvent.new :details => "#{JSON.generate(changes)}", :name => "workshop.update", :timestamp => record.updated_at
          event.workshop = record 
          event.user = record.updated_by
          event.save!
        rescue => e
          logger.warn("WorkshopObserver.after_update: ERROR #{e.class}\n#{e.message}")
        end
      else
        Rails.logger.debug "WorkshopObserver.after_update \nrecord.changes: #{record.changed}\nrecord.inspect: #{record.inspect}"
      end
    end
  end

  def after_create(record)
    # Rails.logger.info("WorkshopObserver.after_create #{record.inspect} #{record.updated_by.inspect}")
    event = WorkshopEvent.new :details => "#{record.to_json}", :name => "workshop.create", :timestamp => record.created_at
    event.workshop = record 
    event.user = record.updated_by
    event.save
  end

  def after_commit(record)
    # Rails.logger.info("WorkshopObserver.after_commit #{record.inspect}")
  end

  def after_destroy(record)
    Rails.logger.debug("WorkshopObserver.after_destroy \n#{record.inspect} \n#{record.changes}")
  end

end
