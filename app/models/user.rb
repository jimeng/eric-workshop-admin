class User < ActiveRecord::Base
  authenticates_with_sorcery!

  attr_accessor :other_names, :titles, :affiliations

# Setup accessible (or protected) attributes for your model

  has_many :requested_workshops, :class_name => 'Workshop', :foreign_key => 'contact_user_id'
  has_many :managed_workshops, :class_name => 'Workshop', :foreign_key => 'managed_by'
  has_many :instructor_assignments
  has_many :workshops, :through => :instructor_assignments
  has_many :workshop_events
  has_many :comments
  has_many :email_recipients
  has_many :comments, :through => :email_recipients, :source => :comment
  has_many :notification_preferences
  has_many :updates

  attr_accessible :active, :calendar_url, :firstname, :lastname, :role, :uniqname, :unit, :notify_new_request, :access_token, :refresh_token, :records_per_page 

  before_validation :strip_blanks

  def strip_blanks
    self.uniqname = self.uniqname.strip
  end

  validates_uniqueness_of :uniqname

  DEFAULT_FILTERS = {
    admin:      :active,
    scheduler:  :active,
    librarian:  :active,
    instructor: :mine,
    requester:  :mine,
    default:    :active
  }

  USER_FILTERS = {
    [:requester, :active]   => :active_for,
    [:requester, :archived] => :archived_for,
    [:default, :mine]       => :active_for,
    [:default, :recent]     => :recent_for
  }

  liquid_methods(*(YAML::load_file(Rails.root.join('config', 'liquid_methods.yml'))['user'].keys))

  def as_json(options={})
    options[:except] = [options[:except] || []].flatten
    options[:except] += [:crypted_password, :salt]
    super(options)
  end

  def sort_name
    "#{lastname}, #{firstname}"
  end

  def display_name
    "#{firstname} #{lastname}"
  end

  def email_address
    if uniqname.nil?
      email = ''
    elsif uniqname.include? '@'
      email = uniqname
    else
      email = "#{uniqname}@umich.edu"
    end
    return email
  end

  def default_filter
    DEFAULT_FILTERS[role.to_sym] || DEFAULT_FILTERS[:default]
  end

  def workshop_filter(filter)
    user_filter = USER_FILTERS[[role.to_sym, filter]] || USER_FILTERS[[:default, filter]]
    if user_filter
      Workshop.send(user_filter, self)
    else
      Workshop.send(filter)
    end
  end

  scope :want_notififications,    -> { where notify_new_request: true }

  # placeholder for a method that may consider 
  # abilities, preferences, etc
  def should_receive_notification(workshop)
    return can? :read, workshop
  end

  def self.search_by_uniqname(uniqname)
    if uniqname.blank?
      users = User.all
    else
      users = User.find_by_sql [ 'select * from users where uniqname like ? order by uniqname', "#{uniqname}%" ]
    end

    if users.blank?
      users = []
    end

    exact_match = User.find_by_uniqname uniqname
    if exact_match.blank?
      directory_user = UserDirectory.find_by_username(uniqname)
      unless directory_user.nil?
        Rails.logger.debug "directory_user: #{directory_user.inspect}"
        users = [directory_user] + users
        Rails.logger.debug "users: #{users.inspect}"
      end
    end

    return users
  end

  # Attempt to find the user by username and, if not found, consult the
  # UserDirectory, creating an application user if found.
  def self.find_or_create_from_directory(username)
    user = find_by_uniqname(username)
    user = create_from_directory(username) unless user.present?
    user || nil
  end

  # Perform a directory search for a username and attempt to create the user
  # if found. If successful, return the new User object, otherwise return false.
  def self.create_from_directory(username)
    user = UserDirectory.find_by_username(username)
    user.present? && user.save && user
  end

end
