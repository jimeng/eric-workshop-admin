class Location < ActiveRecord::Base
  has_many :workshops

  attr_accessible :active, :building, :calendar_url, :computer_type, :computers, :name, :seats, :support_email, :support_phone, :directions

  scope :active, -> {
    where active: true
  }

  scope :archived, -> {
    where active: false
  }

  liquid_methods(*(YAML::load_file(Rails.root.join('config', 'liquid_methods.yml'))['location'].keys))

end
