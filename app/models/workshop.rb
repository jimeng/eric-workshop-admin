class Workshop < ActiveRecord::Base
  include Audited

  # url's added for use in email templates but not persisted
  attr_accessor :scheduler_url, :instructor_url, :short_eval_url, :requested_times_list, :requested_times_count, :instructors_list, :instructors_count
  
  belongs_to :contact_user, :class_name => 'User'
  belongs_to :managed_by, :class_name => 'User'
  belongs_to :requested_by, :class_name => 'User'
  belongs_to :location, :class_name => 'Location'
  belongs_to :session_type
  has_many :instructor_assignments, :order => [:order]
  has_many :instructors, :through => :instructor_assignments, :source => :user, :order => { :instructor_assignments => :order }
  has_many :workshop_events
  has_many :attachments
  has_many :workshop_topics
  has_many :topics, :through => :workshop_topics
  has_one :statistics, :dependent => :destroy
  belongs_to :evaluation
  belongs_to :registration
  has_many :requested_times
  has_many :comments
  has_many :sub_choice_picks
  has_many :picks, :through => :sub_choice_picks, :source => :session_sub_choices
  has_many :notification_preferences
  has_many :updates

  attr_accessible :accepted, :archived, :class_related, :course, :expected_attendance, :scheduler_notes, :notes, :requester_questions, :section, :subject, :title
  attr_accessible :instruction_event_id, :location_event_id, :library_location_required, :location_other, :evaluation_setup, :registration_setup, :eval_results_url
  attr_accessible :scheduled_time, :requested_times, :ttcid, :deleted

  validates_presence_of :statistics, :unless => :new_record?
  before_create :ensure_statistics,  :if => :new_record?

  validates_associated :session_type, :location, :contact_user, :requested_by, :evaluation, :registration

  composed_of :scheduled_time, :class_name => "TimeRange", :mapping => [%w(scheduled_time_start_time start_time), %w(scheduled_time_end_time end_time)]

  liquid_methods(*(YAML::load_file(Rails.root.join('config', 'liquid_methods.yml'))['workshop'].keys))

  ##
  ## Convenience methods/aliass for clearer business logic around states
  ##
  ## Many of these correspond to a scope of the same name
  ##

  # There is at least one instructor
  def assigned?
    instructors.present?
  end

  # Room is chosen or declined
  def booked?
    location.present? || (library_location_required == false && location_other.present?)
  end

  # Time is chosen
  def scheduled?
    scheduled_time.present? && scheduled_time.valid?
  end

  def location_name(include_building = true)
    if location.nil? && location_other.blank?
      location_name = ''
    elsif location.nil?
      location_name = location_other
    else
      location_name = "#{location.name}"
      if include_building == true
        location_name = location_name +  " - #{location.building}"
      end
    end
    return location_name
  end

  # TODO: Decide if this is best as a boolean model method or as a helper that returns a status
  def has_statistics?
    self.statistics.try(:completed) || false
  end

  def full_eval_url(error_message_on_fail = true)
    Rails.logger.debug "Workshop.full_eval_url"
    unless instructors.nil? || instructors.empty?
      if scheduled_time_start_time.present?
        if evaluation.present? && evaluation.eval_template_url.present?
          if evaluation.eval_url_pattern.present?
            Rails.logger.debug "Workshop.full_eval_url -- using template"
            params = {}
            self.instructors_list = []
            instructors.each do |i|
              self.instructors_list << i
            end
            self.instructors_count = instructors_list.size
            params['workshop'] = self
            template = Liquid::Template.parse(evaluation.eval_url_pattern)
            url = template.render params
          else
             Rails.logger.debug "Workshop.full_eval_url --  NOT using template"
            if evaluation.eval_template_url.include? "?"
              url = evaluation.eval_template_url << '&sid=' << id.to_s 
            else
              url = evaluation.eval_template_url << '?sid=' << id.to_s 
            end
            instructor_display_names = nil
            instructors.first(2).each do |instructor|
              if instructor_display_names.nil?
                instructor_display_names = '&spresenters[]=' + instructor.display_name
              else
                instructor_display_names << '&spresenters[]=' + instructor.display_name
              end
            end
            url = url << '&stitle=' << title <<
                     instructor_display_names <<
                    '&starttime='<< scheduled_time_start_time.strftime('%Y-%m-%d %-I:%M %P')
          end
          Rails.logger.debug "url: #{url}"
          return url.strip.gsub(' ', '+')
        end
      elsif error_message_on_fail
        return 'No scheduled start time - URL cannot be created' 
      end
    end
    if error_message_on_fail
      return 'No instructor assigned yet - URL cannot be created'
    end
    return nil
  end


  ## Scopes and class methods for finding Workshops
  ## Note the mixing of regular ARel methods/symbols and Squeel blocks.
  ## Also note that the scopes are using arrow lambda syntax since they
  ## will be required in Rails 4 and the arrow is a bit cleaner.

  scope :not_deleted,   -> { where { deleted == false } }
  scope :active,        -> { where { ( archived == false ) & ( deleted == false ) } }
  scope :archived,      -> { where { ( archived == true )  & ( deleted == false ) } }
  scope :deleted,       -> { where deleted:  true }
  scope :alreadytaught, -> { active.where { scheduled_time_start_time <= Time.zone.now } }
  scope :taughtnotarchived, -> { active.alreadytaught }
  scope :recent,        -> { active.where { ( evaluation_setup == true ) &
                                     ( scheduled_time_start_time >= Time.zone.now.at_midnight - 10.days ) &
                                     ( scheduled_time_start_time <= Time.zone.now)  }
                            }

  scope :allsessions,    -> { where { ( evaluation_setup == true ) & ( deleted == false )  } }

  scope :allusersessions, ->(user) {
    allsessions.taught_by(user)
  }

  scope :recent_for,  ->(user) { recent.taught_by(user) }

  scope :booked,      -> {
    includes { location }.
    where { (location.id != nil) | ( (library_location_required == false) & (location_other != nil) ) }
  }
  scope :assigned,    -> {
    includes { instructors }.
    where { users.id != nil }
  }
  scope :scheduled,   -> { where { (scheduled_time_start_time != nil) & (scheduled_time_end_time != nil) } }

  scope :unbooked,    -> {
    active.includes { location }.
    where {  ((library_location_required == true) & (location.id == nil) ) | ( (library_location_required == false) & (location_other == nil) ) }
  }
  scope :unassigned,  -> {
    active.includes { instructor_assignments }.
    where { instructor_assignments.workshop_id == nil }
  }
  scope :unscheduled, -> { active.where { (scheduled_time_start_time == nil) | (scheduled_time_end_time == nil) } }

  scope :with_stats, -> {
    includes { statistics }.
    where { statistics.completed == true }
  }

  scope :without_stats, -> {
    # We shouldn't strictly need the nil check here, since we are now
    # enforcing that the statistics object exists, but it is a good model
    # for how to do this. Note that the parentheses are required because
    # of the precedence of == vs |.
    includes { statistics }.
    where { (statistics.id == nil) | (statistics.completed == false) }
  }

  scope :needs_stats, -> {
    active.alreadytaught.without_stats.includes { statistics }
  }

  scope :no_eval, -> { where evaluation_setup: false }

  scope :needs_eval, -> { active.no_eval }

  scope :requested, -> {
    active.unbooked.unscheduled.unassigned
  }

  scope :for_user, ->(user) {
    # NOTE that the ActiveRecord::Relation support for OR queries is not good, so
    # this is some required duplication to get the proper condition in place,
    # which would be taught_by(user) | requested_by(user), conceptually.
    eager_load(:instructor_assignments => :user).
    where { (users.id == user.id) | (contact_user_id == user.id) | (requested_by_id == user.id) }
  }

  scope :active_for, ->(user) {
    active.for_user(user)
  }

  def self.requested_by(user)
    where { (contact_user_id == user.id) | (requested_by_id == user.id) }
  end

  def self.archived_for(user)
    archived.for_user(user)
  end

  def self.taught_by(user)
    joins(:instructor_assignments => :user).
    where(:users => { :id => user.id })
  end

  def self.contact_is(user)
    where( archived: false , contact_user_id: user )
  end

  def add_units_for_instructor instructor
    if instructor.blank? || instructor.unit.blank?
      instructor_units = Set.new
    else
      instructor_units = Set.new instructor.unit.split('|')
    end
    # Rails.logger.debug "Workshop.add_units_for_instructor statistics.nil? #{statistics.nil?}"
    if statistics.nil?
      build_statistics
    end
    if statistics.blank? || statistics.unit.blank?
      workshop_units = Set.new
    else
      workshop_units = Set.new statistics.unit.split('|')
    end
    new_units = workshop_units + instructor_units
    unless workshop_units == new_units
      statistics.update_attribute :unit, new_units.to_a.join('|')
    end
  end

  def remove_units_for_instructor instructor
    if instructor.blank? || instructor.unit.blank?
      instructor_units = Set.new
    else
      instructor_units = Set.new instructor.unit.split('|')
    end
    if statistics.blank? || statistics.unit.blank?
      workshop_units = Set.new
    else
      workshop_units = Set.new statistics.unit.split('|')
    end
    if instructors.blank?
      other_units = Set.new
    else
      keep = []
      instructors.each do |i| 
        unless i.unit.blank?
          keep << i.unit.split('|')
        end
      end
      keep.flatten!
      other_units = Set.new keep
    end
    new_units = (workshop_units - instructor_units) + other_units
    unless workshop_units == new_units
      statistics.update_attribute :unit, new_units.to_a.join('|')
    end
  end

  private

    def ensure_statistics
      build_statistics if new_record? && statistics.nil?
    end

end
