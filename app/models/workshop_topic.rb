class WorkshopTopic < ActiveRecord::Base
  belongs_to :workshop
  belongs_to :topic

  attr_accessible :topic, :workshop
end
