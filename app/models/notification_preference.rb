class NotificationPreference < ActiveRecord::Base
  belongs_to :user
  belongs_to :workshop
  attr_accessible :event_type, :notifier, :scope, :active

  def self.rules_for_workshop(event_type, workshop)
    rules = {}
    unless event_type.blank? || workshop.nil?
      for_workshop(event_type, workshop).order(:notifier).each do |rule|
        list = rules[rule.notifier]
        if list.nil?
          list = []
          rules[rule.notifier] = list
        end
        list << rule
      end
    end
    rules
  end

  def self.rules_for_workshops(event_type, workshops = [])
    rules = {}
    unless event_type.blank? || workshops.blank?
      for_workshops(event_type, workshops).order(:notifier).each do |rule|
        list = rules[rule.notifier]
        if list.nil?
          list = []
          rules[rule.notifier] = list
        end
        list << rule
      end
    end
    rules
  end

  scope :for_workshop, ->(etype,ws) {
    where { 
      ( event_type == etype ) & 
      ( ( scope == 'all' ) | 
        ( user_id.in( ws.instructor_assignments.collect{ |ia| ia.user_id } ) ) | 
        ( user_id == ws.contact_user_id ) | 
        ( user_id == ws.managed_by_id ) | 
        ( user_id == ws.requested_by_id ) 
      ) 
    }
  }

  scope :for_workshops, ->(etype,workshops) {
    where { 
      ( event_type == etype ) & 
      ( ( scope == 'all' ) | 
        ( user_id.in( (
            workshops.collect{ |ws| ws.instructor_assignments.collect{ |ia| ia.user_id } } +
            workshops.collect{ |ws| ws.contact_user_id } +
            workshops.collect{ |ws| ws.managed_by_id } +
            workshops.collect{ |ws| ws.requested_by_id } ).flatten.reject{|i| i.nil? }.sort.uniq
          )
        )
      ) 
    }
  }

end
