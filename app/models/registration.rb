class Registration < ActiveRecord::Base
  attr_accessible :name, :setup_url, :active

  scope :active, -> {
    where active: true
  }

  scope :archived, -> {
    where active: false
  }

end
