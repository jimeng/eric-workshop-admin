class Update < ActiveRecord::Base
  belongs_to :user
  belongs_to :workshop
  belongs_to :perpetrator, :class_name => 'User'

  attr_accessible :event_type, :seen
end
