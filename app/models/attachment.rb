class Attachment < ActiveRecord::Base
  belongs_to :workshop
  attr_accessible :attached_file

  has_attached_file :attached_file,
    :path => "#{Rails.root}/attachments/:attachment/:id/:style/:filename",
    :url => "#{Rails.configuration.relative_url_root}/attachments/:attachment/:id/:style/:filename"

end
