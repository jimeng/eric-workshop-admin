class WorkshopEvent < ActiveRecord::Base
  belongs_to :workshop
  belongs_to :user

  attr_accessible :details, :name, :timestamp
  attr_accessor :items, :title
end
