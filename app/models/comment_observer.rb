class CommentObserver < ActiveRecord::Observer

  def after_create(record)
    Rails.logger.info("\n---------------------------------------\nCommentObserver.after_create #{record.as_json}\n---------------------------------------\n")
    # event = WorkshopEvent.new :details => "#{record.to_json}", :name => "workshop.create", :timestamp => record.created_at
    # event.workshop = record 
    # event.user = record.updated_by
    # event.save
  end

  def after_commit(record)
    Rails.logger.info("CommentObserver.after_commit #{record.as_json}")
  end

end