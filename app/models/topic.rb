class Topic < ActiveRecord::Base
  has_many :workshop_topics
  has_many :workshops, :through => :workshop_topics

  belongs_to :category, :class_name => 'TopicCategory'

  attr_accessible :active, :description, :title

  scope :active, -> {
    where active: true
  }

  scope :archived, -> {
    where active: false
  }

  liquid_methods(*(YAML::load_file(Rails.root.join('config', 'liquid_methods.yml'))['topic'].keys))

end
