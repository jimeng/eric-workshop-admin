module Audited
  extend ActiveSupport::Concern

  included do
    attr_accessor  :updated_by
    before_save    :set_updater
    before_destroy :set_updater
  end

  private
    def set_updater
      user = self.updated_by || RequestStore.store[:current_user]
      obj = "#{self.class}[#{self.id || 'new'}]"
      if user == false
        #Rails.logger.debug("##### Anonymous update to #{obj}")
      elsif user.nil?
        #Rails.logger.debug("##### System update to #{obj}")
      else
        #Rails.logger.debug("##### User update to #{obj} by: #{user.uniqname}")
        self.updated_by = user
      end
    end
end
