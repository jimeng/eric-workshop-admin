class Evaluation < ActiveRecord::Base
  attr_accessible :name, :setup_url, :active, :eval_template_url, :eval_url_pattern, :eval_info_url

  scope :active, -> {
    where active: true
  }

  scope :archived, -> {
    where active: false
  }

  scope :active_ordered,  order("evaluations.id").active

  liquid_methods(*(YAML::load_file(Rails.root.join('config', 'liquid_methods.yml'))['evaluation'].keys))

end
