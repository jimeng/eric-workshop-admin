class EmailTemplate < ActiveRecord::Base
  attr_accessible :active, :body, :name, :subject, :multiple, :system_stats

  scope :active, -> {
    where active: true
  }

  scope :archived, -> {
    where active: false
  }

  
end
