class InstructorAssignmentObserver < ActiveRecord::Observer

  def after_update(record)
    Rails.logger.info("InstructorAssignmentObserver.after_update #{record.inspect} \n#{record.changes} \n#{record.changed} ")
    event = WorkshopEvent.new :details => "#{record.to_json}", :name => "workshop.update-instructor", :timestamp => DateTime.now
    #workshop = Workshop.find()
    #event.workshop = record 
    event.user = record.updated_by
    event.workshop = Workshop.find(record.workshop_id)
    event.save
  end

  def after_create(record)
    Rails.logger.info("InstructorAssignmentObserver.after_create #{record.inspect} ")
    event = WorkshopEvent.new :details => "#{record.to_json}", :name => "workshop.add-instructor", :timestamp => DateTime.now
    #event.workshop = record 
    event.user = record.updated_by
    event.workshop = Workshop.find(record.workshop_id)
    event.save
  end

  def after_commit(record)
    Rails.logger.info("InstructorAssignmentObserver.after_commit #{record.inspect}")
  end

  def after_destroy(record)
    Rails.logger.info("InstructorAssignmentObserver.after_destroy \n#{record.inspect} \n#{record.changes}")
    event = WorkshopEvent.new :details => "#{record.to_json}", :name => "workshop.remove-instructor", :timestamp => DateTime.now
    event.user = record.updated_by
    event.workshop = Workshop.find(record.workshop_id)
    event.save
  end

end
