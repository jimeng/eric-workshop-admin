class Comment < ActiveRecord::Base
  belongs_to :workshop
  belongs_to :user
  has_many :email_recipients
  has_many :recipients, :through => :email_recipients, :source => :user  
  attr_accessible :content, :email_to_contact, :email_to_requester, :subject, :user_id, :workshop_id

  scope :recent_active,      -> {
    includes { workshop }.
    where { workshop.archived == false }
  }

end
