class Statistics < ActiveRecord::Base
  belongs_to :workshop
  attr_accessible :completed, :participants, :physical_items, :prep_time, :unit, :unit2, :notes

  validates_presence_of :workshop

  scope :completed,  -> { where completed: true  }
  scope :incomplete, -> { where completed: false }

  before_save :update_completion

  def self.for_instructor(user)
    joins(:workshop => { :instructor_assignments => :user }).
    where(:users => { :id => user.id })
  end


  private

    def update_completion
      # Decide whether to update automatically or let user mark completion.
      #completed = unit.present? && prep_time.present? && participants.present?
    end

end
