class RequestedTime < ActiveRecord::Base
  belongs_to :workshop
  attr_accessible :time_range
  
  composed_of :time_range, :class_name => "TimeRange", :mapping => [%w(time_range_start_time start_time), %w(time_range_end_time end_time)]

  # methods below here allow updates to underlying representation of workshop scheduling 
  # as TimeRange objects without changing controllers, helpers or views at this time.

  def start_time
    if time_range.nil?
      nil
    else
      time_range.start_time
    end
  end

  def end_time
    if time_range.nil?
      nil
    else
      time_range.end_time
    end
  end
  
  def duration_minutes
    if time_range.nil?
      nil
    else
      time_range.duration_minutes
    end
  end

  def day
    if time_range.nil?
      nil
    else
      time_range.day
    end
  end

  def time0
    if time_range.nil?
      nil
    else
      time_range.time0
    end
  end

  def time1
    if time_range.nil?
      nil
    else
      time_range.time1
    end
  end

  def start_time= val
    if time_range.nil?
      time_range = TimeRange.new
    end
    time_range.start_time = val
  end

  def end_time= val
    if time_range.nil?
      time_range = TimeRange.new
    end
    time_range.end_time = val
  end
  
  def duration_minutes= val
    if time_range.nil?
      time_range = TimeRange.new
    end
    time_range.duration_minutes = val
  end

  def day= val
    if time_range.nil?
      time_range = TimeRange.new
    end
    time_range.day = val
  end

  def time0= val
    if time_range.nil?
      time_range = TimeRange.new
    end
    time_range.time0 = val
  end

  def time1= val
    if time_range.nil?
      time_range = TimeRange.new
    end
    time_range.time1 = val
  end



  liquid_methods(*(YAML::load_file(Rails.root.join('config', 'liquid_methods.yml'))['requested_time'].keys))

end
