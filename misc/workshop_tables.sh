rails generate model workshop  title:string  day:date  start_time:time  end_time:time  contact_user:references location:references accepted:boolean archived:boolean managed_by:references course:string subject:string section:string session_type:references notes:text 

rails generate model user  firstname:string  lastname:string  uniqname:string role:string calendar_url:string active:boolean

rails generate model workshop_event workshop:references name:string timestamp:timestamp user:references details:text
rails generate model instructor workshop:references user:references order:integer
rails generate model topic title:string description:text active:boolean

rails generate model statistics workshop:references participants:integer prep_time:time unit:string physical_items:boolean completed:boolean

rails generate model attachment workshop:references attachment_url:string filename:string size:integer uuid:string file_path:string
rails generate model requested_time workshop:references day:date start_time:time end_time:time 

rails generate model location name:string building:string seats:integer computers:integer computer_type:string support_phone:string support_email:string calendar_url:string active:boolean

rails generate model session_type name:string description:text active:boolean

# rails generate model user  firstname:string  lastname:string  uniqname:string role:reference calendar_url:string active:boolean
# rails generate model requested_topic workshop:references topic:references
# rails generate model Roles rolename:string 
# rails generate model configuration name:string value:string description:text

# //------------------------------------
# rails generate model Workshop 
#     title:string
#     scheduled_day:date
#     scheduled_start:time
#     scheduled_end:time
#     contact_user:references
#     location:references
#     accepted:boolean
#     managed_by:references
#     course:string
#     subject:string
#     section:string
#     session_type:references
#     notes:text
# 
# contact_user is a ref to the Users table
# location is a ref to the Locations table
# managed_by is a ref to the Users table
# session_type is a ref to the Session_type table
# nothing is unique. 
#     
# rails generate model Users 
#     firstname:string
#     lastname:string
#     uniqname:string
#     role:reference
#     calendar_url:string 
#     active:boolean
#  
# role is a ref to the Roles table
# uniqname is unique
# calendar_url and active would only be used for users with a role of instructor or admin (not needed for requestors)
# 
# rails generate model Roles
#     rolename:string
#     
# we may want to add a column that designates level of power - so wouldn't have to hardcode role names but could rather
# check for a level of permission when checking what a user can do (e.g., might be two different roles that have admin power)
# rolename is unique
# 
# rails generate model WS_events
#     workshop:references
#     eventname:string
#     event_timestamp:timestamp
#     user:references
#     ref:text
# 
# stores events that we want to log about a workshop that happen as it is managed
# user is a ref to the Users table
# nothing is unique
# 
# rails generate model Instructors
#     workshop:references
#     user:references
# 
# holds those who are instructors for a particular workshop
# workshop is a ref to the Workshops table
# user is a ref to the Users table
# nothing is unique
# 
# rails generate model Topics
#     title:string
#     description:text
# 
# nothing is unique
# 
# rails generate model Requested_topics
#     workshop:references
#     topic:references
#     
# stores topics requested for a particular workshop - could be more than one topic for a given workshop
# workshop is a ref to the Workshop table
# topic is a ref to the Topics table
# nothing is unique
# 
# rails generate model Stats
#     workshop:references
#     num_participants:integer
#     prep_time:time
#     unit:string
#     physical_items:boolean
#     completed:boolean
# 
# holds stats for a given workshop
# workshop is a ref to the Workshop table
# workshop is unique
# 
# rails generate model Attachments
#     workshop:references
#     attachment_url:string
#     filename:string
#     size:int
#     
# holds a pointer to attachments to a workshop - could be many attachments
# workshop is a ref to the Workshop table
# nothing is unique
# 
# rails generate model Requested_dates
#     workshop:references
#     request_day:date
#     request_start_time:time
#     request_end_time:time
#     
# holds the request date(s) for a workshop- could be multiple per workshop
# workshop is a ref to the Workshop table
# nothing is unique
# 
# rails generate model Locations
#     location_name:string
#     location:string
#     seats:integer
#     computers:integer
#     computer_type:string
#     support_phone:string
#     support_email:string
#     calendar_url:string
#     active:boolean
#     
# holds the room locations that can be scheduled/picked from
# a workshop has a reference to this table for its location
# nothing is unique
# 
# rails generate model Session_type
#     session_name:string
#     description:text
#     
# holds descriptions of the available sessions
# a workshop has one type of session, the workshop references this table
# nothing is unique
# 
# rails generate model Config
#     name:string
#     value:string
#     description:text
# 
# holds various config parameters (e.g. email addr of admin group, master schedule url)
# name is unique
# 
# 
