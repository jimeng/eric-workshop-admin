Workshops::Application.routes.draw do

  get 'email_templates/docs'
  resources :email_templates

  get "reports/index"

  match "/recent" => "recent#index"
  match "/recent_sessions" => "recent_sessions#index"

  match '/contact',    to: 'static_pages#contact' , :via => :get
  match '/help',    to: 'static_pages#help'
  match '/about',   to: 'static_pages#about' , :via => :get
  match '/error',   to: 'static_pages#error'
  match '/workshops/:id/details' => 'workshops#details', :as => 'workshop_details'
  match '/workshops/course_select', :controller=>'workshops', :action => 'course_select', :via => [:get, :post]
  match '/workshops/section_select', :controller=>'workshops', :action => 'section_select', :via => [:get, :post]
  match "/attachments(/*path)", :to => 'attachments#file', :as => 'attachment'
  match "/w/:code", to: 'workshops#short', :as => 'short', :via => :get 


  # NOTE: This is not intended to stay -- just to show parallel structure and options
  match '/dash' => 'workshops#dash', :as => 'workshops_dash'

  # The priority is based upon order of creation:
  # first created -> highest priority.

  get 'workshops/session_types'
  get '/workshops/subjects_like'
  get '/workshops/courses_like'
  get '/workshops/sections_like'

  get 'calendars/find_rooms'
  get 'calendars/find_instructors'
  get 'calendars/instructor_calendar'
  get 'calendars/request_token'
  get 'calendars/room_calendar'
  get 'calendars/access'
  get 'calendars/grant_access'
  get 'calendars/revoke_access'

  #get 'attachments/file', :as => 'attachment'

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  resources :workshops do
    member do
      get 'copy'
      get 'instructors_like'
      get 'schedule'
      get 'stats'
      get 'statistics'
      get 'evaluation'
      post 'claim'
      post 'claim_show_view'
      post 'save_stats_tab'
      put 'choose_location'
      put 'library_location_required'
      put 'session_type'
      put 'toggle_manage'
      put 'toggle_archive'
      put 'contact_user'
      # TODO: eliminate most of the puts and posts above
      # TODO: and replace with calls to update_attribute
      put 'update_attribute'
    end
    resources :notification_preferences
    resources :comments
    resources :instructor_assignments, :only => [:create, :update, :destroy]
    resources :workshop_events, :only => [:index]
    resources :updates, :only => [:index]
  end
  get 'topics/xeditable_list'
  resources :topics
  resources :locations
  resources :units
  resources :reports
  resources :recent
  resources :session_sub_choices

  get 'topic_categories/choices'
  get 'topic_categories/reorder'
  put 'topic_categories/order'
  resources :topic_categories 

  get "users/uniqname_like"

  resources :sessions, only: [:create, :delete]
  get   'login'  => 'sessions#new', as: :login
  match 'logout' => 'sessions#destroy', as: :logout, via: [:get, :post]
  match 'logout_now' => 'sessions#logout_now', as: :logout_now, via: [:get, :post]

  get 'users/search_by_name'
  get 'users/search_form'
  resources :users do
    member do
      get 'options'
      put 'revise_options'
    end
    resources :comments
    resources :notification_preferences
    resources :updates, :only => [:index, :update]
  end
  resources :evaluations
  resources :registrations

  resources :comments, :only => [:index, :respond] 

  match "/oauth2callback" => "calendars#access"

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'
  root :to => 'workshops#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
