# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
Workshops::Application.initialize!

if Settings.action_mailer.try(:smtp_settings).present?
  Settings.action_mailer.smtp_settings.merge!({
    user_name: ENV['WORKSHOP_EMAIL_USERNAME'],
    password:  ENV['WORKSHOP_EMAIL_PASSWORD']
  }.reject{|k, v| v.nil?})

  # Not sure whether the OpenStruct / Options object has to be converted to Hash or not, but be safe for now
  Rails.configuration.action_mailer.smtp_settings = Settings.action_mailer.smtp_settings.to_hash
end


if Settings.mailman.try(:config).try(:pop3).present?
  Settings.mailman.config.pop3.merge!({
    username: ENV['WORKSHOP_EMAIL_USERNAME'],
    password: ENV['WORKSHOP_EMAIL_PASSWORD']
  }.reject{|k, v| v.nil?})
end

