module Registrar

  class << self

    def sections(year, term, subject, catalog_nbr)
      request :sections, year, term, subject, catalog_nbr
    end

    def courses(year, term, subject)
      request :courses, year, term, subject
    end

    def subjects(year, term)
      request :subjects, year, term
    end

    private

    def client
      @@client ||= CachingRegistrar.new((Rails.configuration.umiac_enabled ? UMIACRegistrar.new : MockRegistrar.new))
    end

    def request(method, *args)
      begin
        return client.send method, *args
      rescue Exception => e
        if Rails.configuration.registrar_quiet
          Rails.logger.info "Squelching registrar request error for #{method} (#{args.join(', ')}): #{e.inspect} #{e.backtrace.join("\n")}"
          return []
        else
          raise e
        end
      end
    end
  end

  class CachingRegistrar
    def initialize(proxy)
      @proxy = proxy
      @time_to_live = Rails.configuration.registrar_cache_ttl || 300
      @requests = @hits = @misses = @total_time = 0
    end

    def sections(year, term, subject, catalog_nbr)
      cached_request :sections, year, term, subject, catalog_nbr
    end

    def courses(year, term, subject)
      cached_request :courses, year, term, subject
    end

    def subjects(year, term)
      cached_request :subjects, year, term
    end

    private

      def cached_request(method, *args)
        @requests += 1
        key = "#{method}:#{args.join(',')}"
        val = Rails.cache.read key
        if val.nil?
          @misses += 1
          Rails.logger.info "[CachingRegistrar] Cache miss for '#{key}'"
          before = Time.zone.now
          val = @proxy.send method, *args
          elapsed = Time.zone.now - before
          @total_time += elapsed
          Rails.cache.write key, val, expires_in: @time_to_live.seconds
          Rails.logger.info "[CachingRegistrar] Cache write '#{key}' for #{@time_to_live} seconds as: '#{val.to_s.truncate(40)}'"
        else
          @hits += 1
          Rails.logger.info "[CachingRegistrar] Cache hit for '#{key}': '#{val.to_s.truncate(40)}'"
        end

        if (@requests % 10 == 0)
          Rails.logger.info "[#{I18n.l Time.zone.now, format: :short_datetime} - CachingRegistrar] Requests: #{@requests}, Hits: #{@hits}, Misses: #{@misses}"
          average = 0
          average = (@total_time * 1000 / @misses).to_i unless @misses == 0
          Rails.logger.info "[#{I18n.l Time.zone.now, format: :short_datetime} - CachingRegistrar] Total registrar time: #{@total_time} seconds, average: #{average} ms"
        end

        val
      end

  end

  class UMIACRegistrar

    def initialize
      require 'umiac'
      config   = Rails.configuration
      @url     = config.umiac_url
      @enabled = config.umiac_enabled
      @timeout = config.umiac_timeout
    end

    def sections(year, term, subject, catalog_nbr)
      umiac.request('getCourses', ["term_year=#{year}", "term_id=#{term}", "subject=#{subject}", "catalog_nbr=#{catalog_nbr}"])
    end

    def courses(year, term, subject)
      umiac.request('getCourses', ["term_year=#{year}", "term_id=#{term}", "subject=#{subject}"])
    end

    def subjects(year, term)
      umiac.request('getCourses', ["term_year=#{year}", "term_id=#{term}"]).select do |rec|
        rec.length == 2 &&            # not a single entry
        rec[0].to_i.to_s != rec[0] && # not an integer key
        rec[0] != '0'
      end
    end

    def umiac
      Rails.logger.debug "Initializing UMIAC connection to: #{@url}"
      @umiac ||= UMIAC::Client.new(url: @url)
    end

  end

  class MockRegistrar
    def courses(year, term, subject)
      [
        [subject, "200", "Intro to XYZ"],
        [subject, "223", "Intro to ABC"],
        [subject, "323", "Advanced ABC"]
      ]
    end

    def subjects(year, term)
      [
        ["ASTR", "Astrology"],
        ["ENGL", "English"],
        ["PIZZA", "Fine Dining"]
      ]
    end

    def sections(year, term, subject, catalog_nbr)
      [
        ["ENGLISH","125","056","25054","Writing&Academic Inq","REC","https://ctools.umich.edu/portal/site/d9637df9-72c1-4ff9-8873-67a2a1550dd5"],
        ["ENGLISH","125","058","27974","Writing&Academic Inq","REC","https://ctools.umich.edu/portal/site/5e138e4a-2bdd-4906-9181-e1dc1e33e20a"],
        ["ENGLISH","125","060","27976","Writing&Academic Inq","REC","https://ctools.umich.edu/portal/site/9dd57358-0c0a-4117-9f55-d66d6e400ee6"]
      ]
    end
  end

  private 

    def self.load_config!
      defaults = {
        'url'          => nil,
        'enabled'      => true,
        'timeout'      => 2000,
        'time_to_live' => 5,
        'quiet'        => false
      }

      begin
        loaded = YAML::load_file(Rails.root.join('config', 'umiac.yml'))
      rescue Errno::ENOENT => e
        Rails.logger.error "UMIAC configuration file not found (config/umiac.yml)!"
      end

      begin
        local = YAML::load_file(Rails.root.join('config', 'umiac.local.yml'))
      rescue Errno::ENOENT => e
        Rails.logger.debug "UMIAC local overrides file not found (config/umiac.local.yml)."
      end

      loaded = defaults.merge loaded[Rails.env] if loaded && loaded[Rails.env]
      loaded = loaded.merge local[Rails.env] if local && local[Rails.env]

      config = Rails.configuration
      config.umiac_url           = ENV['UMIAC_URL']                 || loaded['url']
      config.umiac_enabled       = env_bool(ENV['UMIAC_ENABLED'])   || loaded['enabled']
      config.umiac_timeout       = ENV['UMIAC_TIMEOUT']             || loaded['timeout']
      config.registrar_cache_ttl = ENV['REGISTRAR_CACHE_TTL']       || loaded['cache_ttl']
      config.registrar_quiet     = env_bool(ENV['REGISTRAR_QUIET']) || loaded['quiet']

      if config.umiac_enabled && config.umiac_url.blank?
        raise "UMIAC configuration present and enabled for '#{Rails.env}' environment, but no URL is set. Please see config/umiac.yml."
      end
    end

    def self.env_bool(val)
      val == true || val =~ /(true|1)$/i ? true : false
    end

  load_config!

end

