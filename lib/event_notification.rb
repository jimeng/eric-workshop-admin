class EventNotification

  # require 'action_dispatch/routing/url_for'
  include Rails.application.routes.url_helpers

  def initialize
    @base_url = Rails.configuration.event_notifications[:base_url]
    @protocol = Rails.configuration.event_notifications[:protocol]
  end

  def notify event_type, workshops, perpetrator
    notification_rules = NotificationPreference.rules_for_workshops event_type, workshops
    unless notification_rules.blank?
      notification_rules.each_pair do |notifier, rules|
        case notifier
        when 'email'
          Rails.logger.debug "EventNotification.notify -- email\n#{rules.as_json}"
          send_event_emails(event_type, workshops, rules)
        when 'sms'
          Rails.logger.debug "EventNotification.notify -- sms\n#{rules.as_json}"
        when 'ping'
          Rails.logger.debug "EventNotification.notify -- ping\n#{rules.as_json}"
          post_pings(event_type, workshops, rules, perpetrator)
        else
          Rails.logger.warn "EventNotification.notify -- ERROR -- unknown notifier: #{notifier}\n#{rules.as_json}"
        end
      end
    end
  end

  def post_pings(event_type, requests, rules, perpetrator)
    Rails.logger.debug("EventNotification.post_pings \n#{requests.as_json} \n#{rules.length}")

    unless event_type.blank? || requests.blank? || rules.blank?
      rules.each do |rule|
        requests.each do |request|
          u = Update.new(:event_type => event_type)
          u.user = rule.user
          u.perpetrator = perpetrator
          u.workshop = request
          u.save
        end
      end
    end    
  end

  def send_event_emails event_type, requests, rules
    Rails.logger.debug("EventNotification.send_event_emails \n#{requests.as_json} \n#{rules.length}")

    unless event_type.blank? || requests.blank? || rules.blank?
      recipients = rules.collect{ |rule| rule.user }
      template = EmailTemplate.find_by_name_and_active(event_type, true)
      if template.blank?
        Rails.logger.warn "EventNotification.send_event_emails -- ERROR -- Emails cannot be sent.  Template \"#{event_type}\" not found.\n\t#{recipients}"
      else
        params = {}
        if template.multiple?
          params['workshops'] = requests.collect{ |ws| liquify(ws) }
        else
          params['workshop'] = liquify(requests[0])
        end
        if template.system_stats?
          params['new_request_count'] = Workshop.requested.count
          params['no_time_count'] = Workshop.unscheduled.count
          params['no_room_count'] = Workshop.unbooked.count
          params['no_instructor_count'] = Workshop.unassigned.count
          params['need_stats_count'] = Workshop.needs_stats.count
        end
        body_template = Liquid::Template.parse(template.body)
        body = body_template.render params
        subj_template = Liquid::Template.parse(template.subject)
        subject = subj_template.render params

        Notifier.new_request_email(recipients, subject, body).deliver

      end

    end
  end

  def self.list_scopes
    scopes = []
    scopes << { :id => 'all', :name => 'All requests' }
    scopes << { :id => 'mine', :name => 'My requests' }
    scopes
  end

  def self.map_scopes
    list = list_scopes
    map = {}
    list.each do |item|
      map[item[:id]] = item[:name]
    end
    map
  end


  def self.list_notifiers
    notifiers = []
    notifiers << { :id => 'email', :name => 'Send email' }
    # notifiers << { :id => 'sms', :name => 'Send SMS text' }
    notifiers << { :id => 'ping', :name => 'Ping me in app' }
    notifiers
  end

  def self.map_notifiers
    list = list_notifiers
    map = {}
    list.each do |item|
      map[item[:id]] = item[:name]
    end
    map
  end

  def self.list_event_types
    event_types = []
    event_types << { :id => "workshop.create", :name => "When a new session is requested" }
    event_types << { :id => "workshop.instructor.add", :name => "When an instructor is added" }
    event_types << { :id => "workshop.instructor.remove", :name => "When an instructor is removed" }
    event_types << { :id => "workshop.room.add", :name => "When a room is assigned" }
    event_types << { :id => "workshop.room.remove", :name => "When a room is removed" }
    event_types << { :id => "workshop.update", :name => "When details for a request are changed" }
    event_types << { :id => "email.received", :name => "When an email is received" }
    event_types << { :id => "email.sent", :name => "When an email is sent" }
    event_types
  end

  def self.map_event_types
    list = list_event_types
    map = {}
    list.each do |item|
      map[item[:id]] = item[:name]
    end
    map
  end

  private

  def liquify(workshop)
    workshop.instructor_url = url_for(controller: 'workshops', action: 'show', id: workshop.id, host: @base_url, protocol: @protocol)  # workshop_url(workshop)
    workshop.short_eval_url = url_for(controller: 'workshops', action: 'short', code: workshop.id.to_s(36), host: @base_url, protocol: @protocol)  # short_url(workshop.id.to_s(36))
    workshop.scheduler_url = url_for(controller: 'workshops', action: 'schedule', id: workshop.id, host: @base_url, protocol: @protocol) # schedule_workshop_url(workshop)
    workshop.requested_times_list = []
    workshop.requested_times.each do |rt|
      workshop.requested_times_list << rt
    end
    workshop.requested_times_count = workshop.requested_times_list.size
    workshop.instructors_list = []
    workshop.instructors.each do |i|
      workshop.instructors_list << i
    end
    workshop.instructors_count = workshop.instructors_list.size
    workshop
  end

end
