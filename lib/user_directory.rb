module UserDirectory

  class << self

    def find_by_username(username)
      adapter.find_by_username(username)
    end

    def search_by_name(name)
      adapter.search_by_name(name)
    end

    private

    def adapter
      @@adapter ||= (Rails.configuration.ldap_enabled ? LDAPAdapter.new : MockAdapter.new)
    end
  end

  class LDAPAdapter
    require 'net/ldap'

    def initialize
      config = Rails.configuration.ldap_config || {}
      @ldap_host = config[:ldap_host]
      @ldap_port = config[:ldap_port]
      @base_dn   = config[:base_dn]

      if @ldap_host.nil? || @ldap_host.empty?
        raise_config_error("Host", :ldap_host)
      end

      if @ldap_host.nil? || @ldap_host.empty?
        raise_config_error("Port", :ldap_port)
      end

      if @ldap_host.nil? || @ldap_host.empty?
        raise_config_error("Base DN", :base_dn)
      end

      @requests = 0
      @hits = 0
      @misses = 0
      @total_time = 0
    end


    # TODO: Define a return structure for directory users and let the model
    #       decide whether or not it should create database records.
    def find_by_username(username)
      # check ldap
      user = nil
      ldap = Net::LDAP.new :host => @ldap_host, :port => @ldap_port
      filter = Net::LDAP::Filter.eq 'uid', username
      Rails.logger.info "UserDirectory.find_user --- trying LDAP search for #{username}"
      @requests += 1
      before = Time.zone.now
      after = nil
      ldap.search(:base => @base_dn, :filter => filter) do |entry|
        after = Time.zone.now
        # entry.each do |attribute, values|
        #   Rails.logger.debug "   #{attribute}:"
        #   values.each do |value|
        #     Rails.logger.debug "      --->#{value}"
        #   end
        # end
        unless entry.givenname.nil? || entry.givenname.empty? || entry.sn.nil? || entry.sn.empty?
          @hits += 1
          Rails.logger.debug "DN: #{entry.dn}"
          Rails.logger.debug "SN: #{entry.sn[0]}"
          Rails.logger.debug "givenname: #{entry.givenname[0]}"

          # We use new here because we may do lookups for users that we don't
          # care to persist. If the calling code wants to save, it has the
          # responsibility to check for an existing record and decide whether
          # to update the fields from the directory.
          user = User.new( {:uniqname => username, :firstname => "#{entry.givenname[0]}", :lastname => "#{entry.sn[0]}", :role => 'requester'} )
          Rails.logger.debug "user: #{user.inspect}"

          # Searches on uid return at most one record, but be sure
          break
        else
          @misses += 1
        end
      end
      after = Time.zone.now if after.nil?
      elapsed = after - before
      @total_time += elapsed

      if @requests % 10 == 0
        Rails.logger.info "[#{I18n.l Time.zone.now, format: :short_datetime} - UserDirectory] Requests: #{@requests}, Hits: #{@hits}, Misses: #{@misses}"
        average = 0
        average = (@total_time / @requests) unless @requests == 0
        Rails.logger.info "[#{I18n.l Time.zone.now, format: :short_datetime} - UserDirectory] Total Directory time: #{@total_time} seconds, average: #{average}"
      end
      
      return user
    end

    def search_by_name(name, max = 36)
      Rails.logger.debug "UserDirectory.search_by_name #{name}"
      errors = []
      users = []
      ldap = Net::LDAP.new :host => @ldap_host, :port => @ldap_port
      filter = Net::LDAP::Filter.contains(:cn, name)
      # Net::LDAP::Filter.equals(:ou, 'People') & Net::LDAP::Filter.equals(:dc, 'umich') & Net::LDAP::Filter.equals(:dc, 'edu') & 
      # ou=People,dc=umich,dc=edu
      count = 0
      ldap.search(:base => "ou=People,dc=umich,dc=edu", :filter => filter, :size => max * 2) do |entry|
        # Rails.logger.debug "UserDirectory.search_by_name entry: #{entry.as_json}\n#{entry.attribute_names.as_json}\n----------------------------"
        begin
          if entry[:givenname].blank? && entry[:sn].blank? && entry[:displayname].blank?
            # punt
            # Rail.logger.debug "  0"
          elsif entry[:givenname].blank? && entry[:sn].blank?
            display_name = "#{entry.displayname[0]}".strip
            if display_name.include? ' '
              index = display_name.rindex(/\s/)
              firstname = display_name.slice(0, index).strip
              lastname = display_name.slice(-index, -1).strip
              # Rails.logger.debug "  1 ==> #{firstname} -- #{lastname} -- #{display_name}"
            else
              # punt
              # Rails.logger.debug "  2 ==> #{display_name}"
            end
          elsif entry[:givenname].blank? && entry[:displayname].blank?
            # punt
            # Rails.logger.debug "  3 => #{entry.sn[0]}"
          elsif entry[:sn].blank? && entry[:displayname].blank?
            # punt
            # Rails.logger.debug "  4 ==> #{entry.givenname[0]}"
          elsif entry[:givenname].blank?
            display_name = "#{entry.displayname[0]}".strip
            lastname = "#{entry.sn[0]}".strip
            if display_name.include? lastname
              firstname = display_name.slice(0, display_name.rindex(" #{lastname}"))
              # Rails.logger.debug "  5 ==> #{firstname} -- #{lastname} -- #{display_name}"
            else
              # punt
              # Rails.logger.debug "  6 ==> #{lastname} -- #{display_name}"
            end
          elsif entry[:sn].blank?
            display_name = "#{entry.displayname[0]}".strip
            firstname = "#{entry.givenname[0]}".strip
            if display_name.include? firstname
              index = display_name.index(firstname) + firstname.length
              if(index >= display_name.length)
                # punt
                # Rails.logger.debug "  7 ==> #{firstname} -- #{display_name}"
              else
                lastname = display_name.slice(-index, -1)
                # Rails.logger.debug "  8 ==> #{firstname} -- #{lastname} -- #{display_name}"
              end
            else
              # punt
              # Rails.logger.debug "  9 ==> #{firstname} -- #{display_name}"
            end
          else
            firstname = "#{entry.givenname[0]}"
            lastname = "#{entry.sn[0]}"
            display_name = "#{firstname} #{lastname}"
            # Rails.logger.debug " 10 ==> #{firstname} -- #{lastname} -- #{display_name}"
          end

          uniqname = "#{entry.uid[0]}"

          unless firstname.blank? || lastname.blank? || display_name.blank?
            user = User.new( {:uniqname => "#{uniqname}", :firstname => "#{firstname}", :lastname => "#{lastname}", :role => 'requester' } )
            count = count + 1
            other_names = [] 
            unless entry[:cn].blank?
              entry.cn.each do |cname|
                cname = "#{cname}".strip
                unless display_name == cname
                  other_names << cname
                end
              end
            end
            user.other_names = other_names

            titles = []
            unless entry[:umichtitle].blank?
              entry.umichtitle.each do |title|
                titles << "#{title}".strip
              end
            end

            affiliations = []
            unless entry[:ou].blank?
              entry.ou.each do |affiliation|
                affiliations << "#{affiliation}".strip
              end
            end
            
            if titles.blank? && affiliations.blank?
              user.titles = titles
              user.affiliations = affiliations
            elsif titles.blank?
              user.titles = affiliations.slice(0,1)
              if affiliations.length == 1
                user.affiliations = []
              else
                user.affiliations = affiliations
              end
            else
              user.titles = titles
              user.affiliations = affiliations
            end

            # Rails.logger.debug "UserDirectory.search_by_name user: #{user.inspect}"

            users << user
          end
        rescue => e1
          Rails.logger.warn "UserDirectory.search_by_name ERROR processing entry #{e1}\n----------------------------\n#{entry.as_json}\n----------------------------"
        end
      end
      # Rails.logger.debug "UserDirectory.search_by_name #{users.length}"
      if count > max
        errors << "Too many people found. Please supply more information."
      elsif count < 1
        errors << "No users found.  Please try again."
      end
      { :people => users, :errors => errors }
    end

    private
    
    def raise_config_error(name, param)
      raise Exception.new("LDAP #{name} not set. Set it in configuration (ldap_config[:#{param}]) or environment. The environment will take precedence. See config/application.rb.")
    end

  end

  class MockAdapter
    def find_by_username(username)
      nil
    end

    def search_by_name(name)
      nil
    end
  end

end

