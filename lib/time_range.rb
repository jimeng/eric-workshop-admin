#
# A class representing a period of time on a particular day. 
# The time range is bounded by two datetime objects representing 
# start_time and end_time.  But also it can be thought of as 
# a Date object representing the day containing both start_time 
# and end_time, along with two Time objects representing the
# times during that day at which start_time and end_time
# occur.  We'll call those two times time0 and time1.  
#
# Setting time0 or time1 should affect only the hours, minutes,
# seconds, or milliseconds of start_time or end_time.  Setting 
# day should affect only the year, month and day portion of 
# start_time and end_time, not hours, minutes, seconds, or 
# milliseconds. On the other hand, setting start_time or end_time 
# should affect both the day and either time0 or time1. 
# 
# One other representation is also useful.  This one is defined 
# as the number of minutes (or duration_minutes) between the
# start_time and the end_time. If start_time has been set, end_time
# can be set by assigning the duration_minutes.   
# 
# The getters for each of the five fields (day, time0, time1, 
# start_time and end_time) returns a data-time object which 
# produce string representations using methods such as strftime,
# provided only the valid parts are output in the case of 
# day, time0 and time1.
# 
# TimeRange ...
#
# TODO: validation?
# TODO: tests
#
require 'date'

class TimeRange

  TIME_FORMAT = "%-I:%2M %p"
  TIME_DATA_FORMAT = "%I:%2M %p"
  DATE_FORMAT = "%A, %B %-d, %Y"
  SHORT_DAY_FORMAT = "%a %b %-d, %Y" # Wed Jan 1, 2013
  MEDIUM_DAY_FORMAT = "%a %B %-d, %Y" # Wed January 1, 2013
  DATE_DATA_FORMAT = "%Y-%m-%d"
  SHORT_DATE_FORMAT = "%m/%d/%Y"
  DATETIME_DATA_FORMAT = "#{DATE_DATA_FORMAT} #{TIME_DATA_FORMAT}"

  def self.from_parts(day, time0, time1)
    Rails.logger.debug "TimeRange.from_parts #{day} #{time0} #{time1}"

    start_time = Time.zone.parse "#{day} #{time0}"
    end_time = Time.zone.parse "#{day} #{time1}"
    # Rails.logger.debug "TimeRange.from_parts #{start_time}"
    # Rails.logger.debug "TimeRange.from_parts #{end_time}"
    return new(start_time, end_time)
  end

  def initialize(stime, etime)
    #Rails.logger.debug "TimeRange.initialize #{stime}, #{etime}"
    unless stime.nil?
      @start_time = Time.zone.parse stime.strftime(DATETIME_DATA_FORMAT)
    end
    unless etime.nil?
      @end_time = Time.zone.parse etime.strftime(DATETIME_DATA_FORMAT)
    end
  end

  def start_time=(start_time)
    Rails.logger.debug "TimeRange.start_time = #{start_time}"
    unless start_time.nil?
      @start_time = start_time
    end
  end

  def end_time=(end_time)
    Rails.logger.debug "TimeRange.end_time = #{end_time}"
    unless end_time.nil?
      @end_time = end_time
    end
  end

  def day=(day)
    unless @start_time.present?
      @start_time = Time.zone.now
    end
    unless @end_time.present?
      @end_time = @start_time
    end
    @start_time = Time.zone.parse "#{day.strftime(DATE_DATA_FORMAT)} #{@start_time.strftime(TIME_DATA_FORMAT)}"
    @end_time = Time.zone.parse "#{day.strftime(DATE_DATA_FORMAT)} #{@end_time.strftime(TIME_DATA_FORMAT)}"
  end

  def time0=(t0)
    unless @start_time.present?
      @start_time = Time.zone.now
    end
    @start_time = Time.zone.parse "#{@start_time.strftime(DATE_DATA_FORMAT)} #{t0.strftime(TIME_DATA_FORMAT)}"
  end

  def time1=(t1)
    unless @end_time.present?
      @end_time = Time.zone.now
    end
    @end_time = Time.zone.parse "#{@end_time.strftime(DATE_DATA_FORMAT)} #{t1.strftime(TIME_DATA_FORMAT)}"
  end

  def start_time
    if @start_time.nil?
      return nil
    end
    @start_time
  end

  def end_time
    if @end_time.nil?
      return nil
    end
    @end_time
  end

  def day
    start_time
  end

  def time0
    start_time
  end

  def time1
    end_time
  end

  def duration_minutes
    unless @start_time.present?
      @start_time = Time.zone.now
    end
    unless @end_time.present?
      @end_time = @start_time
    end
    seconds = @end_time - @start_time
    (seconds / 60.0).round(0) 
  end

  def duration_minutes=(min)
    unless @start_time.present?
      @start_time = Time.zone.now
    end
    @end_time = @start_time.advance(:minutes => min.to_i)
  end

  def display_day
    "#{day.strftime(DATE_FORMAT)}"
  end

  def display_short_day
    "#{day.strftime(SHORT_DATE_FORMAT)}"
  end

  def display_compact_day
    "#{day.strftime(SHORT_DAY_FORMAT)}"
  end

  def display_time0
    "#{time0.strftime(TIME_FORMAT)}"
  end

  def display_time1
    "#{time1.strftime(TIME_FORMAT)}"
  end

  def data_day
    "#{day.strftime(DATE_DATA_FORMAT)}"
  end

  def data_time0
    "#{time0.strftime(TIME_DATA_FORMAT)}"
  end

  def data_time1
    "#{time1.strftime(TIME_DATA_FORMAT)}"
  end

  def data_start_time
    "#{start_time.strftime(DATETIME_DATA_FORMAT)}"
  end

  def data_end_time
    "#{end_time.strftime(DATETIME_DATA_FORMAT)}"
  end

  def display_start_time
    start_time.strftime("#{SHORT_DATE_FORMAT} #{TIME_FORMAT}")
  end

  def display_time_range
    "#{display_day}&nbsp;&nbsp; #{display_time0} - #{display_time1}".html_safe
  end

  def display_short_dm
    "#{display_compact_day}&nbsp;&nbsp; <small> #{display_time0} - #{display_time1}</small>".html_safe
  end

  def display_short_d_long_m
    "#{"#{day.strftime(MEDIUM_DAY_FORMAT)}"}&nbsp;&nbsp; <small> #{display_time0} - #{display_time1}</small>".html_safe
  end

  def valid?
    @start_time.present? && @end_time.present? && (@start_time.beginning_of_day == @end_time.beginning_of_day) && (@end_time - @start_time > 0)
  end

  def marshal_dump
    [@start_time, @end_time]
  end

  def marshal_load(variables)
    initialize(variables[0], variables[1])
  end

  def to_hash
    hash = {}
    unless start_time.nil? || end_time.nil?
      hash[:start_time] = start_time.try('as_json')
      hash[:end_time] = end_time.try('as_json')
      hash[:day] = data_day
      hash[:time0] = data_time0
      hash[:time1] = data_time1
      hash[:display_time_range] = display_time_range
      hash[:display_short_dm] = display_short_dm
      hash[:display_short_day] = display_short_day
      hash[:display_short_d_long_m] = display_short_d_long_m
      hash[:display_day] = display_day
      hash[:duration_minutes] = duration_minutes
    end
    hash
  end

  def to_s
    marshal_dump.to_s
  end

  def as_json()
    to_hash.as_json
  end

  def serializable_hash(options = nil)
    to_hash
  end

  def self.default_data_day
    Time.zone.now.beginning_of_hour.advance(:weeks => 2).strftime(DATE_DATA_FORMAT)
  end

  def self.default_data_time0
    Time.zone.now.beginning_of_hour.strftime(TIME_DATA_FORMAT)
  end
  
  def self.default_data_time1
    Time.zone.now.beginning_of_hour.advance(:hours => 1).strftime(TIME_DATA_FORMAT)
  end

  liquid_methods(*(YAML::load_file(Rails.root.join('config', 'liquid_methods.yml'))['time_range'].keys))


end

