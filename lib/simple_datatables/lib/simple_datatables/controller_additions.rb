module SimpleDataTables
  extend ActiveSupport::Concern
  
  module Logger
    def self.debug(msg)
      Rails.logger.debug("SimpleDataTables: #{msg}")
    end
  end

  included do
    class_attribute :datatables_model_class
    class_attribute :datatables_whitelist
    class_attribute :datatables_default_filter
    class_attribute :datatables_direct_mappings
    class_attribute :datatables_alias_mappings
    class_attribute :datatables_wrapper_key
    extend Convenience

    begin
      self.datatables_model_class = self.controller_name.classify.constantize
    rescue NameError
      Logger.debug "#{self} does not correspond to a model class. It must be associated with a model for SimpleDataTables to be used."
    end
    self.datatables_whitelist = []
    self.datatables_default_filter = :scoped
    self.datatables_direct_mappings = []
    self.datatables_alias_mappings = {}
    self.datatables_wrapper_key = Rails.configuration.respond_to?('datatables_wrapper_key') && Rails.configuration.datatables_wrapper_key || nil
  end

  module Convenience
    def accept_data_filter(*filters)
      filters.map! { |f| f.to_sym }
      Logger.debug "Accepting data filters for model '#{self.datatables_model_class}': #{filters.join(', ')}"
      self.datatables_whitelist += filters
    end

    def reject_data_filter(*filters)
      filters.map! { |f| f.to_sym }
      Logger.debug "Rejecting data filters for model '#{self.datatables_model_class}': #{filters.join(', ')}"
      filters.each do |f|
        self.datatables_whitelist.delete f
      end
    end

    def default_data_filter(filter)
      Logger.debug "Setting default data filter for model '#{self.datatables_model_class}': #{filter}"
      self.datatables_default_filter = filter
    end

    # Map an output field to a method of the same name from the model, helper, or controller, in that order.
    # Mappings should be supplied as symbols and can be given as multiple parameters or an array.
    def direct_data_mapping(*names)
      names.map! { |f| f.to_sym }
      Logger.debug "Mapping attributes/methods directly for model '#{self.datatables_model_class}': #{names.join(', ')}"
      self.datatables_direct_mappings += names
    end

    # Map an output field to a method of the specified name from the model, helper, or controller, in that order.
    # Mappings should be supplied as symbols and must be supplied as two parameters per call to establish an alias.
    def alias_data_mapping(dest, src)
      dest = dest.to_sym
      src = src.to_sym
      Logger.debug "Aliasing attributes/methods for model '#{self.datatables_model_class}': #{dest} will take value from #{src}"
      self.datatables_alias_mappings[dest] = src
    end

    def data_wrapper_key(key)
      Logger.debug "Mapping attributes/methods directly for model '#{self.datatables_model_class}': #{key}"
      self.datatables_wrapper_key = key && key.to_s
    end

    def datatables_model(klass)
      Logger.debug "Setting DataTables model class for #{self} to: #{klass}"
      success = true
      if klass.is_a? Class
        self.datatables_model_class = klass
      elsif klass.is_a?(String) || klass.is_a?(Symbol)
        begin
          self.datatables_model_class = klass.to_s.classify.constantize
        rescue NameError
          success = false
        end
      end

      unless success
        raise "Cannot set DataTables model class for #{self} to: #{klass}"
      end
    end
  end

  def data_filter_whitelist
    self.datatables_whitelist
  end

  def default_data_filter
    self.datatables_default_filter
  end

  def datatables_model
    self.datatables_model_class
  end

  def data_filter
    @data_filter = params[:filter].to_s.to_sym
    @data_filter = default_data_filter unless data_filter_whitelist.include? @data_filter
    return @data_filter
  end

  def filtered_scope
    if datatables_model.nil? || !datatables_model.is_a?(Class)
      raise "#{self.class} is not mapped to a model class (#{datatables_model.class}). Configure by calling datatables_model."
    end
    datatables_model.send data_filter
  end

  def direct_data_mappings
    self.datatables_direct_mappings
  end

  def alias_data_mappings
    self.datatables_alias_mappings
  end

  def mapped_data
    mappings = direct_data_mappings.inject({}) do |hash, field|
      hash.merge!({ field => field })
    end.merge(alias_data_mappings)

    build_mappings(filtered_scope, mappings)
  end

  def mapped_json(wrapper = nil)
    if wrapper.present?
      { wrapper => mapped_data }
    elsif self.datatables_wrapper_key.present?
      { self.datatables_wrapper_key => mapped_data }
    else
      mapped_data
    end
  end

  private
    def model_lambda(src)
      ->(rec) { rec.send(src) }
    end

    def view_lambda(view, src)
      ->(rec) { view.send(src, rec) }
    end

    def self_lambda(src)
      ->(rec) { send(src, rec) }
    end


    def map_methods(mappings, proto)
      mapped_methods = {}
      view = view_context
      mappings.each do |mapping|
        if mapping.is_a? Array
          field = mapping.first
          src = mapping.last
        else
          field = src = mapping
        end

        # Check for arity on the model method to avoid issues when including helper in a controller
        # NOTE: negatives indicate optional and special parameters.
        # We use rescue here to avoid a rare (nonexistent?) NameError when respond_to? passes, but the method can't be found.
        call_model = false
        begin
          call_model = true if proto.respond_to?(src) && proto.method(src).arity <= 0
        rescue
          call_model = false
        end

        if call_model
          mapped_methods[field] = model_lambda(src)
        elsif view.respond_to? src
          mapped_methods[field] = view_lambda(view, src)
        elsif respond_to? src
          mapped_methods[field] = self_lambda(src)
        end
      end

      mapped_methods
    end

    def build_mappings(data, mappings)
      return data if mappings.nil? || mappings.empty?

      # Figure out mapping destinations on first record
      mapped_methods = nil

      data.map do |rec|
        mapped_methods ||= map_methods(mappings, rec)
        fields = {}
        mapped_methods.each do |field, method|
          fields[field] = method.call(rec)
        end

        rec.serializable_hash.merge!(fields)
      end
    end

end
